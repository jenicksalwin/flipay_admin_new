import React, { Component, Suspense, lazy, Fragment } from 'react';
import { Switch, Route, Redirect, BrowserRouter } from 'react-router-dom';
import store from "./store";
import jwt_decode from "jwt-decode";
import setAuthToken from "./utils/setAuthToken";
import { setCurrentUser, logoutUser } from "./actions/authActions";
import Spinner from '../app/shared/Spinner';
import { Provider } from "react-redux";
import { ToastContainer } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import PrivateRoute from "./components/privateRoute";

if (localStorage.adminToken) {
  const token = localStorage.adminToken;
  setAuthToken(token);
  const decoded = jwt_decode(token);
  store.dispatch(setCurrentUser(decoded));
  const currentTime = Date.now() / 1000;
  if (decoded.exp < currentTime) {
    store.dispatch(logoutUser());
    window.location.href = "./login";
  }
}

const Dashboard = lazy(() => import('./dashboard/Dashboard'));
const TransportCooperative = lazy(() => import('./adminPages/transportCooperative'));
const Vehicleservice = lazy(() => import('./adminPages/vehicleservice'));
const Vehicle = lazy(() => import('./adminPages/vehicle'));
const Device = lazy(() => import('./adminPages/device'));
const Distributor = lazy(() => import('./adminPages/Distributor'));
const Retailer = lazy(() => import('./adminPages/Retailer'));
const DriversList = lazy(() => import('./adminPages/DriversList'));
const TimeTracker = lazy(() => import('./adminPages/TimeTracker'));
const Sales = lazy(() => import('./adminPages/Sales'));
const TransactionHistory = lazy(() => import('./adminPages/TransactionHistory'));
const Messages = lazy(() => import('./adminPages/Messages'));
const RidersList = lazy(() => import('./adminPages/RidersList'));
const Wallet = lazy(() => import('./adminPages/Wallet'));
const RideHistory = lazy(() => import('./adminPages/RideHistory'));
const CashIn = lazy(() => import('./adminPages/CashIn'));
const AccountingSystem = lazy(() => import('./adminPages/AccountingSystem'));
const FareIncome = lazy(() => import('./adminPages/FareIncome'));
const LoadCard = lazy(() => import('./adminPages/LoadCard'));
const LoadSales = lazy(() => import('./adminPages/LoadSales'));
const CardSales = lazy(() => import('./adminPages/CardSales'));
const Emailtemplate = lazy(() => import('./adminPages/Emailtemplate'));
const PrivacyPolicy = lazy(() => import('./adminPages/PrivacyPolicy'));
const AdminController = lazy(() => import('./adminPages/AdminController'));
const AdminActivity = lazy(() => import('./adminPages/AdminActivity'));
const Profile = lazy(() => import('./adminPages/profile'));
const Support = lazy(() => import('./adminPages/Support'));
const Login = lazy(() => import('./user-pages/Login'));
























// const Buttons = lazy(() => import('./basic-ui/Buttons'));
// const Dropdowns = lazy(() => import('./basic-ui/Dropdowns'));

// const BasicElements = lazy(() => import('./form-elements/BasicElements'));

// const BasicTable = lazy(() => import('./tables/BasicTable'));

// const Mdi = lazy(() => import('./icons/Mdi'));

// const ChartJs = lazy(() => import('./charts/ChartJs'));

// const Error404 = lazy(() => import('./error-pages/Error404'));
// const Error500 = lazy(() => import('./error-pages/Error500'));

// const Login = lazy(() => import('./user-pages/Login'));
// const Register1 = lazy(() => import('./user-pages/Register'));


class AppRoutes extends Component {
  render() {
    return (
    

      <Provider store={store}>
        {console.log(store.getState().auth.user.rideHistory,"kkkkkkk")}
        <Fragment>
          <Suspense fallback={<Spinner />}>

            <Switch>
              <Route exact path="/login" component={Login} />
              <Route exact path="/" component={Login} />

              <Route exact path="/dashboard" component={Dashboard} />
              {/* {store.getState().auth.user.transport == true ? */}

                <Route exact path="/transportCooperative" component={TransportCooperative} />
                {/* : <Redirect to="/login" />} */}

              {/* {store.getState().auth.user.vehicleService == true ? */}
                <Route path="/vehicleservice" component={Vehicleservice} />
                {/* : <Redirect to="/login" />} */}

              {/* { store.getState().auth.user.Vehicle_Service == true ?
            <Route path="/vehicleservice" component={Vehicleservice} />
            : <Redirect to="/login" /> }  */}

              {/* {store.getState().auth.user.vehicle == true ? */}
                <Route path="/vehicle" component={Vehicle} />
                {/* : <Redirect to="/login" />} */}
              {/* {store.getState().auth.user.device == true ? */}
                <Route path="/device" component={Device} />
                {/* : <Redirect to="/login" />} */}


              {/* {store.getState().auth.user.distributor == true ? */}
                <Route path="/distributor" component={Distributor} />
                {/* : <Redirect to="/login" />} */}

              {/* {store.getState().auth.user.retailer == true ? */}
                <Route path="/retailer" component={Retailer} />
                {/* : <Redirect to="/login" />} */}

              {/* <Route path="/vehicleservice" component={Vehicleservice} /> */}


              {/* { store.getState().auth.user.transport == true ? */}
              {/* <Route exact path="/transportCooperative" component={TransportCooperative} /> */}
              {/* : <Redirect to="/login" /> }  */}


              {/* {store.getState().auth.user.driverList == true ? */}
                <Route path="/driverslist" component={DriversList} />
                {/* : <Redirect to="/login" />} */}

              {/* <Route path="/driverslist" component={DriversList} /> */}

              {/* {store.getState().auth.user.timeTracker == true ? */}
                <Route path="/timeTracker" component={TimeTracker} />
                {/* : <Redirect to="/login" />} */}


              {/* { store.getState().auth.user.sales == true ? */}
              <Route path="/SALES" component={Sales} />
             {/* : <Redirect to="/login" /> }  */}

              {/* {store.getState().auth.user.driverTrxhistory == true ? */}
                <Route path="/transactionhistory" component={TransactionHistory} />
                {/* : <Redirect to="/login" />} */}

              {/* {store.getState().auth.user.messageDriver == true ? */}
                <Route path="/MESSAGES" component={Messages} />
                {/* : <Redirect to="/login" />} */}

              {/* {store.getState().auth.user.riderList == true ? */}
                <Route path="/ridersList" component={RidersList} />
                {/* : <Redirect to="/login" />} */}

              {/* {store.getState().auth.user.wallet == true ? */}
                <Route path="/wallet" component={Wallet} />
                {/* : <Redirect to="/login" />} */}

              {/* {store.getState().auth.user.rideHistory === true ?  */}

                <Route path="/ridehistory" component={RideHistory} />
                          {/* : <Redirect to="/login" />} */}

              {/* {store.getState().auth.user.cashIn == true ? */}
                <Route path="/cashIn" component={CashIn} />
                {/* : <Redirect to="/login" />} */}
              {/* <Route path="/cashIn" component={CashIn} /> */}

              {/* {store.getState().auth.user.loadSales == true ? */}
                <Route path="/loadCard" component={LoadCard} />
                {/* : <Redirect to="/login" />} */}

              {/* {store.getState().auth.user.accountingSystem == true ? */}
                <Route path="/accountingsystem" component={AccountingSystem} />
                {/* : <Redirect to="/login" />} */}


              {/* {store.getState().auth.user.fareIncome == true ? */}
                <Route path="/fareincome" component={FareIncome} />
                {/* : <Redirect to="/login" />} */}
              {/* <Route path="/fareincome" component={FareIncome} /> */}

              {/* <Route path="/loadsales" component={LoadSales} /> */}
              {/* {store.getState().auth.user.cardSales == true ? */}


                <Route path="/cardsales" component={CardSales} />
                {/* : <Redirect to="/login" />} */}

              {/* <Route path="/cardsales" component={CardSales} /> */}

              {/* {store.getState().auth.user.emailTemplate == true ? */}
                <Route path="/emailtemplate" component={Emailtemplate} />
                {/* : <Redirect to="/login" />} */}
              {/* {store.getState().auth.user.privacyPolicy == true ? */}

                <Route path="/privacypolicy" component={PrivacyPolicy} />
                {/* : <Redirect to="/login" />} */}


              {/* {store.getState().auth.user.SubadminMenu == true ? */}

                <Route path="/AdminController" component={AdminController} />
                {/* : <Redirect to="/login" />} */}

              {/* {store.getState().auth.user.adminActivity == true ? */}
                <Route path="/AdminActivity" component={AdminActivity} />
                {/* : <Redirect to="/login" />} */}
              {/* {store.getState().auth.user.support == true ? */}
                <Route path="/support" component={Support} />

                {/* : <Redirect to="/login" />} */}

                <Route path="/profile" component={Profile} />



              {/* <Route path="/basic-ui/buttons" component={ Buttons } />
          <Route path="/basic-ui/dropdowns" component={ Dropdowns } />

          <Route path="/form-Elements/basic-elements" component={ BasicElements } />

          <Route path="/tables/basic-table" component={ BasicTable } />

          <Route path="/icons/mdi" component={ Mdi } />

          <Route path="/charts/chart-js" component={ ChartJs } />


          <Route path="/user-pages/login-1" component={ Login } />
          <Route path="/user-pages/register-1" component={ Register1 } />

          <Route path="/error-pages/error-404" component={ Error404 } />
          <Route path="/error-pages/error-500" component={ Error500 } /> */}


              {/* <Redirect to="/dashboard" /> */}
            </Switch>
          </Suspense>
        </Fragment>
        <ToastContainer />

      </Provider>

    );
  }
}

export default AppRoutes;