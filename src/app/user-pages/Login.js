import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Form } from 'react-bootstrap';
import { connect } from "react-redux";
import { loginUser } from "../actions/authActions";
import PropTypes from "prop-types";
import classnames from "classnames";

export class Login extends Component {
  constructor() {
    super();
    this.state = {
        email: "",
        password: "",
        errors: {}
    };
}

componentDidMount() {
    if (this.props.auth.isAuthenticated) {
        //this.props.history.push("/dashboard");
    }
};

componentWillReceiveProps(nextProps) {
  console.log("nextPropsnextPropsnextProps",nextProps.auth.isAuthenticated)
    if (nextProps.auth.isAuthenticated) {
      
        this.props.history.push("/transportCooperative");
        window.location.reload("/ui-templates/FILIPAY/transportCooperative/transportCooperative");
    }

    if (nextProps.errors) {
        this.setState({
            errors: nextProps.errors
        });
    }
}

onChange = e => {
    this.setState({ [e.target.id]: e.target.value });
    
};

onSubmit = e => {
    e.preventDefault();
    const userData = {
        email: this.state.email,
        password: this.state.password
    };
    this.props.loginUser(userData);
};

  render() {
    const { errors } = this.state;
    return (
      <div>
            <div className="container-fluid p-3">
            <div className="row">
              <div className="col-lg-4">
              <div className="login_form_section">
              <img src={require("../../assets/images/login_logo1.png")} alt="logo" className="img-fluid" />
              <h2 class="crediantial_title">Login Form</h2>
              <Form className="pt-3" noValidate onSubmit={this.onSubmit} >
              <label>Email Address</label>
                  <Form.Group className="d-flex search-field mb-3">
                    <Form.Control  placeholder="Username" size="lg" className="h-auto" 
                    
                                    onChange={this.onChange}
                                    value={this.state.email}
                                    error={errors.email}
                                    id="email"
                                    type="email"
                                    className={classnames("form-control", {
                                        invalid: errors.email
                                    })}
                              />
                             
                  </Form.Group>
                   {errors.email}<br></br>
                  <label>Password</label>
                  <Form.Group className="d-flex search-field mb-3">
                    <Form.Control  placeholder="Password" size="lg" className="h-auto" 
                    onChange={this.onChange}
                    value={this.state.password}
                    error={errors.password}
                    id="password"
                    type="password"
                    // className={classnames("form-control", {
                    //     invalid: errors.password
                    // })}
                    />
                  </Form.Group>
                  {errors.password}<br></br>
                 
                  <div className="my-2 d-flex justify-content-end align-items-center">
                    <a href="!#" onClick={event => event.preventDefault()} className="auth-link text-black">Forgot password?</a>
                  </div>
                  <div className="my-2 d-flex justify-content-between align-items-center">
                    <div className="form-check">
                      <label className="form-check-label text-muted">
                        <input type="checkbox" className="form-check-input"/>
                        <i className="input-helper"></i>
                        Keep me signed in
                      </label>
                    </div>
                   
                  </div>
                  <div className="mt-3">
                  <button
                                        type="submit"
                                        className="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn">
                                        Login
                                    </button>
                    {/* <Link className="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn" to="/dashboard">SIGN IN</Link> */}
                  </div>
                </Form>
                </div>
              </div>
              <div className="col-lg-8">
                <div className="login_form_banner_section">
                <h1><span>FILIPAY</span><br /><span class="inner_color_s">DASHBOARD</span><br /><span class="normal_fpot">MANAGEMENT SYSTEM</span></h1>
                <div className="img_section">
                        <img src={require("../../assets/images/filipay_pc1.png")} alt="logo" className="img-fluid" />
                        </div>
                </div>
              </div>
            </div>
          </div>

        {/* <div className="d-flex align-items-center auth px-0">
      
          <div className="row w-100 mx-0">
            <div className="col-lg-4 mx-auto">
              <div className="auth-form-light text-left py-5 px-4 px-sm-5">
                <div className="brand-logo">
                  <img src={require("../../assets/images/logo.svg")} alt="logo" />
                </div>
                <h4>Hello! let's get started</h4>
                <h6 className="font-weight-light">Sign in to continue.</h6>
                <Form className="pt-3">
                  <Form.Group className="d-flex search-field">
                    <Form.Control type="email" placeholder="Username" size="lg" className="h-auto" />
                  </Form.Group>
                  <Form.Group className="d-flex search-field">
                    <Form.Control type="password" placeholder="Password" size="lg" className="h-auto" />
                  </Form.Group>
                  <div className="mt-3">
                    <Link className="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn" to="/dashboard">SIGN IN</Link>
                  </div>
                  <div className="my-2 d-flex justify-content-between align-items-center">
                    <div className="form-check">
                      <label className="form-check-label text-muted">
                        <input type="checkbox" className="form-check-input"/>
                        <i className="input-helper"></i>
                        Keep me signed in
                      </label>
                    </div>
                    <a href="!#" onClick={event => event.preventDefault()} className="auth-link text-black">Forgot password?</a>
                  </div>
                  <div className="mb-2">
                    <button type="button" className="btn btn-block btn-facebook auth-form-btn">
                      <i className="mdi mdi-facebook mr-2"></i>Connect using facebook
                    </button>
                  </div>
                  <div className="text-center mt-4 font-weight-light">
                    Don't have an account? <Link to="/user-pages/register" className="text-primary">Create</Link>
                  </div>
                </Form>
              </div>
            </div>
          </div>
        </div>   */}
      </div>
    )
  }
}


Login.propTypes = {
  loginUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};
const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});
export default connect(
  mapStateToProps,
  { loginUser }
)(Login);

