import React, { useState, useEffect } from "react";
import DataTable from 'react-data-table-component';
import DatePicker from "react-datepicker";
import Select from 'react-select';
import "react-datepicker/dist/react-datepicker.css";
import { CKEditor } from 'ckeditor4-react';
import { getTransportAdminInfo } from '../actions/transportCooperative'
import config from '../actions/config';

// Trade History Table
const data = [
    {
      Email_Address:"",
      Route:"", 
      Chairman:"",
      CDA_Certification:<div className="tik_flex_img"><img src={require("../../assets/images/tick_img.png")} alt="logo" className="img-fluid inner_img_e" /><a href=""><i class="far fa-folder-open"></i></a><a href=""><i class="fas fa-download"></i></a></div>, 
      SEC_Certification:<div className="tik_flex_img"><img src={require("../../assets/images/tick_img.png")} alt="logo" className="img-fluid inner_img_e" /><a href=""><i class="far fa-folder-open"></i></a><a href=""><i class="fas fa-download"></i></a></div>, 
      Articlesand:<div className="tik_flex_img"><img src={require("../../assets/images/tick_img.png")} alt="logo" className="img-fluid inner_img_e" /><a href=""><i class="far fa-folder-open"></i></a><a href=""><i class="fas fa-download"></i></a></div>,
     Business_Permit:<div className="tik_flex_img"><img src={require("../../assets/images/tick_img.png")} alt="logo" className="img-fluid inner_img_e" /><a href=""><i class="far fa-folder-open"></i></a><a href=""><i class="fas fa-download"></i></a></div>, 
     Bank_Certification:<div className="tik_flex_img"><img src={require("../../assets/images/tick_img.png")} alt="logo" className="img-fluid inner_img_e" /><a href=""><i class="far fa-folder-open"></i></a><a href=""><i class="fas fa-download"></i></a></div>,
     Board_Resolution:<div className="tik_flex_img"><img src={require("../../assets/images/tick_img.png")} alt="logo" className="img-fluid inner_img_e" /><a href=""><i class="far fa-folder-open"></i></a><a href=""><i class="fas fa-download"></i></a></div>, 
     Fare_Matrix:"", 
     DATE:"",
     Status:<p className="text_gree__o"><span className="">Approved</span></p>, 
     Action:<div className="action_tab"><a href="" data-toggle="modal" data-target="#newmsg"><i class="far fa-comments"></i></a><a href=""  data-toggle="modal" data-target="#Editdetais"><i class="fas fa-edit"></i></a></div>,
    },
    {
      Email_Address:"",
      Route:"", 
      Chairman:"",
      CDA_Certification:<div className="tik_flex_img"><img src={require("../../assets/images/tick_img.png")} alt="logo" className="img-fluid inner_img_e" /><a href=""><i class="far fa-folder-open"></i></a><a href=""><i class="fas fa-download"></i></a></div>, 
      SEC_Certification:<div className="tik_flex_img"><img src={require("../../assets/images/tick_img.png")} alt="logo" className="img-fluid inner_img_e" /><a href=""><i class="far fa-folder-open"></i></a><a href=""><i class="fas fa-download"></i></a></div>, 
      Articlesand:<div className="tik_flex_img"><img src={require("../../assets/images/tick_img.png")} alt="logo" className="img-fluid inner_img_e" /><a href=""><i class="far fa-folder-open"></i></a><a href=""><i class="fas fa-download"></i></a></div>,
     Business_Permit:<div className="tik_flex_img"><img src={require("../../assets/images/tick_img.png")} alt="logo" className="img-fluid inner_img_e" /><a href=""><i class="far fa-folder-open"></i></a><a href=""><i class="fas fa-download"></i></a></div>, 
     Bank_Certification:<div className="tik_flex_img"><img src={require("../../assets/images/tick_img.png")} alt="logo" className="img-fluid inner_img_e" /><a href=""><i class="far fa-folder-open"></i></a><a href=""><i class="fas fa-download"></i></a></div>,
     Board_Resolution:<div className="tik_flex_img"><img src={require("../../assets/images/tick_img.png")} alt="logo" className="img-fluid inner_img_e" /><a href=""><i class="far fa-folder-open"></i></a><a href=""><i class="fas fa-download"></i></a></div>, 
     Fare_Matrix:"", 
     DATE:"",
     Status:<p className="text_gree__o color_red_o"><span className="">Disapproved</span></p>, 
     Action:<div className="action_tab"><a href="" data-toggle="modal" data-target="#newmsg"><i class="far fa-comments"></i></a><a href="" data-toggle="modal" data-target="#Editdetais"><i class="fas fa-edit"></i></a></div>,
    },
  // { slno: "1", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "2", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "3", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "4", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "5", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "6", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "7", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "8", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "9", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "10", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  ];


function Test() {





  const columns = [
    {
      name: 'EMAIL ADDRESS',
      selector: 'email',
      sortable: true,
    
    },
    {
      name: 'ROUTE',
      selector: 'Route',
      sortable: true,
    },
    {
      name: 'CHAIRMAN',
      selector: 'Chairman ',
      sortable: true,
    },
    {
      name: 'CDA CERTIFICATION',
      selector: 'CDA_Certification',
      sortable: true,
      cell: record => {
        console.log('record', record)

        return (
          // <Fragment>
          <>
            <button
              className="tik_flex_img buttn_trans"
            >
              <i class="fas fa-check-circle"></i>
            </button>
            <button
              className="tik_flex_img buttn_trans"
            >
              <i class="far fa-folder-open "></i>
            </button>
            <button
              className="tik_flex_img buttn_trans"
            >
              <i class="fas fa-download"></i>
            </button>
          </>

          // </Fragment>
        );
      }
    },
    {
      name: 'SEC CERTIFICATION',
      selector: 'SEC_Certification',
      sortable: true,
      cell: record => {
        console.log('record', record)

        return (
          // <Fragment>
          <>
            <button
              className="tik_flex_img buttn_trans"
            >
              <i class="fas fa-check-circle"></i>
            </button>
            <button
              className="tik_flex_img buttn_trans"
            >
              <i class="far fa-folder-open "></i>
            </button>
            <button
              className="tik_flex_img buttn_trans"
            >
              <i class="fas fa-download"></i>
            </button>
          </>

          // </Fragment>
        );
      }
    },
    {
      name: 'ARTICLESAND BY LAWS',
      selector: 'Articlesand',
      sortable: true,
      cell: record => {
        console.log('record', record)

        return (
          // <Fragment>
          <>
            <button
              className="tik_flex_img buttn_trans"
            >
              <i class="fas fa-check-circle"></i>
            </button>
            <button
              className="tik_flex_img buttn_trans"
            >
              <i class="far fa-folder-open "></i>
            </button>
            <button
              className="tik_flex_img buttn_trans"
            >
              <i class="fas fa-download"></i>
            </button>
          </>

          // </Fragment>
        );
      }
      
    },
    {
      name: 'BUSINESS PERMIT',
      selector: 'Business_Permit',
      sortable: true,
      cell: record => {
        console.log('record', record)

        return (
          // <Fragment>
          <>
            <button
              className="tik_flex_img buttn_trans"
            >
              <i class="fas fa-check-circle"></i>
            </button>
            <button
              className="tik_flex_img buttn_trans"
            >
              <i class="far fa-folder-open "></i>
            </button>
            <button
              className="tik_flex_img buttn_trans"
            >
              <i class="fas fa-download"></i>
            </button>
          </>

          // </Fragment>
        );
      }
      
    },
    {
      name: 'BANK CERTIFICATION',
      selector: 'Bank_Certification',
      sortable: true,
      cell: record => {
        console.log('record', record)

        return (
          // <Fragment>
          <>
            <button
              className="tik_flex_img buttn_trans"
            >
              <i class="fas fa-check-circle"></i>
            </button>
            <button
              className="tik_flex_img buttn_trans"
            >
              <i class="far fa-folder-open "></i>
            </button>
            <button
              className="tik_flex_img buttn_trans"
            >
              <i class="fas fa-download"></i>
            </button>
          </>

          // </Fragment>
        );
      }
    },
    {
      name: 'BOARD RESOLUTION',
      selector: 'Board_Resolution',
      sortable: true,
      cell: record => {
        console.log('record', record)

        return (
          // <Fragment>
          <>
            <button
              className="tik_flex_img buttn_trans"
            >
              <i class="fas fa-check-circle"></i>
            </button>
            <button
              className="tik_flex_img buttn_trans"
            >
              <i class="far fa-folder-open "></i>
            </button>
            <button
              className="tik_flex_img buttn_trans"
            >
              <i class="fas fa-download"></i>
            </button>
          </>

          // </Fragment>
        );
      }
    },
    {
      name: 'FARE MATRIX',
      selector: 'Fare_Matrix',
      sortable: true,
      cell: record => {
        console.log('record', record)

        return (
          // <Fragment>
          <>
            <button
              className="tik_flex_img buttn_trans"
            >
              <i class="fas fa-check-circle"></i>
            </button>
            <button
              className="tik_flex_img buttn_trans"
            >
              <i class="far fa-folder-open "></i>
            </button>
            <button
              className="tik_flex_img buttn_trans"
            >
              <i class="fas fa-download"></i>
            </button>
          </>

          // </Fragment>
        );
      }
    },
    {
      name: 'DATE',
      selector: 'DATE',
      sortable: true,
    },
    {
      name: 'STATUS',
      selector: 'Status',
      sortable: true,
    },
    {
      name: 'ACTION',
      selector: 'Action',
      sortable: true,
      cell: record => {
        // console.log('record', record)

        return (
          // <Fragment>
          <>
            <button
              data-tip="Edit"
              data-toggle="modal"
              data-target="#Editdetais"
              className="action_tab buttn_trans"

              onClick={() => editR(record)}
            // onClick={() => editRecord(record)}
            // style={{ marginRight: "5px" }}
            >
              <i className="fa fa-edit"></i>

            </button>

            <button
              data-tip="Edit"
              data-toggle="modal"
              data-target="#newmsg"
              className="action_tab buttn_trans"

              // onClick={() => editRecord(record._id)}
              style={{ marginRight: "5px" }}
            >
              <i className="fa fa-comments"></i>

            </button>
          </>

          // </Fragment>
        );
      }
    },
  ];
  const options = [
    { value: 'SND', label: 'SND' },
    { value: 'SNR', label: 'SNR' },
    { value: 'allTransactions', label: 'All transactions' },
  ];
  const initialFormValue = {
    '_id': '',
    'role': '',
    'email':'',

  }

  const [startDate, setStartDate] = useState(new Date());
  const [adminInfo, setAdminInfo] = useState('')
  const [formValue, setFormValue] = useState(initialFormValue)
  const [optionValue, setOptionvalue] = useState()
  const [toggleNav, setToggleNav] = useState()

  const getAdminInfo = async (reqData) => {
    const { result, status, loading, error } = await getTransportAdminInfo(reqData);
    if (status == 'success') {
      setAdminInfo(result)
    
    }
    else {
      console.log('err,e', error)
    }
  }

  const handleChange = (e) => {
    // e.preventDefault();
    console.log('1e', e)

    const { id, value } = e.target;
    // const { name} = e.value
    let formData = { ...formValue, ...{ [id]: value } }
    console.log('formData', formData)
    setFormValue(formData)
  }
  const editR = async (record) => {

    // e.preventDefault();
    // const { id, value } = e.target;
    // let formData = { ...formValue, ...{ [id]: value } }
    // setFormValue()    
    console.log('record-----', record)
    setFormValue(record)
    // setKycStatusNo(record.kycStatusNo)
  }
  const handleSearch = async (e) => {
    console.log('e', e.target.value)
    let reqData = {
      page: 1,
      limit: 10,
      search: e.target.value,
      // from: startDate,
      // to: endDate
    }
    getAdminInfo(reqData)
  }

  useEffect(() => {

    getAdminInfo()
  },[])
    return (
        <div>
           <div className="row">
             <div className="col-lg-12">
             <h1 className="tiltl_head_tetx">Transport Cooperative</h1>
             </div>
            <div className="col-lg-1">

            </div>
            <div className="col-lg-3">
              <div className="d-flex justify-content-center">
              <div className="calaleder_section">
                <label>From</label>
                 <div className="datepiker_sec">
                 <DatePicker selected={startDate} onChange={(date) => setStartDate(date)} />
                 <i class="far fa-calendar-alt"></i>
                   </div> 
              </div>
              <div className="calaleder_section">
                <label>To</label>
                 <div className="datepiker_sec">
                 <DatePicker selected={startDate} onChange={(date) => setStartDate(date)} />
                 <i class="far fa-calendar-alt"></i>
                   </div> 
              </div>
              </div>
              
           
            </div>
            <div className="col-lg-3">
              <div className="paddin_sss">
                <label></label>
                <Select
                options={options}
              />
              </div>
            
            </div>
            <div className="col-lg-5">
              <div className="search_download">
                <div className="input_1">
                    <input type="text" onChange={handleSearch} placeholder="Filter in Records..." />
                    <i class="fas fa-search"></i>
                </div>
                <div>
                  <button>Download <i class="fas fa-download"></i></button>
                </div>
              </div>
            </div>
            </div> 
            <div className="row">
              <div className="col-lg-12">
          <DataTable columns={columns} data={adminInfo} noHeader  className="table_new_s"  pagination={true} paginationPerPage="5" paginationRowsPerPageOptions={[5, 10, 15, 20]} />

              </div>
            </div>
          <div className="row">
            <div className="col-lg-12">
              <a href="" className="class_a"> <i class="fas fa-plus"></i> Add</a>
            </div>
          </div>



<div class="modal" id="newmsg">
  <div class="modal-dialog modal-dialog-centered modal-md modal-dialog_cutom_width">
    <div class="modal-content">


      <div class="modal-header">
        <h4 class="modal-title model_heder_colo">New Message</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>


      <div class="modal-body">
        <div className="row">
          <div className="col-lg-12">
              <div className="send_msg_form">
                <label>Recepient:</label>
                <input type="text" />
              </div>
              <div className="send_msg_form">
                <label>Subject:</label>
                <input type="text" />
              </div>
          </div>
        </div>
        <div className="div_clas_po">
        <label>Text:</label>
       <CKEditor initData={<p>This is an example CKEditor 4 WYSIWYG editor instance.</p>} />
        </div>
       
      </div>

      <div class="modal-footer justify-content-between align-items-end">
        <div className="signature_sectooion">
          <label>signature</label>
          <div className="signature_filed">
            <div className="bottom_btn">
            <a href="">Change</a>
              <a href="">Remove</a>
            </div>
          </div>
        </div>
        <div className="button_section_model">
        <button type="button" class="btn dlelte" data-dismiss="modal">Delete</button>
        <button type="button" class="btn savemsg" >Save to Drafts</button>
        
        <button type="button" class="btn sendmsg" data-dismiss="modal">Send Message</button>
        </div>
       
      </div>

    </div>
  </div>
</div>
<div class="modal" id="Editdetais">
  <div class="modal-dialog modal-dialog-centered modal-md">
    <div class="modal-content">


      <div class="modal-header">
        <h4 class="modal-title model_heder_colo">Edit Details</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>


      <div class="modal-body edit_section_d_model">
       <div className="row">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Logo</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="content_clolk">
          <div className="img_section_78">
          <i class="far fa-image"></i>
          </div>
          <div className="image_options">
            <a href=""> View</a>
            <a href=""> Upload</a>

            <a href="" className="remover"> Remove</a>

          </div>
          </div>
        </div>
       </div>


       <div className="row">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>EMAIL ADDRESS</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="input_box_msg">
          <input type="text" id="email" onChange={handleChange} value={formValue.email}/>
         
          </div>
        </div>
       </div>

       <div className="row">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>DATE</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="input_box_msg panndin_back">
          <DatePicker selected={startDate} onChange={(date) => setStartDate(date)} />

          </div>
        </div>
       </div>

       <div className="row">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>ROUTE</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="input_box_msg">
          <Select
                options={options}
              />

          </div>
        </div>
       </div>

       <div className="row">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Chairman ID</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="content_clolk">
          <div className="img_section_78">
          <i class="far fa-image"></i>
          </div>
          <div className="image_options">
            <a href=""> View</a>
            <a href=""> Upload</a>

            <a href="" className="remover"> Remove</a>

          </div>
          </div>
        </div>
       </div>
       <div className="row">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>CDA Certification</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="content_clolk">
          <div className="img_section_78">
          <img src={config.baseUrl+"images/admin/"+formValue.cdacertificate} alt = "" width="40" 
     height="40"/> <br/> <br/>
          <i class="far fa-image"></i>
          </div>
          <div className="image_options">
          <a href={config.baseUrl+"images/admin/"+formValue.cdacertificate}  target="_blank">View</a>
            <a href=""> Upload</a>

            <a href="" className="remover"> Remove</a>

          </div>
          </div>
        </div>
       </div>
       <div className="row">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>SEC Certification</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="content_clolk">
          <div className="img_section_78">
          <img src={config.baseUrl+"images/admin/"+formValue.seccertificate} alt = "" width="40" 
     height="40"/> <br/> <br/>

          <i class="fas fa-file-pdf"></i>
          </div>
          <div className="image_options">
          <a href={config.baseUrl+"images/admin/"+formValue.seccertificate}  target="_blank">View</a>
            <a href=""> Upload</a>

            <a href="" className="remover"> Remove</a>

          </div>
          </div>
        </div>
       </div>
       <div className="row">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Articles and By Laws</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="content_clolk">
          <div className="img_section_78">
          <i class="fas fa-file-pdf"></i>
          </div>
          <div className="image_options">
            <a href=""> View</a>
            <a href=""> Upload</a>

            <a href="" className="remover"> Remove</a>

          </div>
          </div>
        </div>
       </div>
       <div className="row">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Business Permit</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="content_clolk">
          <div className="img_section_78">
          <i class="fas fa-file-pdf"></i>
          </div>
          <div className="image_options">
            <a href=""> View</a>
            <a href=""> Upload</a>

            <a href="" className="remover"> Remove</a>

          </div>
          </div>
        </div>
       </div>
       <div className="row">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Bank Certification</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="content_clolk">
          <div className="img_section_78">
          <i class="fas fa-file-pdf"></i>
          </div>
          <div className="image_options">
            <a href=""> View</a>
            <a href=""> Upload</a>

            <a href="" className="remover"> Remove</a>

          </div>
          </div>
        </div>
       </div>
       <div className="row">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Board Resolution</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="content_clolk">
          <div className="img_section_78">
          <i class="fas fa-file-pdf"></i>
          </div>
          <div className="image_options">
            <a href=""> View</a>
            <a href=""> Upload</a>

            <a href="" className="remover"> Remove</a>

          </div>
          </div>
        </div>
       </div>
       <div className="row">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Fare Matrix</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="content_clolk">
          <div className="img_section_78">
          <i class="fas fa-file-pdf"></i>
          </div>
          <div className="image_options">
            <a href=""> View</a>
            <a href=""> Upload</a>

            <a href="" className="remover"> Remove</a>

          </div>
          </div>
        </div>
       </div>
       <div className="row">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Status</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="content_clolk">
          
          <div className="flexs">
          <div class="custom-control custom-radio">
            <input type="radio" class="custom-control-input" id="customRadio" name="" value="" />
            <label class="custom-control-label" for="customRadio">Approved </label>
          </div>
          <div class="custom-control custom-radio">
            <input type="radio" class="custom-control-input" id="customRadio_1" name="" value="" />
            <label class="custom-control-label" for="customRadio_1">Disapproved </label>
          </div>
          </div>
          </div>
        </div>
       </div>

        
       
      </div>

      <div class="modal-footer  align-items-end">
     
        <div className="button_section_model">
      
        <button type="button" class="btn savemsg" data-dismiss="modal" >Delete</button>
        
        <button type="button" class="btn sendmsg" data-dismiss="modal">Save</button>
        </div>
       
      </div>

    </div>
  </div>
</div>
        </div>
    )
}

export default Test
