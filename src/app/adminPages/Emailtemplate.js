import React, { useState } from "react";
import DataTable from 'react-data-table-component';
import DatePicker from "react-datepicker";
import Select from 'react-select';
import "react-datepicker/dist/react-datepicker.css";
import { CKEditor } from 'ckeditor4-react';

// Trade History Table
const data = [
  {
    Subject:<div><p className="email_temp_text">Activation Link</p></div>, 
    identifier:"",
    action :<div className="action_tab"><a href="" data-toggle="modal" data-target="#newmsg"><i class="fas fa-paper-plane"></i></a><a href="" data-toggle="modal" data-target="#EmailTemp"><i class="fas fa-edit"></i></a><a href="" data-toggle="modal" data-target="#delete"><i class="far fa-trash-alt"></i></a></div>, 

  
  
  },
  {
    Subject:<div><p className="email_temp_text">Contact Us</p></div>, 
    identifier:"",
    action :<div className="action_tab"><a href="" data-toggle="modal" data-target="#newmsg"><i class="fas fa-paper-plane"></i></a><a href="" data-toggle="modal" data-target="#EmailTemp"><i class="fas fa-edit"></i></a><a href="" data-toggle="modal" data-target="#delete"><i class="far fa-trash-alt"></i></a></div>, 
  
  },
  {
    Subject:<div><p className="email_temp_text">Cash In Status (Approved)</p></div>, 
    identifier:"",
    action :<div className="action_tab"><a href="" data-toggle="modal" data-target="#newmsg"><i class="fas fa-paper-plane"></i></a><a href="" data-toggle="modal" data-target="#EmailTemp"><i class="fas fa-edit"></i></a><a href="" data-toggle="modal" data-target="#delete"><i class="far fa-trash-alt"></i></a></div>, 
  
  
  
  },
  {
    Subject:<div><p className="email_temp_text">Cash In Status (Disapproved)</p></div>, 
    identifier:"",
    action :<div className="action_tab"><a href="" data-toggle="modal" data-target="#newmsg"><i class="fas fa-paper-plane"></i></a><a href="" data-toggle="modal" data-target="#EmailTemp"><i class="fas fa-edit"></i></a><a href="" data-toggle="modal" data-target="#delete"><i class="far fa-trash-alt"></i></a></div>, 
  
  
  
  },
  {
    Subject:<div><p className="email_temp_text">Cash Out Status (Approved)</p></div>, 
    identifier:"",
    action :<div className="action_tab"><a href="" data-toggle="modal" data-target="#newmsg"><i class="fas fa-paper-plane"></i></a><a href="" data-toggle="modal" data-target="#EmailTemp"><i class="fas fa-edit"></i></a><a href="" data-toggle="modal" data-target="#delete"><i class="far fa-trash-alt"></i></a></div>, 
  
  
  },
  {
    Subject:<div><p className="email_temp_text">Cash Out Status (Disapproved)</p></div>, 
    identifier:"",
    action :<div className="action_tab"><a href="" data-toggle="modal" data-target="#newmsg"><i class="fas fa-paper-plane"></i></a><a href="" data-toggle="modal" data-target="#EmailTemp"><i class="fas fa-edit"></i></a><a href="" data-toggle="modal" data-target="#delete"><i class="far fa-trash-alt"></i></a></div>, 
  
  
  },
  {
    Subject:<div><p className="email_temp_text">KYC Update Request</p></div>, 
    identifier:"",
    action :<div className="action_tab"><a href="" data-toggle="modal" data-target="#newmsg"><i class="fas fa-paper-plane"></i></a><a href="" data-toggle="modal" data-target="#EmailTemp"><i class="fas fa-edit"></i></a><a href="" data-toggle="modal" data-target="#delete"><i class="far fa-trash-alt"></i></a></div>, 
  
  
  },  
  {
    Subject:<div><p className="email_temp_text">KYC Newsletter</p></div>, 
    identifier:"",
    action :<div className="action_tab"><a href="" data-toggle="modal" data-target="#newmsg"><i class="fas fa-paper-plane"></i></a><a href="" data-toggle="modal" data-target="#EmailTemp"><i class="fas fa-edit"></i></a><a href="" data-toggle="modal" data-target="#delete"><i class="far fa-trash-alt"></i></a></div>, 
  
  
  },  
 

  ];
  const columns = [
    {
      name: 'SUBJECT',
      selector: 'Subject',
      sortable: true,
    
    },
    {
      name: 'IDENTIFIER',
      selector: 'identifier',
      sortable: true,
    
    },
    {
      name: 'ACTION',
      selector: 'action',
      sortable: true,
    },
  ];
  const options = [
    { value: 'SND', label: 'SND' },
    { value: 'SNR', label: 'SNR' },
    { value: 'Alltransactions', label: 'All transactions' },
  ];

function Test() {
  const [startDate, setStartDate] = useState(new Date());

    return (
        <div>
           <div className="row">
             <div className="col-lg-12">
             <h1 className="tiltl_head_tetx">Email Template</h1>
            
             </div>
            <div className="col-lg-1">

            </div>
          
            <div className="col-lg-3">
           
              
           
            </div>
            <div className="col-lg-3">
           
            
            </div>
            
            <div className="col-lg-5 mb-3">
              <div className="search_download">
                <div className="input_1">
                    <input type="text" placeholder="Filter in Records..." />
                    <i class="fas fa-search"></i>
                </div>
                <div>
                  <button>Download <i class="fas fa-download"></i></button>
                </div>
              </div>
            </div>
            </div> 
            <div className="row">
              <div className="col-lg-12 ">
              <DataTable columns={columns} data={data} noHeader  className="table_new_s"  pagination={true} paginationPerPage="5" paginationRowsPerPageOptions={[5, 10, 15, 20]} />
           
                
              </div>
            </div>
          

{/* <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add_detais">
  Open modal
</button> */}



<div class="modal" id="newmsg">
  <div class="modal-dialog modal-dialog-centered modal-md modal-dialog_cutom_width">
    <div class="modal-content">


      <div class="modal-header">
        <h4 class="modal-title model_heder_colo">New Message</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>


      <div class="modal-body">
        <div className="row">
          <div className="col-lg-12">
              <div className="send_msg_form">
                <label>Recepient:</label>
                <input type="text" />
              </div>
              <div className="send_msg_form">
                <label>Subject:</label>
                <input type="text" />
              </div>
          </div>
        </div>
        <div className="div_clas_po">
        <label>Text:</label>
       <CKEditor initData={<p>This is an example CKEditor 4 WYSIWYG editor instance.</p>} />
        </div>
       
      </div>

      <div class="modal-footer justify-content-between align-items-end">
        <div className="signature_sectooion">
          <label>signature</label>
          <div className="signature_filed">
            <div className="bottom_btn">
            <a href="">Change</a>
              <a href="">Remove</a>
            </div>
          </div>
        </div>
        <div className="button_section_model">
        <button type="button" class="btn dlelte" data-dismiss="modal">Delete</button>
        <button type="button" class="btn savemsg" >Save to Drafts</button>
        
        <button type="button" class="btn sendmsg" data-dismiss="modal">Send Message</button>
        </div>
       
      </div>

    </div>
  </div>
</div>
<div class="modal" id="EmailTemp">
  <div class="modal-dialog modal-dialog-centered modal-md modal-dialog_cutom_width">
    <div class="modal-content">


      <div class="modal-header">
        <h4 class="modal-title model_heder_colo">Edit Email Template</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>


      <div class="modal-body">
        <div className="row">
          <div className="col-lg-12">
              <div className="send_msg_form">
                <label>Recepient:</label>
                <input type="text" />
              </div>
              <div className="send_msg_form">
                <label>Subject:</label>
                <input type="text" />
              </div>
          </div>
        </div>
        <div className="div_clas_po">
        <label>Text:</label>
       <CKEditor initData={<p>This is an example CKEditor 4 WYSIWYG editor instance.</p>} />
        </div>
       
      </div>

      <div class="modal-footer justify-content-end align-items-end">
        
        <div className="button_section_model">
        <button type="button" class="btn dlelte" data-dismiss="modal">Delete</button>
        <button type="button" class="btn savemsg" >Save as New Template</button>
        
        <button type="button" class="btn sendmsg" data-dismiss="modal">Send</button>
        </div>
       
      </div>

    </div>
  </div>
</div>
<div class="modal" id="delete">
  <div class="modal-dialog modal-dialog-centered modal-md">
    <div class="modal-content">


      <div class="modal-header">
        <h4 class="modal-title model_heder_colo">Delete Template</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>


      <div class="modal-body edit_section_d_model">

       <div className="row">
        <div className="col-lg-12 grif_place">
         <p>Are you sure you want to delete this file? Click DELETE to proceed</p>
            
        </div>
        
       </div>

        
       
      </div>

      <div class="modal-footer  align-items-end">
     
        <div className="button_section_model">
        <button type="button" class="btn dlelte" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn sendmsg" data-dismiss="modal">Delete</button>
        </div>
       
      </div>

    </div>
  </div>
</div>


        </div>
    )
}

export default Test
