import React, { useState, useEffect } from "react";
import DataTable from 'react-data-table-component';
import DatePicker from "react-datepicker";
import Select from 'react-select';
import "react-datepicker/dist/react-datepicker.css";
import { CKEditor } from 'ckeditor4-react';
import { getRiderWallet } from "../actions/rider";

// Trade History Table
const data = [
  {
    Name:"",
    Email_Address:"", 
    Card_UID:"",
    Card_SN:"", 
    Type:"", 
    Balance:"", 
  },
    {
      Name:"",
      Email_Address:"", 
      Card_UID:"",
      Card_SN:"", 
      Type:"", 
      Balance:"", 
    },
    {
      Name:"",
      Email_Address:"", 
      Card_UID:"",
      Card_SN:"", 
      Type:"", 
      Balance:"", 
    },
    
  ];
  const columns = [
    {
      name: 'NAME',
      selector: 'name',
      sortable: true,
    
    },
    // {
    //   name: 'Email Address',
    //   selector: 'email',
    //   sortable: true,
    // },
    {
      name: 'CARD UID',
      selector: 'cardId',
      sortable: true,
    },
    {
      name: 'CARD SN',
      selector: 'Card_SN',
      sortable: true,
    },
    {
      name: 'TYPE',
      selector: 'Type',
      sortable: true,
    },
    {
      name: 'BALANCE',
      selector: 'balance',
      sortable: true,
    },


  ];
  const options = [
    { value: 'SND', label: 'SND' },
    { value: 'SNR', label: 'SNR' },
    { value: 'Alltransactions', label: 'All Transactions' },
  ];

function Wallet() {
  const [startDate, setStartDate] = useState(new Date());
  const initialFormValue = {
    'name': '',
    'status': 'active'
}
  const [ridewallet, setrideWallet] = useState();
  const [formValue, setFormValue] = useState(initialFormValue);

  const getriderWallet = async () => {
    const { status, loading, message, result } = await getRiderWallet();
    console.log('setrideHistory', result)
    if (status == 'success') {
      console.log('status', status)
      setrideWallet(result)
    }
    else {

    }


  }

  useEffect(() => {
    getriderWallet();
  },[]
  )
    return (
        <div>
           <div className="row">
             <div className="col-lg-12">
             <h1 className="tiltl_head_tetx">Wallet</h1>
             </div>
             <div className="col-lg-1">
              
              
           
              </div>
            <div className="col-lg-3">
              
              
           
            </div>
            <div className="col-lg-3">
              <div className="justify-content-around d-flex">
             
              <div className="paddin_sss w-100">
                <label></label>
                <Select
                options={options}
              />
              </div>
              </div>
          
            
            </div>
            <div className="col-lg-5">
              <div className="search_download ">
                <div className="input_1">
                    <input type="text" placeholder="Filter in Records..." />
                    <i class="fas fa-search"></i>
                </div>
                <div>
                  <button>Download <i class="fas fa-download"></i></button>
                </div>
              </div>
            </div>
            </div> 
            <div className="row">
              <div className="col-lg-12">
          <DataTable columns={columns} data={ridewallet} noHeader            noDataComponent="Loading..." 
 className="table_new_s"  pagination={true} paginationPerPage="5" paginationRowsPerPageOptions={[5, 10, 15, 20]} />

              </div>
            </div>
          <div className="row">
            <div className="col-lg-12">
              <a href="" className="class_a" data-toggle="modal" data-target="#Editdetais"> <i class="fas fa-plus"></i> Add</a>
            </div>
          </div>




        </div>

        
    )
}

export default Wallet
