import React, { useState } from "react";
import DataTable from 'react-data-table-component';
import DatePicker from "react-datepicker";
import Select from 'react-select';
import "react-datepicker/dist/react-datepicker.css";
import { CKEditor } from 'ckeditor4-react';
import SelectNew from 'react-select'

// Trade History Table
const multioptions = [
  { value: 'Provincial_Bus', label: 'Provincial Bus' },
  { value: 'City_Bus', label: 'City Bus' },
  { value: 'Jeepney_Class_2', label: 'Jeepney Class 2' }
]


const data = [
    {
      DEVICE_NUMBER:"",
      TYPE:<p>TPS 530</p>, 
      Status:<div className="status_section"><a href="" className="color_inactive"> Inactive</a></div>,
      Action:<div className="tik_flex_img"><a href="" data-toggle="modal" data-target="#edit_section"><i class="far fa-edit"></i></a></div>, 
      
    },
    {
      DEVICE_NUMBER:"",
      TYPE:<p>TPS 530</p>, 
      Status:<div className="status_section"><a href="" className="color_active"> Active</a></div>,
      Action:<div className="tik_flex_img"><a href="" data-toggle="modal" data-target="#edit_section"><i class="far fa-edit"></i></a></div>, 
      
    },
    {
      DEVICE_NUMBER:"",
      TYPE:<p>TPS 530</p>, 
      Status:<div className="status_section"><a href="" className="color_inactive"> Inactive</a></div>,
      Action:<div className="tik_flex_img"><a href="" data-toggle="modal" data-target="#edit_section"><i class="far fa-edit"></i></a></div>, 
      
    },
    {
      DEVICE_NUMBER:"",
      TYPE:<p>TPS 530</p>, 
      Status:<div className="status_section"><a href="" className="color_inactive"> Inactive</a></div>,
      Action:<div className="tik_flex_img"><a href="" data-toggle="modal" data-target="#edit_section"><i class="far fa-edit"></i></a></div>, 
      
    },
    {
      DEVICE_NUMBER:"",
      TYPE:<p>TPS 530</p>, 
      Status:<div className="status_section"><a href="" className="color_active"> Active</a></div>,
      Action:<div className="tik_flex_img"><a href="" data-toggle="modal" data-target="#edit_section"><i class="far fa-edit"></i></a></div>, 
      
    },
    {
      DEVICE_NUMBER:"",
      TYPE:<p>TPS 530</p>, 
      Status:<div className="status_section"><a href="" className="color_active"> Active</a></div>,
      Action:<div className="tik_flex_img"><a href="" data-toggle="modal" data-target="#edit_section"><i class="far fa-edit"></i></a></div>, 
      
    },
   
    
  // { slno: "1", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "2", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "3", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "4", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "5", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "6", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "7", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "8", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "9", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "10", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  ];
  const columns = [
    {
      name: 'DEVICE NUMBER',
      selector: 'DEVICE_NUMBER',
      sortable: true,
    
    },
    {
      name: 'TYPE',
      selector: 'TYPE',
      sortable: true,
    },
    {
      name: 'Status',
      selector: 'Status',
      sortable: true,
    },
    {
      name: 'Action',
      selector: 'Action',
      sortable: true,
    },
    
  ];
  const options = [
    { value: 'SND', label: 'SND' },
    { value: 'SNR', label: 'SNR' },
    { value: 'Alltransactions', label: 'All transactions' },
  ];

function Test() {
  const [startDate, setStartDate] = useState(new Date());

    return (
        <div>
           <div className="row mb-3">
             <div className="col-lg-12">
             <h1 className="tiltl_head_tetx">Device</h1>
             </div>
            <div className="col-lg-1">

            </div>
            <div className="col-lg-3">
              
              
           
            </div>
            <div className="col-lg-3">
              
            
            </div>
            <div className="col-lg-5">
              <div className="search_download">
                <div className="input_1">
                    <input type="text" placeholder="Filter in Records..." />
                    <i class="fas fa-search"></i>
                </div>
                <div>
                  <button>Download <i class="fas fa-download"></i></button>
                </div>
              </div>
            </div>
            </div> 
            <div className="row">
              <div className="col-lg-12">
          <DataTable columns={columns} data={data} noHeader  className="table_new_s"  pagination={true} paginationPerPage="5" paginationRowsPerPageOptions={[5, 10, 15, 20]} />

              </div>
            </div>
          <div className="row">
            <div className="col-lg-12">
              <a href="" className="class_a" data-toggle="modal" data-target="#Add_vehile"> <i class="fas fa-plus"></i> Add</a>
            </div>
          </div>
{/* 
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#newmsg">
  Open modal
</button> */}



<div class="modal" id="Add_vehile">
  <div class="modal-dialog modal-dialog-centered modal-md">
    <div class="modal-content">


      <div class="modal-header">
        <h4 class="modal-title model_heder_colo">Edit Status</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>


      <div class="modal-body edit_section_d_model">
       
       <div className="row mb-3">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Device Number</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="input_box_msg">
          <input type="text" />

          </div>
        </div>
       </div>

       <div className="row mb-3">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Device Type</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="input_box_msg">
         
          <SelectNew
      closeMenuOnSelect={false}
      
      defaultValue={[multioptions[4], multioptions[5]]}
      isMulti
      options={multioptions}
    />
          </div>
        </div>
       </div>
       

       <div className="row mb-3">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Status</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="content_clolk model_section">
          <div class="form-check form-check-inline d-flex align-items-center">
            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1" />
            <label class="form-check-label" for="inlineRadio1">Active</label>
          </div>
          <div class="form-check form-check-inline d-flex align-items-center">
            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2" />
            <label class="form-check-label" for="inlineRadio2">Inactive</label>
          </div>
          </div>
        </div>
       </div>

        
       
      </div>

      <div class="modal-footer  align-items-end">
     
        <div className="button_section_model">
      
        
        <button type="button" class="btn sendmsg" data-dismiss="modal">Save</button>
        </div>
       
      </div>

    </div>
  </div>
</div>



<div class="modal" id="edit_section">
  <div class="modal-dialog modal-dialog-centered modal-md">
    <div class="modal-content">


      <div class="modal-header">
        <h4 class="modal-title model_heder_colo">Add Device</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>


      <div class="modal-body edit_section_d_model">
       
      <div class="modal-body edit_section_d_model">
       
       <div className="row mb-3">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Device Number</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="input_box_msg">
          <input type="text" />

          </div>
        </div>
       </div>

       <div className="row mb-3">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Device Type</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="input_box_msg">
         
          <SelectNew
      closeMenuOnSelect={false}
      
      defaultValue={[multioptions[4], multioptions[5]]}
      isMulti
      options={multioptions}
    />
          </div>
        </div>
       </div>
       

       <div className="row mb-3">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Status</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="content_clolk model_section">
          <div class="form-check form-check-inline d-flex align-items-center">
            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1" />
            <label class="form-check-label" for="inlineRadio1">Active</label>
          </div>
          <div class="form-check form-check-inline d-flex align-items-center">
            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2" />
            <label class="form-check-label" for="inlineRadio2">Inactive</label>
          </div>
          </div>
        </div>
       </div>

        
       
      </div>
        
       
      </div>

      <div class="modal-footer  align-items-end">
     
        <div className="button_section_model">
      
        
        <button type="button" class="btn sendmsg" data-dismiss="modal">Save</button>
        </div>
       
      </div>

    </div>
  </div>
</div>

        </div>
    )
}

export default Test
