import React, { useState, useEffect } from "react";
import DataTable from 'react-data-table-component';
import DatePicker from "react-datepicker";
import Select from 'react-select';
import "react-datepicker/dist/react-datepicker.css";
import { CKEditor } from 'ckeditor4-react';
import { getAllService, editService, addService, getserviceCSV} from '../actions/service';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

// Trade History Table
const data = [
  {
    Service_Type: "Provincial Bus",
    TOTAL_UNITS: "",
    Status: <div className="status_section"><a href="" className="color_active"> Active</a></div>,
    Action: <div className="tik_flex_img"><a href="" data-toggle="modal" data-target="#edit_section"><i class="far fa-edit"></i></a></div>,

  },
  {
    Service_Type: "City Bus",
    TOTAL_UNITS: "",
    Status: <div className="status_section"><a href="" className="color_inactive"> Inactive</a></div>,
    Action: <div className="tik_flex_img"><a href="" data-toggle="modal" data-target="#edit_section"><i class="far fa-edit"></i></a></div>,

  },
  {
    Service_Type: "Jeepney Class 2",
    TOTAL_UNITS: "",
    Status: <div className="status_section"><a href="" className="color_active"> Active</a></div>,
    Action: <div className="tik_flex_img"><a href="" data-toggle="modal" data-target="#edit_section"><i class="far fa-edit"></i></a></div>,

  },
  {
    Service_Type: "UV Express Class 3",
    TOTAL_UNITS: "",
    Status: <div className="status_section"><a href="" className="color_inactive"> Inactive</a></div>,
    Action: <div className="tik_flex_img"><a href="" data-toggle="modal" data-target="#edit_section"><i class="far fa-edit"></i></a></div>,

  },
  {
    Service_Type: "Airline",
    TOTAL_UNITS: "",
    Status: <div className="status_section"><a href="" className="color_active"> Active</a></div>,
    Action: <div className="tik_flex_img"><a href="" data-toggle="modal" data-target="#edit_section"><i class="far fa-edit"></i></a></div>,

  },
  {
    Service_Type: "Tricycle",
    TOTAL_UNITS: "",
    Status: <div className="status_section"><a href="" className="color_inactive"> Inactive</a></div>,
    Action: <div className="tik_flex_img"><a href="" data-toggle="modal" data-target="#edit_section"><i class="far fa-edit"></i></a></div>,

  },
  // { slno: "1", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "2", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "3", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "4", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "5", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "6", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "7", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "8", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "9", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "10", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
];


//   const kycHistory = async () => {
//     console.log(id,"ffffffffffffff")
//     var test = await getKycHisotory(id);
//     if (test.userValue!=undefined) {
//      console.log("test",test.userValue);
//      setuserdet(test.userValue);
//  }else{

//  }

// }

function VehicleService() {


  const columns = [
    {
      name: 'SERVICE TYPE',
      selector: 'name',
      sortable: true,
  
    },
    {
      name: 'TOTAL UNITS',
      selector: 'vehicleCnt',
      sortable: true,
    },
    {
      name: 'STATUS',
      selector: 'status',
      sortable: true,
    },
    {
      name: 'ACTION',
      selector: 'action',
      sortable: true,
      cell: record => {
        // console.log('record', record)
  
        return (
          // <Fragment>
            <button
              data-tip="Edit"
              data-toggle="modal"
              data-target="#edit_section"
              className="tik_flex_img"
              onClick = {() => editR(record)}   
              style={{ marginRight: "5px" }}
            >
              <i className="fa fa-edit"></i>
            </button>
  
  
  
  
          // </Fragment>
        );
      }
    },
  
  ];

  const initialFormValue = {
    'name': '',
    'status': 'active'
  
}
  const [vehicle, setVehicleDatas] = useState();
  const [serviceType, setServiceType] = useState();
  const [count, setCount] = useState();
  const [formValue, setFormValue] = useState(initialFormValue);

  const [startDate, setStartDate] = useState(new Date());
  const getVehicleService = async (reqData) => {
    const { status, loading, message, result, count} = await getAllService(reqData);
    console.log('getAllservice', result)
    if (status == 'success') {
      console.log('status', status)
      console.log('count',count)
      setVehicleDatas(result)
      setCount(count)

    }
    else {

    }
  }

  const handleChange = (e) => {
    // e.preventDefault();
    console.log('1e', e)

    const { id, value } = e.target;
    // const { name} = e.value
// alert()
      const target = e.target;
      if (target.checked) {
        console.log('ddds',target.checked)
        console.log('target.value', target.value)
        // setFormValue(target.value);
        setFormValue({'status' : target.value})

      }
  
    let formData = { ...formValue, ...{ [id]: value } }
    console.log('formData', formData)
    setFormValue(formData)
  }
  const handleSearch = async (e) => {
    console.log('e', e.target.value)
    let reqData = {
      page: 1,
      limit: 10,
      search: e.target.value,
      // from: startDate,
      // to: endDate
    }
    getVehicleService(reqData)
  }
  const handleDownloadCSV = async () => {
    // const { page, limit, search, from, to } = this.state;
    let reqData = {
      records: [],
      search: '',
      options: [],
      page: 1,
      limit: 10,
      count: 0,
      loader: false,
      from: undefined,
      to: undefined,
      selectedOption: null,
      // search,
      // from = 1,
      // to
    }
    getserviceCSV(reqData)
  }
  
  const { name, status } = formValue;

  useEffect(() => {
    getVehicleService();
  }, [])


  const options = [
    { value: formValue.name, label: formValue.name,  },
    // { value: 'SNR', label: 'SNR' },
    // { value: 'Alltransactions', label: 'All Transactions' },
  ];
  console.log('options',options, serviceType)

  
  const editR = async (record) => {
 
    // const { status, loading, message, result, count} = await getAllService();
    // console.log('getAllservice', result)
    // if (status == 'success') {
    //   let Data= []

    //   result && result.length > 0 && result.map((item, key) => {
    //     // console.log('item',item)
    //     Data = item.name;
    //  console.log('Data',Data)
    console.log('record-----', record)
    setFormValue(record)
    
        // setServiceType(Data)
    //   // setCount(count)
    // })
    // }
  }
//   const handleChange = (e) => {
//     e.preventDefault();
//     const { id, value } = e.target;
//     let formData = { ...formValue, ...{ [id]: value } }
//     setFormValue(formData)
// }
const handleOptionValue = (e) => {
  console.log('e.value',e.value)
  
}


const handleSubmit = async (e) => {
  console.log('formValue',formValue)
    e.preventDefault();
    try {
       
        let reqData = {
            'name': formValue.name,
            'status': formValue.status,
            'serviceId': formValue._id
        };

        const { status, loading, message, error } = await editService(reqData);
        if (status == "success") {
          getVehicleService()
    
          // toast('Admin information Updated Successfully');
          toast(message, {
            position: toast.POSITION.TOP_CENTER,
          });
          
          console.log('reqData123', reqData)
          // alert(message)
        }
        else if(error){
          console.log('err', error)
        }
      
    } catch (err) { }
}
const handleSubmitAdd = async (e) =>{
  e.preventDefault();


  let reqData = {
  name,
  'status': formValue.status,

  }
  const { result, error, status, message } = await addService(reqData)
  if (status == "success") {
    getVehicleService()

    // toast('Admin information Updated Successfully');
    toast(message, {
      position: toast.POSITION.TOP_CENTER,
    });
    
    console.log('reqData123', reqData)
    // alert(message)
  }
  else {
    // setError(error)
    // console.log('err', error)
  }
}
  return (
    <div>
      <div className="row mb-3">
        <div className="col-lg-12">
          <h1 className="tiltl_head_tetx">Vehicle Service</h1>
        </div>
        <div className="col-lg-1">

        </div>
        <div className="col-lg-3">



        </div>
        <div className="col-lg-3">
          <div className="paddin_sss">
            <label></label>
            <Select
            // name = {name}
            // value = {name}
              options={options}
            />
          </div>

        </div>
        <div className="col-lg-5">
          <div className="search_download">
            <div className="input_1">
              <input type="text" onChange={handleSearch} placeholder="Filter in Records..." />
              <i class="fas fa-search"></i>
            </div>
            <div>
              <button onClick={() => handleDownloadCSV()} >Download <i class="fas fa-download"></i></button>
            </div>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-lg-12">
          <DataTable columns={columns} data={vehicle} 
                     noDataComponent="Loading..." 
                     noHeader className="table_new_s" pagination={true} paginationPerPage="5" paginationRowsPerPageOptions={[5, 10, 15, 20]} />

        </div>
      </div>
      <div className="row">
        <div className="col-lg-12">
          <a href="" className="class_a" data-toggle="modal" data-target="#Add_vehile"> <i class="fas fa-plus"></i> Add</a>
        </div>
      </div>
      {/* 
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#newmsg">
  Open modal
</button> */}



      <div class="modal" id="Add_vehile">
        <div class="modal-dialog modal-dialog-centered modal-md">
          <div class="modal-content">


            <div class="modal-header">
              <h4 class="modal-title model_heder_colo">Add Vehicle Service</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>


            <div class="modal-body edit_section_d_model">

              <div className="row mb-3">
                <div className="col-lg-4 grif_place">
                  <div className="label_section">
                    <label>Service Type</label>
                  </div>

                </div>
                <div className="col-lg-8">
                  <div className="input_box_msg">
                    {/* <Select
                    // name= {name}
                    // value={name}
                      options={options}
                    /> */}
    <div className="input_box_msg">
                    <input type="text" onChange={handleChange} id="name" value={formValue.name} />

                  </div>
                  </div>
                </div>
              </div>

              {/* <div className="row mb-3">
                <div className="col-lg-4 grif_place">
                  <div className="label_section">
                    <label>Total Units</label>
                  </div>

                </div>
                <div className="col-lg-8">
                  <div className="input_box_msg">
                    <input type="text" />

                  </div>
                </div>
              </div> */}


              <div className="row mb-3">
                <div className="col-lg-4 grif_place">
                  <div className="label_section">
                    <label>Status</label>
                  </div>

                </div>
           
  
                                   
                <div className="col-lg-8">
                  <div className="content_clolk model_section">
                  <div class="form-check form-check-inline d-flex align-items-center">
                      <input class="form-check-input" type="radio" checked={formValue.status == 'active'}  onChange={handleChange} name='status'  id="status" value="active" />
                      <label class="form-check-label" for="inlineRadio1">Active</label>
                    </div>
                    <div class="form-check form-check-inline d-flex align-items-center">
                      <input class="form-check-input" type="radio" checked={formValue.status == 'deactive'} onChange={handleChange}  name="status" id="status" value="deactive" />
                      <label class="form-check-label" for="inlineRadio2">Inactive</label>
                    </div>
                  </div>
                </div>
              </div>



            </div>

            <div class="modal-footer  align-items-end">

              <div className="button_section_model">

                <button type="button" class="btn savemsg" data-dismiss="modal" >Close</button>

                <button type="button" onClick={handleSubmitAdd} class="btn sendmsg" data-dismiss="modal">Add</button>
              </div>

            </div>

          </div>
        </div>
      </div>
      <div class="modal" id="edit_section">
        <div class="modal-dialog modal-dialog-centered modal-md">
          <div class="modal-content">


            <div class="modal-header">
              <h4 class="modal-title model_heder_colo">Edit Vehicle Service</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>


            <div class="modal-body edit_section_d_model">

              <div className="row mb-3">
                <div className="col-lg-4 grif_place">
                  <div className="label_section">
                    <label>Service Type</label>
                  </div>

                </div>
                <div className="col-lg-8">
                  <div className="input_box_msg">
                    <Select
                    //  name={name}
                     onChange={handleOptionValue}
                     value={options.name}
                      options={options}
                      // value={options[0].value}

                    />

                  </div>
                </div>
              </div>

              {/* <div className="row mb-3">
                <div className="col-lg-4 grif_place">
                  <div className="label_section">
                    <label>Total Units</label>
                  </div>

                </div>
                <div className="col-lg-8">
                  <div className="input_box_msg">
                    <input type="text" />

                  </div>
                </div>
              </div> */}


              <div className="row mb-3">
                <div className="col-lg-4 grif_place">
                  <div className="label_section">
                    <label>Status</label>
                  </div>

                </div>
                <div className="col-lg-8">
                  <div className="content_clolk model_section">
                  <div class="form-check form-check-inline d-flex align-items-center">
                      <input class="form-check-input" type="radio" checked={formValue.status == 'active'}  onChange={handleChange} name='status'  id="status" value="active" />
                      <label class="form-check-label" for="inlineRadio1">Active</label>
                    </div>
                    <div class="form-check form-check-inline d-flex align-items-center">
                      <input class="form-check-input" type="radio" checked={formValue.status == 'deactive'} onChange={handleChange}  name="status" id="status" value="deactive" />
                      <label class="form-check-label" for="inlineRadio2">Inactive</label>
                    </div>
                  </div>
                </div>
              </div>



            </div>

            <div class="modal-footer  align-items-end">

              <div className="button_section_model">

                <button type="button" class="btn savemsg" data-dismiss="modal" >Close</button>

                <button type="button" onClick={handleSubmit} class="btn sendmsg" data-dismiss="modal">Save</button>
              </div>

            </div>

          </div>
        </div>
      </div>

    </div>
  )
}

export default VehicleService
