import React, { useState } from "react";
import DataTable from 'react-data-table-component';



// Trade History Table
const data = [
  {
    NAME:"", 
    ACTIVITY:"",
    DATE_ACCESSED :"", 

  },
  {
    NAME:"", 
    ACTIVITY:"",
    DATE_ACCESSED :"", 

  },
  
  {
    NAME:"", 
    ACTIVITY:"",
    DATE_ACCESSED :"", 

  },
   {
    NAME:"", 
    ACTIVITY:"",
    DATE_ACCESSED :"", 

  },
  {
    NAME:"", 
    ACTIVITY:"",
    DATE_ACCESSED :"", 

  },
  
 

  ];
  const columns = [
    {
      name: 'NAME',
      selector: 'NAME',
      sortable: true,
    
    },
    {
      name: 'ACTIVITY',
      selector: 'ACTIVITY',
      sortable: true,
    
    },
    {
      name: 'DATE ACCESSED',
      selector: 'DATE_ACCESSED',
      sortable: true,
    },
  ];
  const options = [
    { value: 'SND', label: 'SND' },
    { value: 'SNR', label: 'SNR' },
    { value: 'Alltransactions', label: 'All transactions' },
  ];

function Test() {
  const [startDate, setStartDate] = useState(new Date());

    return (
        <div>
           <div className="row">
             <div className="col-lg-12">
             <h1 className="tiltl_head_tetx">Admin Activity</h1>
            
             </div>
            <div className="col-lg-1">

            </div>
          
            <div className="col-lg-3">
           
              
           
            </div>
            <div className="col-lg-3">
           
            
            </div>
            
            <div className="col-lg-5 mb-3">
              <div className="search_download">
                <div className="input_1">
                    <input type="text" placeholder="Filter in Records..." />
                    <i class="fas fa-search"></i>
                </div>
                <div>
                  <button>Download <i class="fas fa-download"></i></button>
                </div>
              </div>
            </div>
            </div> 
            <div className="row">
              <div className="col-lg-12 ">
              <DataTable columns={columns} data={data} noHeader  className="table_new_s"  pagination={true} paginationPerPage="5" paginationRowsPerPageOptions={[5, 10, 15, 20]} />
           
                
              </div>
            </div>
        </div>
    )
}

export default Test
