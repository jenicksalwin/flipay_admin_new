import React, { useState, useEffect } from "react";
import DataTable from 'react-data-table-component';
import DatePicker from "react-datepicker";
import Select from 'react-select';
import "react-datepicker/dist/react-datepicker.css";
import { CKEditor } from 'ckeditor4-react';
import SelectNew from 'react-select'
import RangeSlider from '../components/RangeSlider/RangeSlider'
import { editVehicle, getAllVehicle, getVehicleCsv, addVehicle} from "../actions/vehicle";
import { getDeviceDropDown } from "../actions/device";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
// Trade History Table
const multioptions = [
  { value: 'Provincial_Bus', label: 'Provincial Bus' },
  { value: 'City_Bus', label: 'City Bus' },
  { value: 'Jeepney_Class_2', label: 'Jeepney Class 2' }
]

const data = [
    {
      Service_Type:"",
      Transport_Cooperative_Corporation:"", 
      TPS_530T_Passenger_Validator:"", 
      TPS_900_320_Drivers_Monitor:"",
      Maker:"", 
      Max_Amount:"",
      Plate_Number:"", 
      Chassis_Number:"", 
      Engine_Number:"",
      Distance_Travelled:"", 
      Action:<div className="tik_flex_img"><a href="" data-toggle="modal" data-target="#edit_details_v"><i class="far fa-edit"></i></a></div>, 
    },
    {
      Service_Type:"",
      Transport_Cooperative_Corporation:"", 
      TPS_530T_Passenger_Validator:"", 
      TPS_900_320_Drivers_Monitor:"",
      Maker:"", 
      Max_Amount:"",
      Plate_Number:"", 
      Chassis_Number:"", 
      Engine_Number:"",
      Distance_Travelled:"", 
      Action:<div className="tik_flex_img"><a href="" data-toggle="modal" data-target="#edit_details_v"><i class="far fa-edit"></i></a></div>, 
    },
  // { slno: "1", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "2", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "3", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "4", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "5", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "6", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "7", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "8", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "9", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "10", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  ];
 
  
function Test() {

  const columns = [
    
    {
      name: 'SERVICE TYPE',
      selector: 'serviceName',
      sortable: true,
    },
    {
      name: 'TRANSPORT COOPERATIVE/CORPORATION',
      selector: 'Transport_Cooperative_Corporation',
      sortable: true,
    },
    {
      name: 'PASSENGER VALIDATOR',
      selector: 'deviceId',
      sortable: true,
    },
    {
      name: 'DRIVER"S MONITOR',
      selector: 'driverDeviceId',
      sortable: true,
      
    },
    {
      name: 'MAKER',
      selector: 'maker',
      sortable: true,
    },
    {
      name: 'MAX AMOUNT',
      selector: 'maxAmount',
      sortable: true,
    },
    {
      name: 'PLATE NUMBER',
      selector: 'plateNo',
      sortable: true,
    },
    {
      name: 'CHASSIS NUMBER',
      selector: 'chassisNo',
      sortable: true,
    },
    {
      name: 'ENGINE NUMBER',
      selector: 'engineNo',
      sortable: true,
    },
    {
      name: 'DISTANCE TRAVELLED',
      selector: 'distanceTraveled',
      sortable: true,
    },
    {
      name: 'ACTION',
      selector: 'Action',
      sortable: true,
      cell: record => {
        // console.log('record', record)

        return (
          // <Fragment>
          <>
            <button
              data-tip="Edit"
              data-toggle="modal"
              data-target="#Editdetais"
              className="action_tab buttn_trans"

              onClick={() => editR(record)}
            // onClick={() => editRecord(record)}
            // style={{ marginRight: "5px" }}
            >
              <i className="fa fa-edit"></i>

            </button>

            <button
              data-tip="Edit"
              data-toggle="modal"
              data-target="#newmsg"
              className="action_tab buttn_trans"

              // onClick={() => editRecord(record._id)}
              style={{ marginRight: "5px" }}
            >
              <i className="fa fa-comments"></i>

            </button>
          </>

          // </Fragment>
        );
      }
    },
  
  ];
  const options = [
    { value: 'SND', label: 'SND' },
    { value: 'SNR', label: 'SNR' },
    { value: 'AllTrasactions', label: 'All Transactions' },
  ];
  const [startDate, setStartDate] = useState(new Date());
  const initialFormValue = {
    'serviceId': '',
    'deviceId': '',
    'maker': '',
    'maxAmount': '',
    'plateNo': '',
    'chassisNo': '',
    'engineNo': '',
    'distanceTraveled': '',
    'driverName': '',
    'driverDeviceId': '',
  }
  const [vehicleDet, setvehicleDet] = useState();
  const [Device, setDevice ] = useState();
  const [formValue, setFormValue] = useState(initialFormValue);
  const [maxAmountValue, setMaxAmount] = useState()
  const [distanceTraveledID, setDistancetravelID] = useState()
  const [errors, setError] = useState()

  const getVehicleDetails = async (reqData) => {
    const { status, loading, message, result } = await getAllVehicle(reqData);
    console.log('setvehicleDet', result)
    if (status == 'success') {
      console.log('status', status)

      setvehicleDet(result)
    }
    else {

    }


  }

  const fetchDevice = async () => {
    const { status,loading,message,result } =  await getDeviceDropDown();
    if (status == 'success') {
      console.log('status', status)

      setDevice(result)
    }
    else {
// console.log('err',err)
    }


  }

  const handleSearch = async (e) => {
    console.log('e', e.target.value)
    let reqData = {
      page: 1,
      limit: 10,
      search: e.target.value,
      // from: startDate,
      // to: endDate
    }
    getVehicleDetails(reqData)
  }
  const handleDownloadCSV = async () => {
    // const { page, limit, search, from, to } = this.state;
    let reqData = {
      records: [],
      search: '',
      options: [],
      page: 1,
      limit: 10,
      count: 0,
      loader: false,
      from: undefined,
      to: undefined,
      selectedOption: null,
      // search,
      // from = 1,
      // to
    }
    getVehicleCsv(reqData)
  }
  const editR = async (record) => {

    // e.preventDefault();
    // const { id, value } = e.target;
    // let formData = { ...formValue, ...{ [id]: value } }
    // setFormValue()    
    console.log('record-----', record)
    setFormValue(record)
    // setKycStatusNo(record.kycStatusNo)
  }
  const handleChange = (e) => {
     e.preventDefault();
    const { id, value } = e.target;
    let formData = { ...formValue, ...{ [id]: value } }
    console.log('formData',formData)
    setFormValue(formData)    
    
  }

  const sliderChange =(value)=>{
   console.log('valuesttt',value)
  //  setFormValue(value)
  //  setMaxAmount(value)
   setDistancetravelID(value)
  }

  const sliderChange1 =(value)=>{
    console.log('valuesttt',value)
   //  setFormValue(value)
    setMaxAmount(value)
    // setDistancetravelID(value)
   }
const {
  vehicleId,
  serviceId,
  deviceId,
  driverDeviceId,
  maker,
  maxAmount,
  plateNo,
  chassisNo,
  engineNo,
  distanceTraveled,
  driverName,

} = formValue
  const handleSubmit = async (e) => {

    // e.preventDefault();


    let reqData = {
      'vehicleId': formValue._id,
      'serviceId': formValue.serviceId,
      'deviceId': formValue.deviceId,
      'driverDeviceId': formValue.driverDeviceId,
      'maker': formValue.maker,
      'maxAmount': formValue.maxAmount,
      'plateNo': formValue.plateNo,
      'chassisNo': formValue.chassisNo,
      'engineNo': formValue.engineNo,
      'distanceTraveled': formValue.distanceTraveled,
      'driverName': formValue.driverName,
  
    }
    const { result, error, status, message } = await editVehicle(reqData)
    if (status == "success") {
      getVehicleDetails()

      // toast('Admin information Updated Successfully');
      toast(message, {
        position: toast.POSITION.TOP_CENTER,
      });
      
      console.log('reqData123', reqData)
      // alert(message)
    }
    else {
      console.log('err', error)
    }
  }

  const handleSubmitAdd = async (e) => {

    e.preventDefault();


    let reqData = {
      vehicleId,
      serviceId,
      deviceId,
      driverDeviceId,
      maker,
      maxAmountValue,
      plateNo,
      chassisNo,
      engineNo,
      distanceTraveledID,
      driverName,
  
    }
    const { result, error, status, message } = await addVehicle(reqData)
    if (status == "success") {
      // getVehicleDetails()

      // toast('Admin information Updated Successfully');
      toast(message, {
        position: toast.POSITION.TOP_CENTER,
      });
      
      console.log('reqData123', reqData)
      // alert(message)
    }
    else {
      setError(error)
      console.log('err', error)
    }
  }
  // const handleStartDate = (e) => {
  //   console.log('e', e)
  //   setStartDate(e)
  // }
  // const handleEndDate = async (e) => {
  //   console.log('startDate', startDate)
  //   console.log('endDate', endDate)
  //   setEndDate(e)
  //   let reqData = {
  //     page: 1,
  //     limit: 10,
  //     search: "",
  //     from: startDate,
  //     to: endDate
  //   }
  //   getriderHistory(reqData)
  //   console.log(reqData, 'reqData')
  // }


  useEffect(() => {
    getVehicleDetails();
    fetchDevice();
  }, []
  )
    return (
        <div>
           <div className="row mb-3">
             <div className="col-lg-12">
             <h1 className="tiltl_head_tetx">Vehicle</h1>
             </div>
            <div className="col-lg-1">

            </div>
            <div className="col-lg-3">
              
              
           
            </div>
            <div className="col-lg-3">
              <div className="paddin_sss">
                <label></label>
                <Select
                options={options}
              />
              </div>
            
            </div>
            <div className="col-lg-5">
              <div className="search_download">
                <div className="input_1">
                    <input type="text" onChange={handleSearch} placeholder="Filter in Records..." />
                    <i class="fas fa-search"></i>
                </div>
                <div>
                  <button onClick={() => handleDownloadCSV()}>Download <i class="fas fa-download"></i></button>
                </div>
              </div>
            </div>
            </div> 
            <div className="row">
              <div className="col-lg-12">
          <DataTable columns={columns} data={vehicleDet} noHeader 
           className="table_new_s"  pagination={true} paginationPerPage="5" 
           paginationRowsPerPageOptions={[5, 10, 15, 20]} 
           noDataComponent="Loading..." 
           />

              </div>
            </div>
          <div className="row">
            <div className="col-lg-12">
              <a href="" className="class_a" data-toggle="modal" data-target="#add_details_v"> <i class="fas fa-plus"></i> Add</a>
            </div>
          </div>
{/* 
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#newmsg">
  Open modal
</button> */}



<div class="modal" id="Editdetais">
  <div class="modal-dialog modal-dialog-centered modal-md">
    <div class="modal-content">


      <div class="modal-header">
        <h4 class="modal-title model_heder_colo">Edit Details</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>


      <div class="modal-body edit_section_d_model">
      <div className="row mb-3">
        <div className="col-lg-5 grif_place">
          <div className="label_section">
          <label>Service Type</label>
          </div>
            
        </div>
        <div className="col-lg-7">
          <div className="input_box_msg">
          <SelectNew
      closeMenuOnSelect={false}
      defaultValue={[multioptions[4], multioptions[5]]}
      isMulti
      options={multioptions}
    />

          </div>

          <span className="text-danger">{errors && errors.serviceId}</span>

        </div>
       </div>
       <div className="row mb-3">
        <div className="col-lg-5 grif_place">
          <div className="label_section">
          <label>Transport Cooperative /Corporation</label>
          </div>
            
        </div>
        <div className="col-lg-7">
          <div className="input_box_msg">
          <Select
                options={options}
              />

          </div>
        </div>
       </div>


       <div className="row mb-3">
        <div className="col-lg-5 grif_place">
          <div className="label_section">
          <label>TPS 530T <br />(Passenger Validator)</label>
          </div>
            
        </div>
        <div className="col-lg-7">
          <div className="input_box_msg">
          <input type="text" id="deviceId" name= "deviceId" onChange={handleChange} value={formValue.deviceId} />
          <span className="text-danger">{errors && errors.deviceId}</span>

          </div>
        </div>
       </div>
       <div className="row mb-3">
        <div className="col-lg-5 grif_place">
          <div className="label_section">
          <label>TPS 900/320 <br />
 (Drivers Monitor)</label>
          </div>
            
        </div>
        <div className="col-lg-7">
          <div className="input_box_msg">
          <input type="text" id="driverDeviceId" onChange={handleChange} value={formValue.driverDeviceId} />
          <span className="text-danger">{errors && errors.driverDeviceId}</span>

          </div>
        </div>
       </div>
       <div className="row mb-3">
        <div className="col-lg-5 grif_place">
          <div className="label_section">
          <label>Maker</label>
          </div>
            
        </div>
        <div className="col-lg-7">
          <div className="input_box_msg">
          <input type="text" id="maker" onChange={handleChange} value={formValue.maker} />
         
          </div>
        </div>
       </div>
       <div className="row mb-3">
        <div className="col-lg-5 grif_place">
          <div className="label_section">
          <label>Max Amount (Php)</label>
          </div>
            
        </div>
        <div className="col-lg-7">
          <div className="input_box_msg">
          <RangeSlider handleRangeValue={handleChange}/>
         
          </div>
        </div>
       </div>
       <div className="row mb-3">
        <div className="col-lg-5 grif_place">
          <div className="label_section">
          <label>Plate Number</label>
          </div>
            
        </div>
        <div className="col-lg-7">
          <div className="input_box_msg">
          <input type="text" id="plateNo" onChange={handleChange} value={formValue.plateNo}
            // error={errors.plateNo}
                                        // className={classnames("form-control", {
                                        //     invalid: errors.plateNo
                                        // })} 
                                        />
                                    {/* <span className="text-danger">{errors.plateNo}</span>  */}
         
          </div>
        </div>
       </div>
       <div className="row mb-3">
        <div className="col-lg-5 grif_place">
          <div className="label_section">
          <label>Chassis Number</label>
          </div>
            
        </div>
        <div className="col-lg-7">
          <div className="input_box_msg">
          <input type="text" id="chassisNo" onChange={handleChange} value={formValue.chassisNo} />
         
          </div>
        </div>
       </div>
       <div className="row mb-3">
        <div className="col-lg-5 grif_place">
          <div className="label_section">
          <label>Engine Number</label>
          </div>
            
        </div>
        <div className="col-lg-7">
          <div className="input_box_msg">
          <input type="text" id="driverDeviceId" onChange={handleChange} value={formValue.engineNo} />
         
          </div>
        </div>
       </div>
       <div className="row mb-3">
        <div className="col-lg-5 grif_place">
          <div className="label_section">
          <label>Distance Travelled <br />
 (Km)</label>
          </div>
            
        </div>
        <div className="col-lg-7">
          <div className="input_box_msg">
          <RangeSlider sliderChange={sliderChange}  />
         
          </div>
        </div>
       </div>

      

     

      

        
       
      </div>

      <div class="modal-footer  align-items-end">
     
        <div className="button_section_model">
      
        <button type="button" class="btn savemsg" data-dismiss="modal" >Close</button>
        
        <button type="button" onClick={handleSubmit} class="btn sendmsg" data-dismiss="modal">Save</button>
        </div>
       
      </div>

    </div>
  </div>
</div>
<div class="modal" id="add_details_v">
  <div class="modal-dialog modal-dialog-centered modal-md">
    <div class="modal-content">


      <div class="modal-header">
        <h4 class="modal-title model_heder_colo">Add Details</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>


      <div class="modal-body edit_section_d_model">
      <div className="row mb-3">
        <div className="col-lg-5 grif_place">
          <div className="label_section">
          <label>Service Type</label>
          </div>
            
        </div>
        <div className="col-lg-7">
          <div className="input_box_msg">
          <SelectNew
      closeMenuOnSelect={false}
      
      defaultValue={[multioptions[4], multioptions[5]]}
      isMulti
      options={multioptions}
    />
          </div>
        </div>
       </div>
       <div className="row mb-3">
        <div className="col-lg-5 grif_place">
          <div className="label_section">
          <label>Transport Cooperative /Corporation</label>
          </div>
            
        </div>
        <div className="col-lg-7">
          <div className="input_box_msg">
          <Select
                options={options}
              />

          </div>
        </div>
       </div>


       <div className="row mb-3">
        <div className="col-lg-5 grif_place">
          <div className="label_section">
          <label>TPS 530T <br />(Passenger Validator)</label>
          </div>
            
        </div>
        <div className="col-lg-7">
          <div className="input_box_msg">
          <input type="text" />
         
          </div>
        </div>
       </div>
       <div className="row mb-3">
        <div className="col-lg-5 grif_place">
          <div className="label_section">
          <label>TPS 900/320 <br />
 (Drivers Monitor)</label>
          </div>
            
        </div>
        <div className="col-lg-7">
          <div className="input_box_msg">
          <input type="text"  />
         
          </div>
        </div>
       </div>
       <div className="row mb-3">
        <div className="col-lg-5 grif_place">
          <div className="label_section">
          <label>Maker</label>
          </div>
            
        </div>
        <div className="col-lg-7">
          <div className="input_box_msg">
          <input type="text" id="maker" onChange={handleChange} value={maker} />
          <span className="text-danger">{errors && errors.maker}</span>
          </div>
        </div>
       </div>
       <div className="row mb-3">
        <div className="col-lg-5 grif_place">
          <div className="label_section">
          <label>Max Amount (Php)</label>
          </div>
            
        </div>
        <div className="col-lg-7">
          <div className="input_box_msg">
          <RangeSlider sliderChange={sliderChange1} Slidervalues={maxAmountValue}  />
          <span className="text-danger">{errors && errors.maxAmountValue}</span>

          </div>
        </div>
       </div>
       <div className="row mb-3">
        <div className="col-lg-5 grif_place">
          <div className="label_section">
          <label>Plate Number</label>
          </div>
            
        </div>
        <div className="col-lg-7">
          <div className="input_box_msg">
          <input type="text" id="plateNo" onChange={handleChange} value={plateNo}
          />
      <span className="text-danger">{errors && errors.plateNo}</span>
         
          </div>
        </div>
       </div>
       <div className="row mb-3">
        <div className="col-lg-5 grif_place">
          <div className="label_section">
          <label>Chassis Number</label>
          </div>
            
        </div>
        <div className="col-lg-7">
          <div className="input_box_msg">
          <input type="text" id="chassisNo" onChange={handleChange} value={chassisNo}/>
          <span className="text-danger">{errors && errors.chassisNo}</span>

         
          </div>
        </div>
       </div>
       <div className="row mb-3">
        <div className="col-lg-5 grif_place">
          <div className="label_section">
          <label>Engine Number</label>
          </div>
            
        </div>
        <div className="col-lg-7">
          <div className="input_box_msg">
          <input type="text" id="engineNo" onChange={handleChange} value={engineNo}/>
          <span className="text-danger">{errors && errors.engineNo}</span>

          </div>
        </div>
       </div>
       <div className="row mb-3">
        <div className="col-lg-5 grif_place">
          <div className="label_section">
          <label>Distance Travelled <br />
 (Km)</label>
          </div>
            
        </div>
        <div className="col-lg-7">
          <div className="input_box_msg">

            
          <RangeSlider sliderChange={sliderChange}  Slidervalues1={distanceTraveledID} />
         
          </div>
        </div>
       </div>

      

     

      

        
       
      </div>

      <div class="modal-footer  align-items-end">
     
        <div className="button_section_model">
      
        <button type="button" class="btn savemsg" data-dismiss="modal" >Close</button>
        
        <button type="button" onClick={handleSubmitAdd}  class="btn sendmsg">Add</button>
        </div>
       
      </div>

    </div>
  </div>
</div>
        </div>
    )
}

export default Test
