import React, { useState, useEffect } from "react";
import DataTable from 'react-data-table-component';
import DatePicker from "react-datepicker";
import Select from 'react-select';
import "react-datepicker/dist/react-datepicker.css";
import { CKEditor } from 'ckeditor4-react';
import { getAllUsers, statusChanged, statusRejected } from '../actions/rider'
import config from '../actions/config'
import $ from 'jquery';

// Trade History Table
const data = [
  {
    Name: "",
    Account_Number: "",
    Email_Address: "",
    Mobile_Number: "",
    address: "",
    Photo_ID: <div className="tik_flex_img"><img src={require("../../assets/images/tick_img.png")} alt="logo" className="img-fluid inner_img_e" /><a href=""><i class="far fa-folder-open"></i></a><a href=""><i class="fas fa-download"></i></a></div>,
    Selfie_Verification: <div className="tik_flex_img"><img src={require("../../assets/images/tick_img.png")} alt="logo" className="img-fluid inner_img_e" /><a href=""><i class="far fa-folder-open"></i></a><a href=""><i class="fas fa-download"></i></a></div>,
    Business_Permit: <div className="tik_flex_img"><img src={require("../../assets/images/tick_img.png")} alt="logo" className="img-fluid inner_img_e" /><a href=""><i class="far fa-folder-open"></i></a><a href=""><i class="fas fa-download"></i></a></div>,
    Action: <div className="action_tab"><a href="" data-toggle="modal" data-target="#newmsg"><i class="far fa-comments"></i></a><a href="" data-toggle="modal" data-target="#Editdetais"><i class="fas fa-edit"></i></a></div>,
  },
  {
    Name: "",
    Account_Number: "",
    Email_Address: "",
    Mobile_Number: "",
    address: "",
    Photo_ID: <div className="tik_flex_img"><img src={require("../../assets/images/tick_img.png")} alt="logo" className="img-fluid inner_img_e" /><a href=""><i class="far fa-folder-open"></i></a><a href=""><i class="fas fa-download"></i></a></div>,
    Selfie_Verification: <div className="tik_flex_img"><img src={require("../../assets/images/tick_img.png")} alt="logo" className="img-fluid inner_img_e" /><a href=""><i class="far fa-folder-open"></i></a><a href=""><i class="fas fa-download"></i></a></div>,
    Business_Permit: <div className="tik_flex_img"><img src={require("../../assets/images/tick_img.png")} alt="logo" className="img-fluid inner_img_e" /><a href=""><i class="far fa-folder-open"></i></a><a href=""><i class="fas fa-download"></i></a></div>,
    Action: <div className="action_tab"><a href="" data-toggle="modal" data-target="#newmsg"><i class="far fa-comments"></i></a><a href="" data-toggle="modal" data-target="#Editdetais"><i class="fas fa-edit"></i></a></div>,
  },
  // { slno: "1", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "2", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "3", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "4", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "5", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "6", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "7", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "8", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "9", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "10", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
];

function Distributorkyc() {

  const columns = [
    {
      name: 'NAME',
      selector: 'firstName',
      sortable: true,
    },
    {
      name: 'ACCOUNT NUMBER',
      selector: 'accountNo',
      sortable: true,

    },
    {
      name: 'EMAIL ADDRESS',
      selector: 'email',
      sortable: true,
    },
    {
      name: 'MOBILE NUMBER',
      selector: 'phoneNo',
      sortable: true,
    },
    {
      name: 'KYC STATUS',
      selector: 'kycStatus',
      sortable: true,
    },
    {
      name: 'TYPE LOGIN',
      selector: 'typeLogin',
      sortable: true,
    },
    {
      name: 'PHOTO ID',
      selector: 'photoId',
      sortable: true,
      cell: record => {
        console.log('record', record)

        return (
          // <Fragment>
          <>
            <button
              className="tik_flex_img buttn_trans"
            >
              <i class="fas fa-check-circle"></i>
            </button>
            <button
              className="tik_flex_img buttn_trans"
            >
              <i class="far fa-folder-open "></i>
            </button>
            <button
              className="tik_flex_img buttn_trans"
            >
              <i class="fas fa-download"></i>
            </button>
          </>

          // </Fragment>
        );
      }
    },
    {
      name: 'SELFIE VERIFICATION',
      selector: 'Selfie_Verification',
      sortable: true,
      cell: record => {
        console.log('record', record)

        return (
          // <Fragment>
          <>
            <button
              className="tik_flex_img buttn_trans"
            >
              <i class="fas fa-check-circle"></i>
            </button>
            <button
              className="tik_flex_img buttn_trans"
            >
              <i class="far fa-folder-open "></i>
            </button>
            <button
              className="tik_flex_img buttn_trans"
            >
              <i class="fas fa-download"></i>
            </button>
          </>

          // </Fragment>
        );
      }

    },
    {
      name: 'BUSINESS PERMIT',
      selector: 'Business_Permit',
      sortable: true,
      cell: record => {
        console.log('record', record)

        return (
          // <Fragment>
          <>
            <button
              className="tik_flex_img buttn_trans"
            >
              <i class="fas fa-check-circle"></i>
            </button>
            <button
              className="tik_flex_img buttn_trans"
            >
              <i class="far fa-folder-open "></i>
            </button>
            <button
              className="tik_flex_img buttn_trans"
            >
              <i class="fas fa-download"></i>
            </button>
          </>

          // </Fragment>
        );
      }
    },

    {
      name: 'ACTION',
      selector: 'Action',
      sortable: true,
      cell: record => {
        // console.log('record', record)

        return (
          // <Fragment>
          <>
            <button
              data-tip="Edit"
              data-toggle="modal"
              data-target="#Editdetais"
              className="action_tab buttn_trans"

              onClick={() => editR(record)}
            // onClick={() => editRecord(record)}
            // style={{ marginRight: "5px" }}
            >
              <i className="fa fa-edit"></i>

            </button>

            <button
              data-tip="Edit"
              data-toggle="modal"
              data-target="#newmsg"
              className="action_tab buttn_trans"

              // onClick={() => editRecord(record._id)}
              style={{ marginRight: "5px" }}
            >
              <i className="fa fa-comments"></i>

            </button>
          </>

          // </Fragment>
        );
      }
    },
  ];
  const options = [
    { value: 'SND', label: 'SND' },
    { value: 'SNR', label: 'SNR' },
    { value: 'Alltransactions', label: 'All transactions' },
  ];

  const editR = async (record) => {

    // e.preventDefault();
    // const { id, value } = e.target;
    // let formData = { ...formValue, ...{ [id]: value } }
    // setFormValue()    
    console.log('record-----', record)
    setFormValue(record)
    setKycStatusNo(record.kycStatusNo)
  }
  const handleChange = (e) => {
    //  e.preventDefault();
    // const { id, value } = e.target;
    // let formData = { ...formValue, ...{ [id]: value } }
    // console.log('formData',formData)
    // setFormValue(formData)    
  }
  const handleChanges = (e) => {
    e.preventDefault();
    const { id, value } = e.target;
    let formData = { ...formValue, ...{ [id]: value } }
    console.log('formData', formData)
    setFormValue(formData)
  }
  const [startDate, setStartDate] = useState(new Date());
  const [distributorkyc, setDistributorkyc] = useState('')
  const [kycStatusNo, setKycStatusNo] = useState()
  const [rejectReason, setRejectModal] = useState(false)
  const initialFormValue = {

    _id: '',
    'firstName': '',
    'phonoNo': '',
    'photoId': '',
    'email': '',
    'selfieId': '',
    'accountNo': '',
    'businessPermit': '',
    'kycStatusNo': '',
    "reason": '',
    "rejectReason": false
  }
  const [formValue, setFormValue] = useState(initialFormValue);
  console.log('form', formValue)

  const getkycDistributor = async (reqData) => {
    const { result, status, error } = await getAllUsers(reqData);
    if (status == 'success') {
      setDistributorkyc(result)
      // setFormValue(result._id)
      // return 
    }
    else {
      console.log('erer', error)
    }
  }

  useEffect(() => {
    getkycDistributor();
  }, []
  )
  const handleSearch = async (e) => {
    console.log('e', e.target.value)
    let reqData = {
      page: 1,
      limit: 10,
      search: e.target.value,
      // from: startDate,
      // to: endDate
    }
    getkycDistributor(reqData)
  }
  const changeStatus = async (id) => {
    console.log('id', id)
    try {


      const { status, loading, message, result } = await statusChanged({ id: id });
      console.log("resultresultresult", result);
      console.log("message", message);
      if (status == 'success') {
        // toast("Approved Successfully");
        setFormValue(result)
        // this.setState({ 'records': result })
      }

    } catch (err) {
      console.log('err', err)
    }

  }

  const reasonRejected = async (id) => {
    try {
      let data = {

        "id": id,
        "rejectReason": formValue.reason
      }

      const { status, loading, message, result } = await statusRejected(data);
      console.log('rrr', result)

      if (status == 'success') {
        setFormValue(result)
      }
    }
    catch (err) {

    }
  }

  const changeRejected = async (id) => {

    // alert(rejectReason)

    try {
      //     alert(id);
      // this.setState({'rButton':true})

      $('#rejectReason1').modal('show');
      $('#Editdetais').modal('hide');
      // const { status, loading, message, result } = await statusRejected({id:id});
      // console.log("resultresultresult",result);
      // if (status == 'success') {
      //   setFormValue(result)
      // setRejectModal({rejectReason : true})       

      // }
    } catch (err) { }

  }

  return (
    <div>
      <div className="row mb-3">
        <div className="col-lg-12">
          <h1 className="tiltl_head_tetx">Distributor</h1>
        </div>
        <div className="col-lg-1">

        </div>
        <div className="col-lg-3">



        </div>
        <div className="col-lg-3">


        </div>
        <div className="col-lg-5">
          <div className="search_download">
            <div className="input_1">
              <input type="text" onChange={handleSearch} placeholder="Filter in Records..." />
              <i class="fas fa-search"></i>
            </div>
            {/* <div>
              <button>Download <i class="fas fa-download"></i></button>
            </div> */}
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-lg-12">
          <DataTable columns={columns} 
          data={distributorkyc} 
          noHeader className="table_new_s"
           pagination={true} paginationPerPage="5" paginationRowsPerPageOptions={[5, 10, 15, 20]}
           noDataComponent="Loading..." 
           />

        </div>
      </div>
      <div className="row">
        <div className="col-lg-12">
          {/* <a href="" className="class_a" data-toggle="modal" data-target="#add_detais"> <i class="fas fa-plus"></i> Add</a> */}
        </div>
      </div>
      {/* 
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#newmsg">
  Open modal
</button> */}


      <div class="modal" id="newmsg">
        <div class="modal-dialog modal-dialog-centered modal-md modal-dialog_cutom_width">
          <div class="modal-content">


            <div class="modal-header">
              <h4 class="modal-title model_heder_colo">New Message</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>


            <div class="modal-body">
              <div className="row">
                <div className="col-lg-12">
                  <div className="send_msg_form">
                    <label>Recepient:</label>
                    <input type="text" />
                  </div>
                  <div className="send_msg_form">
                    <label>Subject:</label>
                    <input type="text" />
                  </div>
                </div>
              </div>
              <div className="div_clas_po">
                <label>Text:</label>
                <CKEditor initData={<p>This is an example CKEditor 4 WYSIWYG editor instance.</p>} />
              </div>

            </div>

            <div class="modal-footer justify-content-between align-items-end">
              <div className="signature_sectooion">
                <label>signature</label>
                <div className="signature_filed">
                  <div className="bottom_btn">
                    <a href="">Change</a>
                    <a href="">Remove</a>
                  </div>
                </div>
              </div>
              <div className="button_section_model">
                <button type="button" class="btn dlelte" data-dismiss="modal">Delete</button>
                <button type="button" class="btn savemsg" >Save to Drafts</button>

                <button type="button" class="btn sendmsg" data-dismiss="modal">Send Message</button>
              </div>

            </div>

          </div>
        </div>
      </div>
      {/* <Modal show={ trButto} onHide={this.handleClose1} ></Modal> */}
      {/* { rejectReason == true && */}
      <div class="modal" id="rejectReason1">
        <div class="modal-dialog modal-dialog-centered modal-md">
          <div class="modal-content">

            <div class="modal-header">
              <h4 class="modal-title model_heder_colo">Reject Reason</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body edit_section_d_model">

              <div className="row mb-3">
                <div className="col-lg-4 grif_place">
                  <div className="label_section">
                    <label>Reason</label>
                  </div>

                </div>
                <div className="col-lg-8">
                  <div className="input_box_msg">
                    <input type="text" id="reason" onChange={handleChanges} value={formValue.reason} />

                  </div>
                </div>
              </div>
            </div>

            <div className="button_section_model">
              {/* <button type="button" onClick={() => changeStatus(formValue._id)} class="btn savemsg" data-dismiss="modal" >Accept</button> */}

              <button type="button" onClick={() => reasonRejected(formValue._id)} class="btn sendmsg" data-dismiss="modal">Reject</button>
            </div>

          </div>
        </div>
      </div>
      {/* } */}

      <div class="modal" id="Editdetais">
        <div class="modal-dialog modal-dialog-centered modal-md">
          <div class="modal-content">


            <div class="modal-header">
              <h4 class="modal-title model_heder_colo">Edit Details</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>


            <div class="modal-body edit_section_d_model">

              <div className="row mb-3">
                <div className="col-lg-4 grif_place">
                  <div className="label_section">
                    <label>Name</label>
                  </div>

                </div>
                <div className="col-lg-8">
                  <div className="input_box_msg">
                    <input type="text" id="firstName" onChange={handleChange} value={formValue.firstName} />

                  </div>
                </div>
              </div>

              <div className="row mb-3">
                <div className="col-lg-4 grif_place">
                  <div className="label_section">
                    <label>Account Number</label>
                  </div>

                </div>
                <div className="col-lg-8">
                  <div className="input_box_msg">
                    <input type="text" id="accountNo" onChange={handleChange} value={formValue.accountNo} />

                  </div>
                </div>
              </div>

              <div className="row mb-3">
                <div className="col-lg-4 grif_place">
                  <div className="label_section">
                    <label>Mobile Number</label>
                  </div>

                </div>
                <div className="col-lg-8">
                  <div className="input_box_msg">
                    <input type="text" id="phoneNo" onChange={handleChange} value={formValue.phoneNo} />

                  </div>
                </div>
              </div>
              <div className="row mb-3">
                <div className="col-lg-4 grif_place">
                  <div className="label_section">
                    <label>Email Address</label>
                  </div>

                </div>
                <div className="col-lg-8">
                  <div className="input_box_msg">
                    <input type="text" id="email" onChange={handleChange} value={formValue.email} />

                  </div>
                </div>
              </div>
              {/* <div className="row">
                <div className="col-lg-4 grif_place">
                  <div className="label_section">
                    <label>Address</label>
                  </div>

                </div>
                <div className="col-lg-8">
                  <div className="input_box_msg">
                    <input type="text" />

                  </div>
                </div>
              </div> */}


              <div className="row">
                <div className="col-lg-4 grif_place">
                  <div className="label_section">
                    <label>Photo ID</label>
                  </div>

                </div>
                <div className="col-lg-8">
                  <div className="content_clolk">
                    <div className="img_section_78">
                      <img src={config.baseUrl + "kyc/" + formValue.photoId} alt="" width="40"
                        height="40" /> <br /> <br />
                      {/* <i class="far fa-image"></i> */}
                    </div>
                    <div className="image_options">
                      <a href={config.baseUrl + "kyc/" + formValue.photoId} target="_blank">View</a>
                      <a href=""> Upload</a>

                      <a href="" className="remover"> Remove</a>

                    </div>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-lg-4 grif_place">
                  <div className="label_section">
                    <label>Selfie Verification</label>
                  </div>

                </div>
                <div className="col-lg-8">
                  <div className="content_clolk">
                    <div className="img_section_78">
                      <img src={config.baseUrl + "kyc/" + formValue.selfieId} alt="" width="40"
                        height="40" /> <br /> <br />
                    </div>
                    <div className="image_options">
                      <a href={config.baseUrl + "kyc/" + formValue.selfieId} target="_blank"> View</a>
                      <a href=""> Upload</a>

                      <a href="" className="remover"> Remove</a>

                    </div>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-lg-4 grif_place">
                  <div className="label_section">
                    <label>Business Permit</label>
                  </div>

                </div>
                <div className="col-lg-8">
                  <div className="content_clolk">
                    <div className="img_section_78">
                      {/* <img src></img> */}
                      <img src={config.baseUrl + "kyc/" + formValue.businessPermit} alt="" width="40"
                        height="40" /> <br /> <br />
                    </div>
                    <div className="image_options">
                      <a href={config.baseUrl + "kyc/" + formValue.businessPermit} target="_blank"> View</a>
                      <a href=""> Upload</a>

                      <a href="" className="remover"> Remove</a>

                    </div>
                  </div>
                </div>
              </div>
            </div>
            {kycStatusNo == 2 &&

              <div class="modal-footer  align-items-end">

                <div className="button_section_model">
                  <button type="button" onClick={() => changeStatus(formValue._id)} class="btn savemsg" data-dismiss="modal" >Accept</button>

                  <button type="button" data-toggle="modal"
                    data-target="#rejectReason1" onClick={() => changeRejected(formValue._id)} class="btn sendmsg" data-dismiss="modal">Reject</button>
                </div>

              </div>
            }
          </div>
        </div>
      </div>
      {/* <div class="modal" id="add_detais">
        <div class="modal-dialog modal-dialog-centered modal-md">
          <div class="modal-content">


            <div class="modal-header">
              <h4 class="modal-title model_heder_colo">Add Distributor</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>


            <div class="modal-body edit_section_d_model">

              <div className="row mb-3">
                <div className="col-lg-4 grif_place">
                  <div className="label_section">
                    <label>Name</label>
                  </div>

                </div>
                <div className="col-lg-8">
                  <div className="input_box_msg">
                    <input type="text" />

                  </div>
                </div>
              </div>

              <div className="row mb-3">
                <div className="col-lg-4 grif_place">
                  <div className="label_section">
                    <label>Account Number</label>
                  </div>

                </div>
                <div className="col-lg-8">
                  <div className="input_box_msg">
                    <input type="text" />

                  </div>
                </div>
              </div>

              <div className="row mb-3">
                <div className="col-lg-4 grif_place">
                  <div className="label_section">
                    <label>Mobile Number</label>
                  </div>

                </div>
                <div className="col-lg-8">
                  <div className="input_box_msg">
                    <input type="text" />

                  </div>
                </div>
              </div>
              <div className="row mb-3">
                <div className="col-lg-4 grif_place">
                  <div className="label_section">
                    <label>Email Address</label>
                  </div>

                </div>
                <div className="col-lg-8">
                  <div className="input_box_msg">
                    <input type="text" />

                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-lg-4 grif_place">
                  <div className="label_section">
                    <label>Address</label>
                  </div>

                </div>
                <div className="col-lg-8">
                  <div className="input_box_msg">
                    <input type="text" />

                  </div>
                </div>
              </div>


              <div className="row">
                <div className="col-lg-4 grif_place">
                  <div className="label_section">
                    <label>Photo ID</label>
                  </div>

                </div>
                <div className="col-lg-8">
                  <div className="content_clolk">
                    <div className="img_section_78">
                      <i class="far fa-image"></i>
                    </div>
                    <div className="image_options">
                      <a href=""> View</a>
                      <a href=""> Upload</a>

                      <a href="" className="remover"> Remove</a>

                    </div>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-lg-4 grif_place">
                  <div className="label_section">
                    <label>Selfie Verification</label>
                  </div>

                </div>
                <div className="col-lg-8">
                  <div className="content_clolk">
                    <div className="img_section_78">
                      <i class="fas fa-file-pdf"></i>
                    </div>
                    <div className="image_options">
                      <a href=""> View</a>
                      <a href=""> Upload</a>

                      <a href="" className="remover"> Remove</a>

                    </div>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-lg-4 grif_place">
                  <div className="label_section">
                    <label>Business Permit</label>
                  </div>

                </div>
                <div className="col-lg-8">
                  <div className="content_clolk">
                    <div className="img_section_78">
                      <i class="fas fa-file-pdf"></i>
                    </div>
                    <div className="image_options">
                      <a href=""> View</a>
                      <a href=""> Upload</a>

                      <a href="" className="remover"> Remove</a>

                    </div>
                  </div>
                </div>
              </div>






            </div>

            <div class="modal-footer  align-items-end">

              <div className="button_section_model">

                <button type="button" class="btn savemsg" data-dismiss="modal" >Delete</button>

                <button type="button" class="btn sendmsg" data-dismiss="modal">Add</button>
              </div>

            </div>

          </div>
        </div>
      </div> */}
    </div>
  )
}

export default Distributorkyc
