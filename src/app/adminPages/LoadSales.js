import React, { useState } from "react";
import DataTable from 'react-data-table-component';
import DatePicker from "react-datepicker";
import Select from 'react-select';
import "react-datepicker/dist/react-datepicker.css";
import { CKEditor } from 'ckeditor4-react';

// Trade History Table
const data = [
  {
    PassengerNumber:"", 
    Date:"",
    NameofCompany:"", 
    DeviceID:"",
    ReferenceID:"", 
    PaymentMethod:"Driver", 
    Type:"Card",
    CardSN:"",
    Amount:"",
    ServiceFee:"",
    Status:<div className="status_section"><a href="" className="color_completed"> successful</a></div>,
  },
  {
    PassengerNumber:"", 
    Date:"",
    NameofCompany:"", 
    DeviceID:"",
    ReferenceID:"", 
    PaymentMethod:"Driver", 
    Type:"Card",
    CardSN:"",
    Amount:"",
    ServiceFee:"",
    Status:<div className="status_section"><a href="" className="color_completed"> successful</a></div>,

  },
  {
    PassengerNumber:"", 
    Date:"",
    NameofCompany:"", 
    DeviceID:"",
    ReferenceID:"", 
    PaymentMethod:"Driver", 
    Type:"Card",
    CardSN:"",
    Amount:"",
    ServiceFee:"",
    Status:<div className="status_section"><a href="" className="colot_po"> Unsuccessful</a></div>,
  },




  {
    PassengerNumber:"", 
    Date:"",
    NameofCompany:"", 
    DeviceID:"",
    ReferenceID:"", 
    PaymentMethod:"Driver", 
    Type:"Card",
    CardSN:"",
    Amount:"",
    ServiceFee:"",
    Status:<div className="status_section"><a href="" className="color_completed"> successful</a></div>,

  },

  ];
  const columns = [
    {
      name: 'PASSENGER NUMBER',
      selector: 'PassengerNumber',
      sortable: true,
    
    },
    {
      name: 'DATE',
      selector: 'Date',
      sortable: true,
    
    },
    {
      name: 'NAME OF COMPANY',
      selector: 'NameofCompany',
      sortable: true,
    },
    {
      name: 'DEVICE ID',
      selector: 'DeviceID',
      sortable: true,
    },
    {
      name: 'PAYMENT METHOD',
      selector: 'PaymentMethod',
      sortable: true,
    },
    {
      name: 'TYPE',
      selector: 'Type',
      sortable: true,
    },
    {
      name: 'CARD SN',
      selector: 'CardSN',
      sortable: true,
      
    },
    {
      name: 'AMOUNT',
      selector: 'Amount',
      sortable: true,
      
    },
    {
      name: 'SERVICEFEE',
      selector: 'ServiceFee',
      sortable: true,
      
    },
    
    {
      name: 'STATUS',
      selector: 'Status',
      sortable: true,
      
    },
    
  ];
  const options = [
    { value: 'SND', label: 'SND' },
    { value: 'SNR', label: 'SNR' },
    { value: 'Alltransactions', label: 'All Transactions' },
  ];

function Test() {
  const [startDate, setStartDate] = useState(new Date());

    return (
        <div>
           <div className="row">
             <div className="col-lg-12">
             <h1 className="tiltl_head_tetx">Load Sales</h1>
               <p className="clas_posoa">Description: Lorem ipsum dolor sit amet, consectetur adipiscing<br />
                elit. Eget egestas iaculis rhoncus, id et venenatis eu mauris.</p>
             
             </div>
            <div className="col-lg-1">

            </div>
            
            <div className="col-lg-3">
              <div className="d-flex justify-content-center">
              <div className="calaleder_section">
                <label>From</label>
                 <div className="datepiker_sec">
                 <DatePicker selected={startDate} onChange={(date) => setStartDate(date)} />
                 <i class="far fa-calendar-alt"></i>
                   </div> 
              </div>
              <div className="calaleder_section">
                <label>To</label>
                 <div className="datepiker_sec">
                 <DatePicker selected={startDate} onChange={(date) => setStartDate(date)} />
                 <i class="far fa-calendar-alt"></i>
                   </div> 
              </div>
              </div>
              
           
            </div>
            <div className="col-lg-3">
            <div className="paddin_sss w-100">
                <label></label>
                <Select
                options={options}
              />
              </div>
            
            </div>
            <div className="col-lg-5">
              <div className="search_download">
                <div className="input_1">
                    <input type="text" placeholder="Filter in Records..." />
                    <i class="fas fa-search"></i>
                </div>
                <div>
                  <button>Download <i class="fas fa-download"></i></button>
                </div>
              </div>
            </div>
            </div> 
            <div className="row">
              <div className="col-lg-12 table_new_one">
              <DataTable columns={columns} data={data} noHeader  className="table_new_s"  pagination={true} paginationPerPage="5" paginationRowsPerPageOptions={[5, 10, 15, 20]} />
               
              <div class="row new_table_total_filed">
              <div class="col">
                    
              </div>
              <div class="col border_left_0">
            
              </div>
              <div class="col border_left_0">
            
              </div>
              <div class="col border_left_0">
            
              </div>
              <div class="col border_left_0">
       
              </div>
              <div class="col border_left_0">
              
              </div>
              <div class="col border_left_0">
               
              </div>
              <div class="col border_left_0">
               
              </div>
              <div class="col border_left_0">
              <h2>Total</h2>
              </div>
              <div class="col row_bg_f">
              
              </div>
              <div class="col row_bg_f">
              
              </div>
            </div>
                
              </div>
            </div>
          





        </div>
    )
}

export default Test
