import React, { useState, useEffect } from "react";
import DataTable from 'react-data-table-component';
import DatePicker from "react-datepicker";
import Select from 'react-select';
import "react-datepicker/dist/react-datepicker.css";
import { CKEditor } from 'ckeditor4-react';
import { getRideHistory, getRideHistoryCsv } from "../actions/rider";

// Trade History Table
const data = [
  {
    Name: "",
    Card_SN: "",
    TPS_530T_Passenger_Validator: "",
    TPS_900_320_Driver_s_Device: "",
    Origin: "",
    Destination: "",
    Maximum_Fee: "",
    Ride_Fee: "",
    Travel_Distance_in_KM: "",
    End_Date: "",
    Start_Date: "",
    Status: <p className="text_gree__o"><span className="">Completed</span></p>,
    Action: <div className="action_tab"><a href="" data-toggle="modal" data-target="#Editdetais"><i class="fas fa-edit"></i></a></div>,
  },
  {
    Name: "",
    Card_SN: "",
    TPS_530T_Passenger_Validator: "",
    TPS_900_320_Driver_s_Device: "",
    Origin: "",
    Destination: "",
    Maximum_Fee: "",
    Ride_Fee: "",
    Travel_Distance_in_KM: "",
    End_Date: "",
    Start_Date: "",
    Status: <p className="text_gree__o"><span className="">Completed</span></p>,
    Action: <div className="action_tab"><a href="" data-toggle="modal" data-target="#Editdetais"><i class="fas fa-edit"></i></a></div>,
  },

];
const columns = [
  {
    name: 'NAME',
    selector: 'riderName',
    sortable: true,

  },
  {
    name: 'CARD SN',
    selector: 'sNo',
    sortable: true,
  },
  {
    name: 'CARD UID',
    selector: 'cardId',
    sortable: true,
  },
  {
    name: 'PASSENGER VALIDATOR',
    selector: 'deviceId',
    sortable: true,
  },
  {
    name: 'DRIVERS MONITOR',
    selector: 'driverDeviceId',
    sortable: true,
  },
  {
    name: 'ORIGIN',
    selector: 'startAddress',
    sortable: true,
  },
  {
    name: 'DESTINATION',
    selector: 'endAddress',
    sortable: true,

  },

  {
    name: 'RIDE FEE',
    selector: 'amout',
    sortable: true,
  },
  {
    name: '20% Discount',
    selector: 'discount',
    sortable: true,
  },
  {
    name: 'TRAVEL DISTANCE(IN KM)',
    selector: 'travelKM',
    sortable: true,
  },

  {
    name: 'END DATE',
    selector: 'endDate',
    sortable: true,
  },
  {
    name: 'START DATE',
    selector: 'startDate',
    sortable: true,
  },
  {
    name: 'STATUS',
    selector: 'status',
    sortable: true,
  }
  // {
  //   name: 'Action',
  //   selector: 'Action',
  //   sortable: true,
  //   cell: record => {
  //     console.log('record', record)

  //     return (
  //       // <Fragment>
  //       <>
  //         <button
  //           data-tip="Edit"
  //           data-toggle="modal"
  //           data-target="#Editdetais"
  //           className="action_tab"

  //           // onClick={() => editRecord(record._id)}
  //           style={{ marginRight: "5px" }}
  //         >
  //           <i className="fa fa-edit"></i>

  //         </button>

  //         <button
  //           data-tip="Edit"
  //           data-toggle="modal"
  //           data-target="#newmsg"
  //           className="action_tab"

  //           // onClick={() => editRecord(record._id)}
  //           style={{ marginRight: "5px" }}
  //         >
  //           <i className="fa fa-comments"></i>

  //         </button>
  //         </>

  //       // </Fragment>
  //     );
  //   }
  // },
];
const options = [
  { value: 'SND', label: 'SND' },
  { value: 'SNR', label: 'SNR' },
  { value: 'Alltransactions', label: 'All transactions' },
];


function Ridehistory() {
  const [startDate, setStartDate] = useState(new Date());
  const [endDate, setEndDate] = useState(new Date());

  const initialFormValue = {
    'name': '',
    'status': 'active',
  }
  const [rideHistory, setrideHistory] = useState();
  const [formValue, setFormValue] = useState(initialFormValue);

  const getriderHistory = async (reqData) => {
    const { status, loading, message, result } = await getRideHistory(reqData);
    console.log('setrideHistory', result)
    if (status == 'success') {
      console.log('status', status)

      setrideHistory(result)
    }
    else {

    }


  }

  const handleSearch = async (e) => {
    console.log('e', e.target.value)
    let reqData = {
      page: 1,
      limit: 10,
      search: e.target.value,
      // from: startDate,
      // to: endDate
    }
    getriderHistory(reqData)
  }
  const handleDownloadCSV = async () => {
    // const { page, limit, search, from, to } = this.state;
    let reqData = {
      records: [],
      search: '',
      options: [],
      page: 1,
      limit: 10,
      count: 0,
      loader: false,
      from: undefined,
      to: undefined,
      selectedOption: null,
      // search,
      // from = 1,
      // to
    }
    getRideHistoryCsv(reqData)
  }
  const handleStartDate = (e) => {
    console.log('e', e)
    setStartDate(e)
  }
  const handleEndDate = async (e) => {
    console.log('startDate', startDate)
    console.log('endDate', endDate)
    setEndDate(e)
    let reqData = {
      page: 1,
      limit: 10,
      search: "",
      from: startDate,
      to: endDate
    }
    getriderHistory(reqData)
    console.log(reqData, 'reqData')
  }


  useEffect(() => {
    getriderHistory();
  }, []
  )
  return (
    <div>
      <div className="row">
        <div className="col-lg-12">
          <h1 className="tiltl_head_tetx">Ride History</h1>
        </div>
        <div className="col-lg-1">

        </div>
        <div className="col-lg-3">
          <div className="d-flex justify-content-center">
            <div className="calaleder_section">
              <label>From</label>
              <div className="datepiker_sec">
                <DatePicker selected={startDate} onChange={(date) => handleStartDate(date)} />
                <i class="far fa-calendar-alt"></i>
              </div>
            </div>
            <div className="calaleder_section">
              <label>To</label>
              <div className="datepiker_sec">
                <DatePicker selected={endDate} onChange={(date) => handleEndDate(date)} />
                <i class="far fa-calendar-alt"></i>
              </div>
            </div>
          </div>


        </div>

        {/* <div className="col-lg-3">
              <div className="d-flex justify-content-center">
              <div className="calaleder_section">
                <label>From</label>
                 <div className="datepiker_sec">
                 <DatePicker selected={startDate} onChange={(date) => setStartDate(date)} />
                 <i class="far fa-calendar-alt"></i>
                   </div> 
              </div>
              <div className="calaleder_section">
                <label>To</label>
                 <div className="datepiker_sec">
                 <DatePicker selected={endDate} onChange={(date) => setEndDate(date)} />
                 <i class="far fa-calendar-alt"></i>
                   </div> 
              </div>


              </div>
              
           
            </div> */}
        <div className="col-lg-3">
          <div className="paddin_sss">
            <label></label>
            <Select
              options={options}
            />
          </div>

        </div>
        <div className="col-lg-5">
          <div className="search_download">
            <div className="input_1">
              <input type="text" onChange={handleSearch} placeholder="Filter in Records..." />
              <i class="fas fa-search"></i>
            </div>
            <div>
              <button
                onClick={() => handleDownloadCSV()}
              >Download <i class="fas fa-download"></i></button>
            </div>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-lg-12">
          <DataTable columns={columns} data={rideHistory} noHeader className="table_new_s"
            pagination={true} paginationPerPage="5"
            paginationRowsPerPageOptions={[5, 10, 15, 20]} 
            noDataComponent="Loading..." 

            />

        </div>
      </div>
      {/* <div className="row">
            <div className="col-lg-12">
              <a href="" className="class_a"> <i class="fas fa-plus"></i> Add</a>
            </div>
          </div> */}
      {/* 
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#newmsg">
  Open modal
</button> */}



      <div class="modal" id="Editdetais">
        <div class="modal-dialog modal-dialog-centered modal-md">
          <div class="modal-content">


            <div class="modal-header">
              <h4 class="modal-title model_heder_colo">Edit Details</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>


            <div class="modal-body edit_section_d_model">

              <div className="row">
                <div className="col-lg-4 grif_place">
                  <div className="label_section">
                    <label>Status</label>
                  </div>

                </div>
                <div className="col-lg-8">
                  <div className="content_clolk">
                    <div className="content_clolk model_section">
                      <div class="form-check form-check-inline d-flex align-items-center">
                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1" />
                        <label class="form-check-label" for="inlineRadio1">Completed </label>
                      </div>
                      <div class="form-check form-check-inline d-flex align-items-center">
                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2" />
                        <label class="form-check-label" for="inlineRadio2">In Progress</label>
                      </div>

                    </div>
                  </div>
                </div>
              </div>



            </div>

            <div class="modal-footer  align-items-end">

              <div className="button_section_model">
                <button type="button" class="btn sendmsg" data-dismiss="modal">Save</button>
              </div>

            </div>

          </div>
        </div>
      </div>
    </div>
  )
}

export default Ridehistory
