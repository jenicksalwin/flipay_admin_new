import React, { useState, useEffect } from "react";
import DataTable from 'react-data-table-component';
import DatePicker from "react-datepicker";
import Select from 'react-select';
import "react-datepicker/dist/react-datepicker.css";
import { CKEditor } from 'ckeditor4-react';
import { getFareIncome, getFareIncomeCsv } from "../actions/rider";

import store from '../store';

// Trade History Table
const data = [
  {
    PassengerNumber: "",
    Date: "",
    CardNumber: "",
    DeviceID: "",
    Origin: "",
    Destination: "",
    DistanceTravelledKM: "",
    TypeofPassenger: "Regular",
    ModeofTransport: "Jeepney",
    PlateNumber: "",
    StartDate: "",
    EndDate: "",
    Fare: "",
    Status: <div className="status_section"><a href="" className="color_completed"> Successful</a></div>,



  },
  {
    PassengerNumber: "",
    Date: "",
    CardNumber: "",
    DeviceID: "",
    Origin: "",
    Destination: "",
    DistanceTravelledKM: "",
    TypeofPassenger: "Regular",
    ModeofTransport: "Jeepney",
    PlateNumber: "",
    StartDate: "",
    EndDate: "",
    Fare: "",
    Status: <div className="status_section"><a href="" className="colot_po"> Unsuccessful</a></div>,



  },
  {
    PassengerNumber: "",
    Date: "",
    CardNumber: "",
    DeviceID: "",
    Origin: "",
    Destination: "",
    DistanceTravelledKM: "",
    TypeofPassenger: "Regular",
    ModeofTransport: "Jeepney",
    PlateNumber: "",
    StartDate: "",
    EndDate: "",
    Fare: "",
    Status: <div className="status_section"><a href="" className="colot_po"> Unsuccessful</a></div>,



  },





  {
    PassengerNumber: "",
    Date: "",
    CardNumber: "",
    DeviceID: "",
    Origin: "",
    Destination: "",
    DistanceTravelledKM: "",
    TypeofPassenger: "Regular",
    ModeofTransport: "Jeepney",
    PlateNumber: "",
    StartDate: "",
    EndDate: "",
    Fare: "",
    Status: <div className="status_section"><a href="" className="colot_po"> Unsuccessful</a></div>,




  },

];
const columns = [
  {
    name: 'PASSENGER NUMBER',
    selector: 'riderName',
    sortable: true,

  },
  {
    name: 'DATE',
    selector: 'Date',
    sortable: true,

  },
  {
    name: 'CARD NUMBER',
    selector: 'cardId',
    sortable: true,
  },
  {
    name: 'DEVICE ID',
    selector: 'deviceId',
    sortable: true,
  },
  {
    name: 'ORIGIN',
    selector: 'startAddress',
    sortable: true,
  },
  {
    name: 'DESTINATION',
    selector: 'endAddress',
    sortable: true,
  },
  {
    name: 'DISTANCE TRAVELLED(KM)',
    selector: 'travelKM',
    sortable: true,

  },
  {
    name: 'TYPE OF PASSENGER',
    selector: 'typeofPassenger',
    sortable: true,

  },
  {
    name: 'MODE OF TRANSPORT',
    selector: 'modeofTransport',
    sortable: true,

  },
  {
    name: 'PLATE NUMBER',
    selector: 'plateNo',
    sortable: true,

  },
  {
    name: 'START DATE',
    selector: 'startDate',
    sortable: true,

  },
  {
    name: 'END DATE',
    selector: 'endDate',
    sortable: true,

  },
  {
    name: 'FARE',
    selector: 'amout',
    sortable: true,

  },
  {
    name: 'STATUS',
    selector: 'status',
    sortable: true,
    cell: record => {
      if (record.status == "completed") {
        var status = 'Successful';

      }
      else {
        var status = 'UnSuccessful';
      }
      return (
        status
      );
    }
  },

];


function FareIncome() {
  const [startDate, setStartDate] = useState(new Date());
  const [endDate, setEndDate] = useState(new Date());
  const [pagination, setPagination] = useState()
  const [page, setPage] = useState(1);
  const countPerPage = 3;
  const initialFormValue = {
    'name': '',
    'status': 'active'
  }

  const [record, setRecord] = useState({
    'data': [],
    'count': 0
  })
  const [filter, setFilter] = useState({

    'search': '',
    'page': 1,
    'limit': 10
  })
  const options = [
    // { value: 'chocolate', label: 'Chocolate' },
    // { value: 'strawberry', label: 'Strawberry' },
    // { value: 'vanilla', label: 'Vanilla' },

    { value: 'SND', label: 'SND' },
    { value: 'SNR', label: 'SNR' },
    { value: '', label: 'All Transactions' },
  ];
  const [fareIncome, setfareIncome] = useState({
    'data': [],
    'count': 0
  });
  const [formValue, setFormValue] = useState(initialFormValue);

  const getriderHistory = async (reqData) => {

    var resultArr = [];
    const { status, loading, message, result, count} = await getFareIncome(reqData);
    console.log('setrideHistory', result)
    if (status == 'success') {
      console.log('status', status)
      setfareIncome(result)

      setfareIncome({

        'data': result,
        count: count
      })
    }
    else {

    }

  }

  const handleSearch = async (e) => {
    console.log('e', e.target.value)
    let reqData = {
      page: 1,
      limit: 10,
      search: e.target.value,
      // from: startDate,
      // to: endDate
    }
    console.log('reqData', reqData)

    getriderHistory(reqData)
  }
  const handleDownloadCSV = async () => {
    // const { page, limit, search, from, to } = this.state;
    let reqData = {
      records: [],
      search: '',
      options: [],
      page: 1,
      limit: 10,
      count: 0,
      loader: false,
      from: undefined,
      to: undefined,
      selectedOption: null,
    }
    getFareIncomeCsv(reqData)
  }


  const handlePageChange = page => {
    console.log('page', page)
    let filterData = { ...filter, ...{ 'page': page } }
    setFilter(filterData)
    getriderHistory(filterData);
  }
  const handlePerRowsChange = async (newPerPage, page) => {
    console.log('filter',...filter)
    console.log('newPerPage',newPerPage)
    console.log('page',page)
    let filterData = { ...filter, ...{ 'page': page, 'limit': newPerPage } }
    setFilter(filterData)
    getriderHistory(filterData);
  };
  const handleStartDate = (e) => {
    console.log('e', e)
    setStartDate(e)
  }
  const handleEndDate = async (e) => {
    console.log('startDate', startDate)
    console.log('endDate', endDate)
    setEndDate(e)
    let reqData = {
      page: 1,
      limit: 10,
      search: "",
      from: startDate,
      to: endDate
    }
    getriderHistory(reqData)
    console.log(reqData, 'reqData')
  }

  const handlePagination = async (index) => {

    console.log('index', index)
    // var id = store.getState().auth.user._id;
    // const { from, to } = this.state;
    // let reqData = {
    //   page: 1,
    //   limit: 10,
    //   // search: index.filter_value,
    //   id: id,
    //   // from,
    //   // to
    // }
    // getriderHistory(reqData)
    // setPagination({ page: index.page_number, limit: index.page_size, search: index.filter_value })
    // this.setState({ page: index.page_number, limit: index.page_size, search: index.filter_value })
  }
  const handleOptions = (e) => {
    console.log('checkValues', e)
    let reqData = {
      search: e.value
    }
    console.log('reqData', reqData)
    getriderHistory(reqData);

  }

  useEffect(() => {

    let reqData = {
      page: 1,
      limit: 10,
      // search: "",
      // from: startDate,
      // to: endDate
    }
    getriderHistory(reqData);
    //   setRecord({
    //     'data': resultArr,
    //     count: result.count
    // })
  }, []
  )
  return (
    <div>
      <div className="row">
        <div className="col-lg-12">
          <h1 className="tiltl_head_tetx">Fare Income</h1>
          {/* <p className="clas_posoa">Description: Lorem ipsum dolor sit amet, consectetur adipiscing<br />
elit. Eget egestas iaculis rhoncus, id et venenatis eu mauris.</p> */}

        </div>
        <div className="col-lg-1">

        </div>

        <div className="col-lg-3">
          <div className="d-flex justify-content-center">
            <div className="calaleder_section">
              <label>From</label>
              <div className="datepiker_sec">
                <DatePicker selected={startDate} onChange={(date) => handleStartDate(date)} />
                <i class="far fa-calendar-alt"></i>
              </div>
            </div>
            <div className="calaleder_section">
              <label>To</label>
              <div className="datepiker_sec">
                <DatePicker selected={endDate} onChange={(date) => handleEndDate(date)} />
                <i class="far fa-calendar-alt"></i>
              </div>
            </div>
          </div>


        </div>
        <div className="col-lg-3">
          <div className="paddin_sss w-100">
            <label></label>
            <Select
              options={options}
              onChange={handleOptions}
            />
          </div>

        </div>
        <div className="col-lg-5">
          <div className="search_download">
            <div className="input_1">
              <input type="text" onChange={handleSearch} placeholder="Filter in Records..." />
              <i class="fas fa-search"></i>
            </div>
            <div>
              <button onClick={() => handleDownloadCSV()}>Download <i class="fas fa-download"></i></button>
            </div>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-lg-12 table_new_one">
          <DataTable columns={columns}
            data={fareIncome.data}
            noHeader 
            className="table_new_s"
            pagination
            noDataComponent="Loading..." 

            // progressPending={loader}
            paginationServer
            //  paginationPerPage="10"
            paginationTotalRows={fareIncome.count}

            // paginationRowsPerPageOptions={[5, 10, 15, 20]}
            onChangeRowsPerPage={handlePerRowsChange}
            onChangePage={handlePageChange}
          />

          <div class="row new_table_total_filed">
            <div class="col">

            </div>
            <div class="col">

            </div>
            <div class="col">

            </div>
            <div class="col">

            </div>
            <div class="col">

            </div>
            <div class="col">
              <h2>Total</h2>
            </div>
            <div class="col row_bg_f">

            </div>
          </div>

        </div>
      </div>

      {/* 
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#newmsg">
  Open modal
</button> */}



      <div class="modal" id="Editdetais">
        <div class="modal-dialog modal-dialog-centered modal-md">
          <div class="modal-content">


            <div class="modal-header">
              <h4 class="modal-title model_heder_colo">Edit Details</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>


            <div class="modal-body edit_section_d_model">

              <div className="row">
                <div className="col-lg-4 grif_place">
                  <div className="label_section">
                    <label>Status</label>
                  </div>

                </div>
                <div className="col-lg-8">
                  <div className="content_clolk">
                    <div className="content_clolk model_section">
                      <div class="form-check form-check-inline d-flex align-items-center">
                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1" />
                        <label class="form-check-label" for="inlineRadio1">Completed </label>
                      </div>
                      <div class="form-check form-check-inline d-flex align-items-center">
                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2" />
                        <label class="form-check-label" for="inlineRadio2">In Progress</label>
                      </div>

                    </div>
                  </div>
                </div>
              </div>



            </div>

            <div class="modal-footer  align-items-end">

              <div className="button_section_model">
                <button type="button" class="btn sendmsg" data-dismiss="modal">Save</button>
              </div>

            </div>

          </div>
        </div>
      </div>
    </div>
  )
}

export default FareIncome
