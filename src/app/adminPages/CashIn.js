import React, { useState, useEffect} from "react";
import DataTable from 'react-data-table-component';
import DatePicker from "react-datepicker";
import Select from 'react-select';
import "react-datepicker/dist/react-datepicker.css";
import { CKEditor } from 'ckeditor4-react';
import { getAllCashUsers, statusChanged, statusRejected  } from '../actions/userCash';
import config from '../actions/config';
import $ from 'jquery';
// Trade History Table
const data = [
  {
    Name:"",
    Bank_Partner:"", 
    Transaction_Number:"",
    Reference_Number:"", 
    Date_of_Transaction:"", 
    Balance:"",
    Status:<p className="text_gree__o"><span className="">Approved</span></p>, 
    Action:<div className="action_tab"><a href=""  data-toggle="modal" data-target="#Editdetais"><i class="fas fa-edit"></i></a></div>,
  },
    {
      Name:"",
      Bank_Partner:"", 
      Transaction_Number:"",
      Reference_Number:"", 
      Date_of_Transaction:"", 
      Balance:"",
      Status:<p className="text_gree__o"><span className="">Approved</span></p>, 
      Action:<div className="action_tab"><a href=""  data-toggle="modal" data-target="#Editdetais"><i class="fas fa-edit"></i></a></div>,
    },

  ];
 
  const options = [
    { value: 'SND', label: 'SND' },
    { value: 'SNR', label: 'SNR' },
    { value: 'Alltransactions', label: 'All Transactions' },
  ];

function Cashin() {

  const columns = [
    {
      name: 'NAME',
      selector: 'acc_holder_name',
      sortable: true,
    
    },
    {
      name: 'BANK NAME',
      selector: 'bank_name',
      sortable: true,
    },
    {
      name: 'TRANSACTION NUMBER',
      selector: 'transaction_number',
      sortable: true,
    },
    {
      name: 'REFERENCE NUMBER',
      selector: 'referenceNumber',
      sortable: true,
    },
    {
      name: 'DATE OF TRANSACTION',
      selector: 'transaction_date',
      sortable: true,
    },
    {
      name: 'BALANCE',
      selector: 'amount',
      sortable: true,
      
    },
    {
      name: 'STATUS',
      selector: 'cashinStatus',
      sortable: true,
    },
  
    {
      name: 'ACTION',
      selector: 'Action',
      sortable: true,
      cell: record => {
        // console.log('record', record)
  
        return (
          // <Fragment>
          <>
            <button
              data-tip="Edit"
              data-toggle="modal"
              data-target="#Editdetais"
              className="action_tab buttn_trans"
  
              onClick={() => editR(record)}
              style={{ marginRight: "5px" }}
            >
              <i className="fas fa-edit"></i>
              
            </button>
            </>
  
          // </Fragment>
        );
      }
    },
  ];
  const [startDate, setStartDate] = useState(new Date());
  const [endDate, setEndDate]  =  useState(new Date());
const [cashinStatus, setCashNo] = useState('')
  const initialFormValue = {
    'name': '',
    'status': 'active'
}
  const [Cashin, setCashin] = useState();
  const [formValue, setFormValue] = useState(initialFormValue);

  const getcashin = async (reqData) => {
    const { status, loading, message, result } = await getAllCashUsers(reqData);
    console.log('results', result)
    if (status == 'success') {
      console.log('status', status)
      setCashin(result)
    }
    else {

    }


  }

  useEffect(() => {
    getcashin();
  },[]
  )
  const handleChange = e => {
    e.preventDefault();
   // const { id, value } = e.target;
   // let formData = { ...formValue, ...{ [id]: value } }
   // console.log('formData',formData)
   // setFormValue(formData)    
}
const handleChanges = (e) => {
  e.preventDefault();
  const { id, value } = e.target;
  let formData = { ...formValue, ...{ [id]: value } }
  console.log('formData', formData)
  setFormValue(formData)
}
  const editR = async (record) => {
  
    // e.preventDefault();
    // const { id, value } = e.target;
    // let formData = { ...formValue, ...{ [id]: value } }
    // setFormValue()    
  console.log('record-----',record)
  setFormValue(record)
  setCashNo(record.cashinStatus)
  }
  // const handleDownloadCSV = async () => {
  //   // const { page, limit, search, from, to } = this.state;
  //   let reqData = {
  //     records: [],
  //     search: '',
  //     options: [],
  //     page: 1,
  //     limit: 10,
  //     count: 0,
  //     loader: false,
  //     from: undefined,
  //     to: undefined,
  //     selectedOption: null,
  //     // search,
  //     // from = 1,
  //     // to
  //   }
  //   getRideHistoryCsv(reqData)
  // }
  // const handleStartDate = (e) => {
  //   console.log('e', e)
  //   setStartDate(e)
  // }
  // const handleEndDate = async (e) => {
  //   console.log('startDate', startDate)
  //   console.log('endDate', endDate)
  //   setEndDate(e)
  //   let reqData = {
  //     page: 1,
  //     limit: 10,
  //     search: "",
  //     from: startDate,
  //     to: endDate
  //   }
  //   getAllCashUsers(reqData)
  //   console.log(reqData, 'reqData')
  // }
  const reasonRejected = async (id) => {
    try {
      let data = {

        "id": id,
        "rejectReason": formValue.reason
      }

      const { status, loading, message, result } = await statusRejected(data);
      console.log('rrr', result)

      if (status == 'success') {
        setFormValue(result)
      }
    }
    catch (err) {

    }
  }

  const changeRejected = async (id) => {

    // alert(rejectReason)

    try {
      //     alert(id);
      // this.setState({'rButton':true})

      $('#rejectReason1').modal('show');
      $('#Editdetais').modal('hide');
      // const { status, loading, message, result } = await statusRejected({id:id});
      // console.log("resultresultresult",result);
      // if (status == 'success') {
      //   setFormValue(result)
      // setRejectModal({rejectReason : true})       

      // }
    } catch (err) { }

  }
  const handleSearch = async (e) => {
    console.log('e', e.target.value)
    let reqData = {
      page: 1,
      limit: 10,
      search: e.target.value,
      // from: startDate,
      // to: endDate
    }
    getcashin(reqData)
  }

  const changeStatus = async (id,amount) => {
    console.log('id',id)
    try {
       
       
        const { status, loading, message, result } = await statusChanged({id:id,amount:amount});
        console.log("resultresultresult",result);
        console.log("message",message);
        if (status == 'success') {
            // toast("Approved Successfully");
     setFormValue(result)
            // this.setState({ 'records': result })
        }
       
    } catch (err) { 
      console.log('err',err)
    }

}


    return (
        <div>
           <div className="row">
             <div className="col-lg-12">
             <h1 className="tiltl_head_tetx">Cash In</h1>
             </div>
            <div className="col-lg-1">

            </div>
            <div className="col-lg-3">
              {/* <div className="d-flex justify-content-center">
              <div className="calaleder_section">
                <label>From</label>
                 <div className="datepiker_sec">
                 <DatePicker selected={startDate} onChange={(date) => handleStartDate(date)} />
                 <i class="far fa-calendar-alt"></i>
                   </div> 
              </div>
              <div className="calaleder_section">
                <label>To</label>
                 <div className="datepiker_sec">
                 <DatePicker selected={endDate} onChange={(date) => handleEndDate(date)} />
                 <i class="far fa-calendar-alt"></i>
                   </div> 
              </div>
              </div> */}
              
           
            </div>
            <div className="col-lg-3">
              {/* <div className="paddin_sss">
                <label></label>
                <Select
                options={options}
              />
              </div> */}
            
            </div>
            <div className="col-lg-5">
              <div className="search_download justify-content-end mb-3">
                <div className="input_1">
                    <input type="text" onChange={handleSearch} placeholder="Filter in Records..." />
                    <i class="fas fa-search"></i>
                </div>
                {/* <div>
                  <button>Download <i class="fas fa-download"></i></button>
                </div> */}
              </div>
            </div>
            </div> 
            <div className="row">
              <div className="col-lg-12">
          <DataTable columns={columns} data={Cashin}            noDataComponent="Loading..." 
 noHeader  className="table_new_s"  pagination={true} paginationPerPage="5" paginationRowsPerPageOptions={[5, 10, 15, 20]} />

              </div>
            </div>
          {/* <div className="row">
            <div className="col-lg-12">
              <a href="" className="class_a"> <i class="fas fa-plus"></i> Add</a>
            </div>
          </div> */}
{/* 
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#newmsg">
  Open modal
</button> */}
      <div class="modal" id="rejectReason1">
        <div class="modal-dialog modal-dialog-centered modal-md">
          <div class="modal-content">

            <div class="modal-header">
              <h4 class="modal-title model_heder_colo">Reject Reason</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body edit_section_d_model">

              <div className="row mb-3">
                <div className="col-lg-4 grif_place">
                  <div className="label_section">
                    <label>Reason</label>
                  </div>

                </div>
                <div className="col-lg-8">
                  <div className="input_box_msg">
                    <input type="text" id="reason" onChange={handleChanges} value={formValue.reason} />

                  </div>
                </div>
              </div>
            </div>

            <div className="button_section_model">
              {/* <button type="button" onClick={() => changeStatus(formValue._id)} class="btn savemsg" data-dismiss="modal" >Accept</button> */}

              <button type="button" onClick={() => reasonRejected(formValue._id)} class="btn sendmsg" data-dismiss="modal">Reject</button>
            </div>

          </div>
        </div>
      </div>


<div class="modal" id="Editdetais">
  <div class="modal-dialog modal-dialog-centered modal-md">
    <div class="modal-content">


      <div class="modal-header">
        <h4 class="modal-title model_heder_colo">Edit Details</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>


      <div class="modal-body edit_section_d_model">
   

              {/* <div className="row mb-3">
                <div className="col-lg-4 grif_place">
                  <div className="label_section">
                    <label>Account Number</label>
                  </div>

                </div>
                <div className="col-lg-8">
                  <div className="input_box_msg">
                    <input type="text" id="accountNo" onChange={handleChange} value={formValue.accountNo} />

                  </div>
                </div>
              </div> */}

              <div className="row mb-3">
                <div className="col-lg-4 grif_place">
                  <div className="label_section">
                    <label>Account Name</label>
                  </div>

                </div>
                <div className="col-lg-8">
                  <div className="input_box_msg">
                    <input type="text" id="accountName" onChange={handleChange} value={formValue.acc_holder_name}/>

                  </div>
                </div>
              </div>
              <div className="row mb-3">
                <div className="col-lg-4 grif_place">
                  <div className="label_section">
                    <label>Bank Name</label>
                  </div>

                </div>
                <div className="col-lg-8">
                  <div className="input_box_msg">
                    <input type="text" id="bankName" onChange={handleChange} value={formValue.bank_name}/>

                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-lg-4 grif_place">
                  <div className="label_section">
                    <label>Transaction Number</label>
                  </div>

                </div>
                <div className="col-lg-8">
                  <div className="input_box_msg">
                    <input type="text" id="referenceNumber" value={formValue.transaction_number} />

                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-lg-4 grif_place">
                  <div className="label_section">
                    <label>Reference Number</label>
                  </div>

                </div>
                <div className="col-lg-8">
                  <div className="input_box_msg">
                    <input type="text" id="referenceNumber" value={formValue.referenceNumber} />

                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-lg-4 grif_place">
                  <div className="label_section">
                    <label>Amount</label>
                  </div>

                </div>
                <div className="col-lg-8">
                  <div className="input_box_msg">
                    <input type="text" id="amount" value={formValue.amount} />

                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-lg-4 grif_place">
                  <div className="label_section">
                    <label>Status</label>
                  </div>

                </div>
                <div className="col-lg-8">
                  <div className="input_box_msg">
                    <input type="text" id="status" value={formValue.cashinStatus} />

                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-lg-4 grif_place">
                  <div className="label_section">
                    <label>Cash in Recepient</label>
                  </div>

                </div>
                <div className="col-lg-8">
                  <div className="content_clolk">
                    <div className="img_section_78">
                    <img src={config.baseUrl+"bankcash/"+formValue.cashprofileImage} alt = "" width="40" 
     height="40"/> <br/> <br/>
                      {/* <i class="far fa-image"></i> */}
                    </div>
                    <div className="image_options">
                    <a href={config.baseUrl+"bankcash/"+formValue.cashprofileImage}  target="_blank">View</a>
                    </div>
                  </div>
                </div>
              </div>
           
      
       {/* <div className="row">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Status</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="content_clolk">
          <div className="content_clolk model_section">
          <div class="form-check form-check-inline d-flex align-items-center">
            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1" />
            <label class="form-check-label" for="inlineRadio1">Completed </label>
          </div>
          <div class="form-check form-check-inline d-flex align-items-center">
            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2" />
            <label class="form-check-label" for="inlineRadio2">In Progress</label>
          </div>
       
          </div>
          </div>
        </div>
       </div> */}

        
       
      </div>
      {cashinStatus == 'submitted'  &&

<div class="modal-footer  align-items-end">

  <div className="button_section_model">
    <button type="button" onClick={() => changeStatus(formValue._id,formValue.amount)} class="btn savemsg" data-dismiss="modal" >Accept</button>

    <button type="button"  data-toggle="modal"
                    data-target="#rejectReason1" onClick={() => changeRejected(formValue._id)} class="btn sendmsg" data-dismiss="modal">Reject</button>
  </div>

</div>
}
      {/* <div class="modal-footer  align-items-end">
     
        <div className="button_section_model">
        <button type="button" class="btn sendmsg" data-dismiss="modal">Save</button>
        </div>
       
      </div> */}

    </div>
  </div>
</div>
        </div>
    )
}

export default Cashin
