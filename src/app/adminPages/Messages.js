import React, { useState } from "react";
import DataTable from 'react-data-table-component';
import DatePicker from "react-datepicker";
import Select from 'react-select';
import "react-datepicker/dist/react-datepicker.css";
import { CKEditor } from 'ckeditor4-react';

// Trade History Table
const data = [
  {
    Name:"",
    Email_Address:"", 
    Concern:"",
    Remarks:"card", 
    Action:<div className="action_tab"><a href="" data-toggle="modal" data-target="#newmsg"><i class="far fa-comments"></i></a><a href="" data-toggle="modal" data-target="#Editdetais"><i class="fas fa-edit"></i></a><a href=""><i class="far fa-trash-alt trash_color_i"></i></a></div>,
  },
    {
      Name:"",
      Email_Address:"", 
      Concern:"",
      Remarks:"card", 
      Action:<div className="action_tab"><a href="" data-toggle="modal" data-target="#newmsg"><i class="far fa-comments"></i></a><a href="" data-toggle="modal" data-target="#Editdetais"><i class="fas fa-edit"></i></a><a href="" ><i class="far fa-trash-alt trash_color_i"></i></a></div>,
    },
    {
      Name:"",
      Email_Address:"", 
      Concern:"",
      Remarks:"card", 
      Action:<div className="action_tab"><a href="" data-toggle="modal" data-target="#newmsg"><i class="far fa-comments"></i></a><a href="" data-toggle="modal" data-target="#Editdetais"><i class="fas fa-edit"></i></a><a href="" ><i class="far fa-trash-alt trash_color_i"></i></a></div>,
    },
    
  ];
  const columns = [
    {
      name: 'NAME',
      selector: 'Name',
      sortable: true,
    
    },
    {
      name: 'EMAIL ADDRESS',
      selector: 'Email_Address',
      sortable: true,
    },
    {
      name: 'CONCERN',
      selector: 'Concern',
      sortable: true,
    },
    {
      name: 'REMARKS',
      selector: 'remarks',
      sortable: true,
    },
    {
      name: 'ACTION',
      selector: 'Action',
      sortable: true,
    },


  ];
  const options = [
    { value: 'SND', label: 'SND' },
    { value: 'SNR', label: 'SNR' },
    { value: 'Alltransactions', label: 'All transactions' },
  ];

function Test() {
  const [startDate, setStartDate] = useState(new Date());

    return (
        <div>
           <div className="row">
             <div className="col-lg-12">
             <h1 className="tiltl_head_tetx">Messages</h1>
             </div>
             <div className="col-lg-1">
              
              
           
              </div>
            <div className="col-lg-3">
              
              
           
            </div>
            <div className="col-lg-3">
              <div className="justify-content-around d-flex">
             
              <div className="paddin_sss w-100">
                <label></label>
                <Select
                options={options}
              />
              </div>
              </div>
          
            
            </div>
            <div className="col-lg-5">
              <div className="search_download ">
                <div className="input_1">
                    <input type="text" placeholder="Filter in Records..." />
                    <i class="fas fa-search"></i>
                </div>
                <div>
                  <button>Download <i class="fas fa-download"></i></button>
                </div>
              </div>
            </div>
            </div> 
            <div className="row">
              <div className="col-lg-12">
          <DataTable columns={columns} data={data} noHeader  className="table_new_s"  pagination={true} paginationPerPage="5" paginationRowsPerPageOptions={[5, 10, 15, 20]} />

              </div>
            </div>
          <div className="row">
            <div className="col-lg-12">
              <a href="" className="class_a" data-toggle="modal" data-target="#Editdetais"> <i class="fas fa-plus"></i> Add</a>
            </div>
          </div>


          <div class="modal" id="newmsg">
  <div class="modal-dialog modal-dialog-centered modal-md modal-dialog_cutom_width">
    <div class="modal-content">


      <div class="modal-header">
        <h4 class="modal-title model_heder_colo">New Message</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>


      <div class="modal-body">
        <div className="row">
          <div className="col-lg-12">
              <div className="send_msg_form">
                <label>Recepient:</label>
                <input type="text" />
              </div>
              <div className="send_msg_form">
                <label>Subject:</label>
                <input type="text" />
              </div>
          </div>
        </div>
        <div className="div_clas_po">
        <label>Text:</label>
       <CKEditor initData={<p>This is an example CKEditor 4 WYSIWYG editor instance.</p>} />
        </div>
       
      </div>

      <div class="modal-footer justify-content-between align-items-end">
        <div className="signature_sectooion">
          <label>signature</label>
          <div className="signature_filed">
            <div className="bottom_btn">
            <a href="">Change</a>
              <a href="">Remove</a>
            </div>
          </div>
        </div>
        <div className="button_section_model">
        <button type="button" class="btn dlelte" data-dismiss="modal">Delete</button>
        <button type="button" class="btn savemsg" >Save to Drafts</button>
        
        <button type="button" class="btn sendmsg" data-dismiss="modal">Send Message</button>
        </div>
       
      </div>

    </div>
  </div>
</div>

<div class="modal" id="Editdetais">
  <div class="modal-dialog modal-dialog-centered modal-md">
    <div class="modal-content">


      <div class="modal-header">
        <h4 class="modal-title model_heder_colo">Edit Details</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>


      <div class="modal-body edit_section_d_model">

       <div className="row">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Status</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="content_clolk">
          <div className="content_clolk model_section">
          <div class="form-check form-check-inline d-flex align-items-center">
            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1" />
            <label class="form-check-label" for="inlineRadio1">Completed </label>
          </div>
          <div class="form-check form-check-inline d-flex align-items-center">
            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2" />
            <label class="form-check-label" for="inlineRadio2">In Progress</label>
          </div>
       
          </div>
          </div>
        </div>
       </div>

        
       
      </div>

      <div class="modal-footer  align-items-end">
     
        <div className="button_section_model">
        <button type="button" class="btn sendmsg" data-dismiss="modal">Save</button>
        </div>
       
      </div>

    </div>
  </div>
</div>
        </div>

        
    )
}

export default Test
