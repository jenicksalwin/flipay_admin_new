import React, { useState, useEffect } from "react";
import DataTable from 'react-data-table-component';
import DatePicker from "react-datepicker";
import Select from 'react-select';
import "react-datepicker/dist/react-datepicker.css";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import { CKEditor } from 'ckeditor4-react';
import { addAdmin, editAdmin, getAllAdminInfo } from '../actions/AdminInfo';
import Toggle from "../adminPages/Toggle"
// Trade History Table
const data = [
  {
    NAME: "",
    EMAIL_ADDRESS: "",
    ROLE: "",
    Action: <div className="action_tab"><a href="" data-toggle="modal" data-target="#Editdetais"><i class="fas fa-edit"></i></a><a href="" data-toggle="modal" data-target="#delete"><i class="far fa-trash-alt"></i></a></div>,
  },
  {
    NAME: "",
    EMAIL_ADDRESS: "",
    ROLE: "",
    Action: <div className="action_tab"><a href="" data-toggle="modal" data-target="#Editdetais"><i class="fas fa-edit"></i></a><a href="" data-toggle="modal" data-target="#delete"><i class="far fa-trash-alt"></i></a></div>,
  },

];

function AdminInfo() {
  let toasterOption = { 
    position: "top-right",  
    autoClose: 6000,  
    hideProgressBar: false, 
    closeOnClick: true, 
    pauseOnHover: true, 
    draggable: true,  
    progress: undefined,  
  } 

  const initialFormValue = {
    '_id': '',
    'role': '',
    'companyName': '',
    'name': '',
    'email': '',
    'password': '',
    'transport': false,
    'vehicleService': false,
    'vehicle': false,
    'distributor': false,
    'retailer': false,
    'driverList': false,
    'timeTracker': false,
    'sales': false,
    'driverTrxhistory': false,
    'messageDriver': false,
    'riderList': false,
    'wallet': false,
    'rideHistory': false,
    'riderMessages': false,
    'cashIn': false,
    'transactionHistory': false,
    'messageDistributor': false,
    'fareIncome': false,
    'loadSales': false,
    'cardSales': false,
    'emailTemplate': false,
    'privacyPolicy': false,
    'SubadminMenu': false,
    'adminActivity': false,
    'support': false,
    'accountingSystem': false
  }
  const columns = [
    {
      name: 'NAME',
      selector: 'name',
      sortable: true,
    },
    {
      name: 'EMAIL ADDRESS',
      selector: 'email',
      sortable: true,

    },
    // {
    //   name: 'Password',
    //   selector: 'password',
    //   sortable: true,

    // },

    {
      name: 'ROLE',
      selector: 'role',
      sortable: true,
    },
    {
      name: 'ACTION',
      selector: 'Action',
      sortable: true,
      cell: record => {
        // console.log('record', record)

        return (
          // <Fragment>
          <>
            <button
              data-tip="Edit"
              data-toggle="modal"
              data-target="#Editdetais"
              className="action_tab buttn_trans"

              onClick={() => editR(record)}
              style={{ marginRight: "5px" }}
            >
              <i className="fas fa-edit"></i>

            </button>
          </>

          // </Fragment>
        );
      }
    },

  ];
  const options = [
    { value: 'Admin', label: 'Admin', id: 'Admin' },
    { value: 'subadmin', label: 'Sub Admin', id: 'subadmin' },
  ];
  const [startDate, setStartDate] = useState(new Date());

  const [adminInfo, setAdminInfo] = useState('')
  const [formValue, setFormValue] = useState(initialFormValue)
  const [optionValue, setOptionvalue] = useState()
  const [toggleNav, setToggleNav] = useState()
  const getAdminInfo = async () => {
    const { result, status, loading, error } = await getAllAdminInfo();
    if (status == 'success') {
      setAdminInfo(result)
    }
    else {
      console.log('err,e', error)
    }
  }

  const handleChange = (e) => {
    // e.preventDefault();
    console.log('1e', e)

    const { id, value } = e.target;
    // const { name} = e.value
    let formData = { ...formValue, ...{ [id]: value } }
    console.log('formData', formData)
    setFormValue(formData)
  }

  const { email, password, name, companyName, role, transport, vehicleService, vehicle, distributor,
    retailer,
    driverList,
    timeTracker,
    sales,
    driverTrxhistory,
    messageDriver,
    riderList,
    wallet,
    rideHistory,
    riderMessages,
    cashIn,
    transactionHistory,
    messageDistributor,
    fareIncome,
    loadSales,
    cardSales,
    emailTemplate,
    privacyPolicy,
    SubadminMenu,
    adminActivity,
    support,
    accountingSystem } = formValue

  const handleOptionvalues = (e) => {
    console.log('eeeeeeeeeeeee', e.value)
    // const { id, value } = e.value;
    var id = "role"
    // console.log('vvvvvvvvvvvv', value)
    // const { role } = 'role';
    let formData = { ...formValue, ...{ [id]: e.value } }
    console.log('formData1', formData)
    setFormValue(formData)
    // setOptionvalue(e.value)
  }
  useEffect(() => {
    getAdminInfo()
  }, [])

  const handleSubmit = async (e) => {

    // e.preventDefault();


    let reqData = {
      '_id': formValue._id,
      'role': formValue.role,
      'name': formValue.name,
      'email': formValue.email,
      'companyName': formValue.companyName,
      'transport': formValue.transport,
      'vehicleService': formValue.vehicleService,
      'vehicle': formValue.vehicle,
      'distributor': formValue.distributor,
      'retailer': formValue.retailer,
      'driverList': formValue.driverList,
      'timeTracker': formValue.timeTracker,
      'sales': formValue.sales,
      'driverTrxhistory': formValue.driverTrxhistory,
      'messageDriver': formValue.messageDriver,
      'riderList': formValue.riderList,
      'wallet': formValue.wallet,
      'rideHistory': formValue.rideHistory,
      'riderMessages': formValue.riderMessages,
      'cashIn': formValue.cashIn,
      'transactionHistory': formValue.transactionHistory,
      'messageDistributor': formValue.messageDistributor,
      'fareIncome': formValue.fareIncome,
      'loadSales': formValue.loadSales,
      'cardSales': formValue.cardSales,
      'emailTemplate': formValue.emailTemplate,
      'privacyPolicy': formValue.privacyPolicy,
      'SubadminMenu': formValue.SubadminMenu,
      'adminActivity': formValue.adminActivity,
      'support': formValue.support,
      'accountingSystem': formValue.accountingSystem

    }
    const { result, error, status, message } = await editAdmin(reqData)
    if (status == "success") {
      getAdminInfo()

      // toast('Admin information Updated Successfully');
      toast(message, {
        position: toast.POSITION.TOP_CENTER,
      });
      
      console.log('reqData123', reqData)
      // alert(message)
    }
    else {
      console.log('err', error)
    }
  }


  const handleSubmitAdd = async () => {

    let reqData = {

      name,
      role,
      email,
      companyName,
      password,
      transport,
      vehicleService,
      vehicle,
      distributor,
      retailer,
      driverList,
      timeTracker,
      sales,
      driverTrxhistory,
      messageDriver,
      riderList,
      wallet,
      rideHistory,
      riderMessages,
      cashIn,
      transactionHistory,
      messageDistributor,
      fareIncome,
      loadSales,
      cardSales,
      emailTemplate,
      privacyPolicy,
      SubadminMenu,
      adminActivity,
      support,
      accountingSystem

      // 'rideHistory': toggleNav.rideHistory,
      // 'transport': toggleNav.transport,
      // 'vehicle' : toggleNav.vehicle
      // cashin
      // '_id': formValue._id,
      // 'role': formValue.role,
      // 'name': formValue.name,
      // 'email': formValue.email,
    }
    const { result, error, status, message } = await addAdmin(reqData)
    if (status == "success") {

      window.location.reload();
      toast(message, {
        position: toast.POSITION.TOP_CENTER,
      });
      // getAdminInfo()
      // alert(message)

      console.log('reqData', reqData)
      // alert(message)
    }
    else {
      console.log('err', error)
    }
  }
  // }
  const editR = async (record) => {

    // e.preventDefault();
    // const { id, value } = e.target;
    // let formData = { ...formValue, ...{ [id]: value } }
    // setFormValue()    
    console.log('record-----', record)
    setFormValue(record)
    // setCashNo(record.cashinStatus)
  }

  // const handleToggle1 = (checked, navbar) => {

  //   console.log('Navbar', navbar)
  //   console.log('checked123', checked)
  //   console.log('frst', navbar.checked)

  //   setToggleNav(checked)
  //   // this.setState({ checked });
  // }
  const handleToggle = (checked, navbar) => {

    console.log('Navbar', navbar)
    console.log('checked123', checked)
    // const { name } = navbar;
    console.log('formValue', formValue)

    let formData = { ...formValue, [navbar]: checked }
    // let formData = { ...formValue,  ...{[navbar]: checked  } }

    console.log('formData', formData)
    setFormValue(formData)

  }
  return (
    <div>
      <div className="row">
        <div className="col-lg-12">
          <h1 className="tiltl_head_tetx">Admin Controller</h1>
        </div>
        <div className="col-lg-1">

        </div>
        <div className="col-lg-3">


        </div>
        <div className="col-lg-3">


        </div>
        <div className="col-lg-5 mb-3">
          <div className="search_download">
            <div className="input_1">
              <input type="text" placeholder="Filter in Records..." />
              <i class="fas fa-search"></i>
            </div>
            <div>
              <button>Download <i class="fas fa-download"></i></button>
            </div>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-lg-12">
          <DataTable columns={columns} data={adminInfo} noHeader className="table_new_s" pagination={true} paginationPerPage="5" paginationRowsPerPageOptions={[5, 10, 15, 20]} />

        </div>
      </div>
      <div className="row">
        <div className="col-lg-12">
          <a href="" className="class_a" data-toggle="modal" data-target="#adddetais"> <i class="fas fa-plus"></i> Add</a>
        </div>
        {/* <div className="col-lg-12">
          <a href="" className="class_a" data-toggle="modal" data-target="#edit_new"> <i class="fas fa-plus"></i> Add</a>
        </div> */}
      </div>
      <div class="modal" id="Editdetais">
        <div class="modal-dialog modal-dialog-centered modal-md">
          <div class="modal-content">


            <div class="modal-header">
              <h4 class="modal-title model_heder_colo">Edit</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body edit_section_d_model">

            <div className="row mb-3">


<div className="col-lg-4 grif_place">
  <div className="label_section">
    <label>Admin Type</label>
  </div>

</div>
<div className="col-lg-8">
  <div className="input_box_msg">
    <Select
      id='role'
      name='role'
      onChange={handleOptionvalues}
      value={options.value}
      options={options}

    />

  </div>
</div>
</div>
              <div className="row mb-3">
                <div className="col-lg-4 grif_place">
                  <div className="label_section">
                    <label>Name</label>
                  </div>

                </div>
                <div className="col-lg-8">
                  <div className="input_box_msg">
                    <input type="text" id="name" onChange={handleChange} value={formValue.name} />

                  </div>
                </div>
              </div>
              <div className="row mb-3">
                <div className="col-lg-4 grif_place">
                  <div className="label_section">
                    <label>Company Name</label>
                  </div>

                </div>
                <div className="col-lg-8">
                  <div className="input_box_msg">
                    <input type="text" id="companyName" onChange={handleChange} value={formValue.companyName} />

                  </div>
                </div>
              </div>
              <div className="row mb-3">
                <div className="col-lg-4 grif_place">
                  <div className="label_section">
                    <label>Email Address</label>
                  </div>

                </div>
                <div className="col-lg-8">
                  <div className="input_box_msg">
                    <input type="text" id="email" onChange={handleChange} value={formValue.email} />

                  </div>
                </div>
              </div>
     
        
              <div className="row mb-3">
                <div className="col-lg-6 flex-wrap">
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Transport Cooperatives</label>
                    <Toggle
                      handleToggle={handleToggle} name="transport" checked={formValue.transport} />
                  </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Vehicle Service</label>
                    <Toggle
                      handleToggle={handleToggle} name="vehicleService" checked={formValue.vehicleService} />
                  </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Vehicle</label>
                    <Toggle
                      handleToggle={handleToggle} name="vehicle" checked={formValue.vehicle} />
                  </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Distributor</label>
                    <Toggle
                      handleToggle={handleToggle} name="distributor" checked={formValue.distributor} />                </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Retailer</label>
                    <Toggle
                      handleToggle={handleToggle} name="retailer" checked={formValue.retailer} />                  </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Drivers List</label>
                    <Toggle
                      handleToggle={handleToggle} name="driverList" checked={formValue.driverList} />                   </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Time Tracker</label>
                    <Toggle
                      handleToggle={handleToggle} name="timeTracker" checked={formValue.timeTracker} />                   </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Sales</label>
                    <Toggle
                      handleToggle={handleToggle} name="sales" checked={formValue.sales} />                   </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Transaction History Driver</label>
                    <Toggle
                      handleToggle={handleToggle} name="driverTrxhistory" checked={formValue.driverTrxhistory} />                  </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Messages Driver</label>
                    <Toggle
                      handleToggle={handleToggle} name="messageDriver" checked={formValue.messageDriver} />                  </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Riders List</label>
                    <Toggle
                      handleToggle={handleToggle} name="riderList" id="riderList" checked={formValue.riderList} />                  </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Wallet</label>
                    <Toggle
                      handleToggle={handleToggle} name="wallet" checked={formValue.wallet} />                   </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Ride History</label>
                    <Toggle
                      handleToggle={handleToggle} checked={formValue.rideHistory} name="rideHistory" />
                  </div>

                </div>
                <div className="col-lg-6">
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Messages Rider</label>
                    <Toggle
                      handleToggle={handleToggle} name="riderMessages" checked={formValue.riderMessages} />                  </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Cash In</label>
                    <Toggle
                      handleToggle={handleToggle} name="cashIn"  checked={formValue.cashIn}/>                  </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Transaction History</label>
                    <Toggle
                      handleToggle={handleToggle} name="transactionHistory" checked={formValue.transactionHistory} />                  </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Messages Distributor</label>
                    <Toggle
                      handleToggle={handleToggle} name="messageDistributor" checked={formValue.messageDistributor} />                  </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>AccountingSystem</label>
                    <Toggle
                      handleToggle={handleToggle} name="accountingSystem" checked={formValue.accountingSystem}/>                  </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Fare Income</label>
                    <Toggle
                      handleToggle={handleToggle} name="fareIncome" checked={formValue.fareIncome}/>                  </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Load Sales</label>
                    <Toggle
                      handleToggle={handleToggle} name="loadSales" checked={formValue.loadSales} />                  </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Card Sales</label>
                    <Toggle
                      handleToggle={handleToggle} name="cardSales" checked={formValue.cardSales}/>                  </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Email Template</label>
                    <Toggle
                      handleToggle={handleToggle} name="emailTemplate" checked={formValue.emailTemplate}/>                  </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Privacy Policy</label>
                    <Toggle
                      handleToggle={handleToggle} name="privacyPolicy" checked={formValue.privacyPolicy} />                  </div>

                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Admin/Sub-Admin Controller</label>
                    <Toggle
                      handleToggle={handleToggle} name="SubadminMenu" checked={formValue.SubadminMenu}/>                  </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Admin Activity</label>
                    <Toggle
                      handleToggle={handleToggle} name="adminActivity" checked={formValue.adminActivity}/>                  </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Support</label>
                    <Toggle
                      handleToggle={handleToggle} name="support" checked={formValue.support}/>                  </div>
                </div>

              </div>
            </div>

            <div class="modal-footer justify-content-between  align-items-end">
              {/* <div className="button_section_model">
      
      <button type="button" class="btn savemsg" data-dismiss="modal" >Add New Account <i class="fas fa-plus"></i></button>
      
    
      </div> */}
              <div className="button_section_model">


                <button type="button" onClick={handleSubmit} class="btn sendmsg" data-dismiss="modal">Save</button>
              </div>

            </div>

          </div>
        </div>
      </div>
      <div class="modal" id="adddetais">
        <div class="modal-dialog modal-dialog-centered modal-md">
          <div class="modal-content">


            <div class="modal-header">
              <h4 class="modal-title model_heder_colo">Create Account</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>


            <div class="modal-body edit_section_d_model">

            <div className="row mb-3">
                <div className="col-lg-4 grif_place">
                  <div className="label_section">
                    <label>Admin Type</label>
                  </div>

                </div>
                <div className="col-lg-8">
                  <div className="input_box_msg">
                    <Select
                      id='role'
                      name='role'
                      onChange={handleOptionvalues}
                      value={options.value}
                      options={options}
                    />


                  </div>
                </div>
                </div>
              <div className="row mb-3">
                <div className="col-lg-4 grif_place">
                  <div className="label_section">
                    <label>Name</label>
                  </div>

                </div>
                <div className="col-lg-8">
                  <div className="input_box_msg">
                    <input type="text" id="name" onChange={handleChange} value={name} />

                  </div>
                </div>
              </div>
              <div className="row mb-3">
                <div className="col-lg-4 grif_place">
                  <div className="label_section">
                    <label>Company Name</label>
                  </div>

                </div>
                <div className="col-lg-8">
                  <div className="input_box_msg">
                    <input type="text" id="companyName" onChange={handleChange} value={formValue.companyName} />

                  </div>
                </div>
              </div>
              <div className="row mb-3">
                <div className="col-lg-4 grif_place">
                  <div className="label_section">
                    <label>Email Address</label>
                  </div>

                </div>
                <div className="col-lg-8">
                  <div className="input_box_msg">
                    <input type="text" id="email" onChange={handleChange} value={email} />

                  </div>
                </div>

              </div>
              <div className="row mb-3">

                <div className="col-lg-4 grif_place">
                  <div className="label_section">
                    <label>Password</label>
                  </div>
                </div>
                <div className="col-lg-8">
                  <div className="input_box_msg">
                    <input type="text" id="password" onChange={handleChange} value={password} />

                  </div>
                </div>
                </div>



         
                <div className="row mb-3">
                  <div className="col-lg-6 flex-wrap">
                    <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                      <label>Transport Cooperatives</label>
                      <Toggle
                        handleToggle={handleToggle} name="transport" checked={formValue.transport} />
                    </div>
                    <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                      <label>Vehicle Service</label>
                      <Toggle
                        handleToggle={handleToggle} name="vehicleService" checked={formValue.vehicleService} />
                    </div>
                    <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                      <label>Vehicle</label>
                      <Toggle
                        handleToggle={handleToggle} name="vehicle" checked={formValue.vehicle} />
                    </div>
                    <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Distributor</label>
                    <Toggle
                      handleToggle={handleToggle} name="distributor" checked={formValue.distributor} />                </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Retailer</label>
                    <Toggle
                      handleToggle={handleToggle} name="retailer" checked={formValue.retailer} />                  </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Drivers List</label>
                    <Toggle
                      handleToggle={handleToggle} name="driverList" checked={formValue.driverList} />                   </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Time Tracker</label>
                    <Toggle
                      handleToggle={handleToggle} name="timeTracker" checked={formValue.timeTracker} />                   </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Sales</label>
                    <Toggle
                      handleToggle={handleToggle} name="sales" checked={formValue.sales} />                   </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Transaction History Driver</label>
                    <Toggle
                      handleToggle={handleToggle} name="driverTrxhistory" checked={formValue.driverTrxhistory} />                  </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Messages Driver</label>
                    <Toggle
                      handleToggle={handleToggle} name="messageDriver" checked={formValue.messageDriver} />                  </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Riders List</label>
                    <Toggle
                      handleToggle={handleToggle} name="riderList" id="riderList" checked={formValue.riderList} />                  </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Wallet</label>
                    <Toggle
                      handleToggle={handleToggle} name="wallet" checked={formValue.wallet} />                   </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Ride History</label>
                    <Toggle
                      handleToggle={handleToggle} checked={formValue.rideHistory} name="rideHistory" />
                  </div>

                </div>
                <div className="col-lg-6">
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Messages Rider</label>
                    <Toggle
                      handleToggle={handleToggle} name="riderMessages" checked={formValue.riderMessages} />                  </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Cash In</label>
                    <Toggle
                      handleToggle={handleToggle} name="cashIn"  checked={formValue.cashIn}/>                  </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Transaction History</label>
                    <Toggle
                      handleToggle={handleToggle} name="transactionHistory" checked={formValue.transactionHistory} />                  </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Messages Distributor</label>
                    <Toggle
                      handleToggle={handleToggle} name="messageDistributor" checked={formValue.messageDistributor} />                  </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>AccountingSystem</label>
                    <Toggle
                      handleToggle={handleToggle} name="accountingSystem" checked={formValue.accountingSystem}/>                  </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Fare Income</label>
                    <Toggle
                      handleToggle={handleToggle} name="fareIncome" checked={formValue.fareIncome}/>                  </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Load Sales</label>
                    <Toggle
                      handleToggle={handleToggle} name="loadSales" checked={formValue.loadSales} />                  </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Card Sales</label>
                    <Toggle
                      handleToggle={handleToggle} name="cardSales" checked={formValue.cardSales}/>                  </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Email Template</label>
                    <Toggle
                      handleToggle={handleToggle} name="emailTemplate" checked={formValue.emailTemplate}/>                  </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Privacy Policy</label>
                    <Toggle
                      handleToggle={handleToggle} name="privacyPolicy" checked={formValue.privacyPolicy} />                  </div>

                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Admin/Sub-Admin Controller</label>
                    <Toggle
                      handleToggle={handleToggle} name="SubadminMenu" checked={formValue.SubadminMenu}/>                  </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Admin Activity</label>
                    <Toggle
                      handleToggle={handleToggle} name="adminActivity" checked={formValue.adminActivity}/>                  </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Support</label>
                    <Toggle
                      handleToggle={handleToggle} name="support" checked={formValue.support}/>                  </div>
                </div>

                </div>

            </div>

            <div class="modal-footer justify-content-between  align-items-end">
              <div className="button_section_model">

                <button type="button" class="btn savemsg" data-dismiss="modal" >Add More <i class="fas fa-plus"></i></button>


              </div>
              <div className="button_section_model">


                <button type="button" onClick={handleSubmitAdd} class="btn sendmsg" data-dismiss="modal">Save</button>
              </div>

            </div>

          </div>
        </div>
      </div>




      <div class="modal admin_input" id="edit_new">
        <div class="modal-dialog modal-dialog-centered modal-md">
          <div class="modal-content">


            <div class="modal-header">
              <h4 class="modal-title model_heder_colo">Edit</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>


            <div class="modal-body edit_section_d_model">


              <div className="row mb-3">
                <div className="col-lg-4 grif_place">
                  <div className="label_section">
                    <label>Admin Type</label>
                  </div>

                </div>
                <div className="col-lg-8">
                  <div className="input_box_msg">
                    <Select
                      id='role'
                      name='role'
                      onChange={handleOptionvalues}
                      value={options.value}
                      options={options}
                    />


                  </div>
                </div>


              </div>

              <div className="row mb-3">
                <div className="col-lg-4 grif_place">
                  <div className="label_section">
                    <label>Name</label>
                  </div>

                </div>
                <div className="col-lg-8">
                  <div className="input_box_msg">
                    <input type="text" id="name" />

                  </div>
                </div>
              </div>
              <div className="row mb-3">

                <div className="col-lg-4 grif_place">
                  <div className="label_section">
                    <label>Company Name</label>
                  </div>
                </div>
                <div className="col-lg-8">
                  <div className="input_box_msg">
                    <input type="text" id="password" />

                  </div>
                </div>
              </div>
              <div className="row mb-3">
                <div className="col-lg-4 grif_place">
                  <div className="label_section">
                    <label>Email Address</label>
                  </div>

                </div>
                <div className="col-lg-8">
                  <div className="input_box_msg">
                    <input type="text" id="email" />

                  </div>
                </div>

              </div>
              <div className="row mb-3">
                <div className="col-lg-6 flex-wrap">
                  {/* <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Transport Cooperatives</label>
                    <Toggle
                      handleToggle={handleToggle} />
                  </div> */}
                  {/* <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Vehicle Service</label>
                    <Toggle />
                  </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Vehicle</label>
                    <Toggle />
                  </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Distributor</label>
                    <Toggle />
                  </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Retailer</label>
                    <Toggle />
                  </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Drivers List</label>
                    <Toggle />
                  </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Time Tracker</label>
                    <Toggle />
                  </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Sales</label>
                    <Toggle />
                  </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Transaction History Driver</label>
                    <Toggle />
                  </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Messages Driver</label>
                    <Toggle />
                  </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Riders List</label>
                    <Toggle />
                  </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Wallet</label>
                    <Toggle />
                  </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Ride History</label>
                    <Toggle />
                  </div>

                </div>
                <div className="col-lg-6">
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Messages Rider</label>
                    <Toggle />
                  </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Cash In</label>
                    <Toggle />
                  </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Transaction History</label>
                    <Toggle />
                  </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Messages Distributor</label>
                    <Toggle />
                  </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Fare Income</label>
                    <Toggle />
                  </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Load Sales</label>
                    <Toggle />
                  </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Card Sales</label>
                    <Toggle />
                  </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Email Template</label>
                    <Toggle />
                  </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Privacy Policy</label>
                    <Toggle />
                  </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Admin/Sub-Admin Controller</label>
                    <Toggle />
                  </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Admin Activity</label>
                    <Toggle />
                  </div>
                  <div className="flex_conteni_w d-flex justify-content-between align-items-center">
                    <label>Support</label>
                    <Toggle />
                  </div> */}
                </div>

              </div>




            </div>

            <div class="modal-footer justify-content-between  align-items-end">
              <div className="button_section_model">

                <button type="button" class="btn savemsg" data-dismiss="modal" >Add New Account<i class="fas fa-plus"></i></button>


              </div>
              <div className="button_section_model">

                <button type="button" onClick={handleSubmitAdd} class="btn" data-dismiss="modal">Clear</button>
                <button type="button" onClick={handleSubmitAdd} class="btn sendmsg" data-dismiss="modal">Save</button>
              </div>

            </div>

          </div>
        </div>

      </div>
      <ToastContainer/>

    </div>

  )
}

export default AdminInfo
