import React, { useState } from "react";
import DataTable from 'react-data-table-component';
import DatePicker from "react-datepicker";
import Select from 'react-select';
import "react-datepicker/dist/react-datepicker.css";
import { CKEditor } from 'ckeditor4-react';

// Trade History Table
const data = [
  {
    Name:"", 
    Accountnumber:"",
    CardsSN :"", 
    Type:"",
    Numberofcardssold:"", 
    Amount:"", 
  
  },
  {
    Name:"", 
    Accountnumber:"",
    CardsSN :"", 
    Type:"",
    Numberofcardssold:"", 
    Amount:"", 
  
  },
  {
    Name:"", 
    Accountnumber:"",
    CardsSN :"", 
    Type:"",
    Numberofcardssold:"", 
    Amount:"", 
  
  },
  {
    Name:"", 
    Accountnumber:"",
    CardsSN :"", 
    Type:"",
    Numberofcardssold:"", 
    Amount:"", 
  },

  ];
  const columns = [
    {
      name: 'NAME',
      selector: 'Name',
      sortable: true,
    
    },
    {
      name: 'ACCOUNT NUMBER',
      selector: 'Accountnumber',
      sortable: true,
    
    },
    {
      name: 'CARDS SN',
      selector: 'CardsSN',
      sortable: true,
    },
    {
      name: 'TYPE',
      selector: 'Type ',
      sortable: true,
    },
    {
      name: 'NUMBER OF CARDS SOLD',
      selector: 'Numberofcardssold',
      sortable: true,
    },
    {
      name: 'AMOUNT',
      selector: 'Amount',
      sortable: true,
    },
   
    
  ];
  const options = [
    { value: 'SND', label: 'SND' },
    { value: 'SNR', label: 'SNR' },
    { value: 'Alltransactions', label: 'All transactions' },
  ];

function Test() {
  const [startDate, setStartDate] = useState(new Date());

    return (
        <div>
           <div className="row">
             <div className="col-lg-12">
             <h1 className="tiltl_head_tetx">CardSales</h1>
             <p className="clas_posoa">Description: Lorem ipsum dolor sit amet, consectetur adipiscing<br />
elit. Eget egestas iaculis rhoncus, id et venenatis eu mauris.</p>
             </div>
            <div className="col-lg-1">

            </div>
          
            <div className="col-lg-3">
              <div className="d-flex justify-content-center">
              <div className="calaleder_section">
                <label>From</label>
                 <div className="datepiker_sec">
                 <DatePicker selected={startDate} onChange={(date) => setStartDate(date)} />
                 <i class="far fa-calendar-alt"></i>
                   </div> 
              </div>
              <div className="calaleder_section">
                <label>To</label>
                 <div className="datepiker_sec">
                 <DatePicker selected={startDate} onChange={(date) => setStartDate(date)} />
                 <i class="far fa-calendar-alt"></i>
                   </div> 
              </div>
              </div>
              
           
            </div>
            <div className="col-lg-3">
            <div className="paddin_sss w-100">
                <label></label>
                <Select
                options={options}
              />
              </div>
            
            </div>
            
            <div className="col-lg-5">
              <div className="search_download">
                <div className="input_1">
                    <input type="text" placeholder="Filter in Records..." />
                    <i class="fas fa-search"></i>
                </div>
                <div>
                  <button>Download <i class="fas fa-download"></i></button>
                </div>
              </div>
            </div>
            </div> 
            <div className="row">
              <div className="col-lg-12 table_new_one">
              <DataTable columns={columns} data={data} noHeader  className="table_new_s"  pagination={true} paginationPerPage="5" paginationRowsPerPageOptions={[5, 10, 15, 20]} />
               
              <div class="row new_table_total_filed">
          
              <div class="col">
       
              </div>
              <div class="col">
       
              </div>
              <div class="col">
              <h2>Total</h2>
              </div>
              <div class="col row_bg_f">
               
              </div>
              <div class="col row_bg_f">
               
              </div>
              <div class="col row_bg_f">
              
              </div>
            </div>
                
              </div>
            </div>
          

{/* <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add_detais">
  Open modal
</button> */}



<div class="modal" id="add_detais">
  <div class="modal-dialog modal-dialog-centered modal-md">
    <div class="modal-content">


      <div class="modal-header">
        <h4 class="modal-title model_heder_colo">Add Card Sales</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>


    
      <div class="modal-body edit_section_d_model">
       
      <div className="row mb-3">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Date</label>
          </div>
            
        </div>
        <div className="col-lg-8">
        <div className="input_box_msg panndin_back">
          <DatePicker selected={startDate} onChange={(date) => setStartDate(date)} />

          </div>
        </div>
       </div>
       <div className="row mb-3">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Name</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="input_box_msg">
          <input type="text" />
         
          </div>
        </div>
       </div>
       <div className="row mb-3">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Account Number</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="input_box_msg">
          <input type="text" />
         
          </div>
        </div>
       </div>
       <div className="row mb-3">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Serial Number</label>
          </div>
            
        </div>
        <div className="col-lg-8">
         <div className="row">
          <div className="col-lg-6">
          <div className="input_box_msg">
          <label>From</label>
          <Select
                options={options}
              />
          </div>
          </div>
          <div className="col-lg-6">
          <div className="input_box_msg">
            <label>To</label>
          <Select
                options={options}
              />
          </div>
          </div>
         </div>
        </div>
       </div>
    

     
       <div className="row mb-3">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Distributor/ Transport Cooperative</label>
          </div>
            
        </div>
        <div className="col-lg-8">
        <div className="input_box_msg">
         
          <Select
                options={options}
              />
          </div>
        </div>
       </div>
     



       <div className="row mb-3">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Number of Cards Sold</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="input_box_msg">
          <input type="text" />
         
          </div>
        </div>
       </div>
       <div className="row mb-3">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Amount</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="input_box_msg">
          <input type="text" />
         
          </div>
        </div>
       </div>
    

      </div>

      <div class="modal-footer  align-items-between">
      <button type="button" class="btn sendmsg" data-dismiss="modal">Add New Account</button>

        <div className="button_section_model">
      

        
        <button type="button" class="btn sendmsg" data-dismiss="modal">Save</button>
        </div>
       
      </div>

    </div>
  </div>
</div>
        </div>
    )
}

export default Test
