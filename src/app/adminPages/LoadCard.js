import React, { useState, useEffect} from "react";
import DataTable from 'react-data-table-component';
import DatePicker from "react-datepicker";
import Select from 'react-select';
import "react-datepicker/dist/react-datepicker.css";
import { CKEditor } from 'ckeditor4-react';
import { getLoadHistory, getAdminLoadHistoryCsv } from '../actions/rider';

// Trade History Table
const data = [
  {
    Name:"",
    Bank_Partner:"", 
    Transaction_Number:"",
    Reference_Number:"", 
    Date_of_Transaction:"", 
    Balance:"",
    Status:<p className="text_gree__o"><span className="">Approved</span></p>, 
    Action:<div className="action_tab"><a href=""  data-toggle="modal" data-target="#Editdetais"><i class="fas fa-edit"></i></a></div>,
  },
    {
      Name:"",
      Bank_Partner:"", 
      Transaction_Number:"",
      Reference_Number:"", 
      Date_of_Transaction:"", 
      Balance:"",
      Status:<p className="text_gree__o"><span className="">Approved</span></p>, 
      Action:<div className="action_tab"><a href=""  data-toggle="modal" data-target="#Editdetais"><i class="fas fa-edit"></i></a></div>,
    },

  ];
  const columns = [
    {
      name: 'NAME',
      selector: 'riderName',
      sortable: true,
    
    },
    {
      name: 'TYPE INFO',
      selector: 'typeInfo',
      sortable: true,
    },
    {
      name: 'AMOUNT',
      selector: 'amount',
      sortable: true,
    },
    {
      name: 'CASHIN STATUS',
      selector: 'cashinStatus',
      sortable: true,
    },
    {
      name: 'DATE OF TRANSACTION',
      selector: 'transaction_date',
      sortable: true,
    },
    // {
    //   name: 'Balance',
    //   selector: 'amount',
    //   sortable: true,
      
    // },
    // {
    //   name: 'Status',
    //   selector: 'cashinStatus',
    //   sortable: true,
    // },
    // {
    //   name: 'Action',
    //   selector: 'Action',
    //   sortable: true,
    // },
  ];
  const options = [
    { value: 'SND', label: 'SND' },
    { value: 'SNR', label: 'SNR' },
    { value: 'Alltransactions', label: 'All Transactions' },
  ];

function Loadcard() {
  const [startDate, setStartDate] = useState(new Date());
  const initialFormValue = {
    'name': '',
    'status': 'active',
    'count':''
}

// const formData { count} = 
  const [LoadcardHistory, setLoadcardHistory] = useState();
  const [formValue, setFormValue] = useState(initialFormValue);
const [count, setCount] = useState()
  const getLoadcard = async (reqData) => {
    const { status, loading, message, result, count } = await getLoadHistory(reqData);
    console.log('results', count)
    if (status == 'success') {
      console.log('status', status)
      setLoadcardHistory(result)
      setCount(count)
    }
    else {

    }


  }
  const handleSearch = async (e) => {
    console.log('e', e.target.value)
    let reqData = {
      page: 1,
      limit: 10,
      search: e.target.value,
      count:count
      // from: startDate,
      // to: endDate
    }
    getLoadcard(reqData)
  }
  const handleDownloadCSV = async () => {
    // const { page, limit, search, from, to } = this.state;
    let reqData = {
      records: [],
      search: '',
      options: [],
      page: 1,
      limit: 10,
      count: count,
      loader: false,
      from: undefined,
      to: undefined,
      selectedOption: null,
      // search,
      // from = 1,
      // to
    }
    getAdminLoadHistoryCsv(reqData)
  }

  useEffect(() => {
    getLoadcard();
  },[]
  )
    return (
        <div>
           <div className="row  mb-3">
             <div className="col-lg-12">
             <h1 className="tiltl_head_tetx">Load Card</h1>
             </div>
            <div className="col-lg-1">

            </div>
            <div className="col-lg-3">
              {/* <div className="d-flex justify-content-center">
              <div className="calaleder_section">
                <label>From</label>
                 <div className="datepiker_sec">
                 <DatePicker selected={startDate} onChange={(date) => setStartDate(date)} />
                 <i class="far fa-calendar-alt"></i>
                   </div> 
              </div>
              <div className="calaleder_section">
                <label>To</label>
                 <div className="datepiker_sec">
                 <DatePicker selected={startDate} onChange={(date) => setStartDate(date)} />
                 <i class="far fa-calendar-alt"></i>
                   </div> 
              </div>
              </div> */}
              
           
            </div>
            <div className="col-lg-3">
              {/* <div className="paddin_sss">
                <label></label>
                <Select
                options={options}
              />
              </div> */}
            
            </div>
            <div className="col-lg-5">
              <div className="search_download">
                <div className="input_1">
                    <input type="text" onChange={handleSearch} placeholder="Filter in Records..." />
                    <i class="fas fa-search"></i>
                </div>
                <div>
                  <button onClick={handleDownloadCSV}>Download <i class="fas fa-download"></i></button>
                </div>
              </div>
            </div>
            </div> 
            <div className="row">
              <div className="col-lg-12">
          <DataTable columns={columns}
           data={LoadcardHistory} noHeader 
           noDataComponent="Loading..." 

            className="table_new_s"  pagination={true}
             paginationPerPage="5"
              paginationRowsPerPageOptions={[5, 10, 15, 20]}
              totalRows={count}/>

              </div>
            </div>
          {/* <div className="row">
            <div className="col-lg-12">
              <a href="" className="class_a"> <i class="fas fa-plus"></i> Add</a>
            </div>
          </div> */}
{/* 
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#newmsg">
  Open modal
</button> */}



<div class="modal" id="Editdetais">
  <div class="modal-dialog modal-dialog-centered modal-md">
    <div class="modal-content">


      <div class="modal-header">
        <h4 class="modal-title model_heder_colo">Edit Details</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>


      <div class="modal-body edit_section_d_model">

       <div className="row">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Status</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="content_clolk">
          <div className="content_clolk model_section">
          <div class="form-check form-check-inline d-flex align-items-center">
            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1" />
            <label class="form-check-label" for="inlineRadio1">Completed </label>
          </div>
          <div class="form-check form-check-inline d-flex align-items-center">
            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2" />
            <label class="form-check-label" for="inlineRadio2">In Progress</label>
          </div>
       
          </div>
          </div>
        </div>
       </div>

        
       
      </div>

      <div class="modal-footer  align-items-end">
     
        <div className="button_section_model">
        <button type="button" class="btn sendmsg" data-dismiss="modal">Save</button>
        </div>
       
      </div>

    </div>
  </div>
</div>
        </div>
    )
}

export default Loadcard
