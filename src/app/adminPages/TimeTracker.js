import React, { useState } from "react";
import DataTable from 'react-data-table-component';
import DatePicker from "react-datepicker";
import Select from 'react-select';
import "react-datepicker/dist/react-datepicker.css";
import { CKEditor } from 'ckeditor4-react';

// Trade History Table
const data = [
    {
      Name:"",
      Transport_Cooperative_Corporation:"", 
      PLATE_number:"",
      Route:"", 
      Time_In:"", 
      Time_Out:"",
    
    },
    {
      Name:"",
      Transport_Cooperative_Corporation:"", 
      PLATE_number:"",
      Route:"", 
      Time_In:"", 
      Time_Out:"",
    
    },
    {
      Name:"",
      Transport_Cooperative_Corporation:"", 
      PLATE_number:"",
      Route:"", 
      Time_In:"", 
      Time_Out:"",
    
    },
    {
      Name:"",
      Transport_Cooperative_Corporation:"", 
      PLATE_number:"",
      Route:"", 
      Time_In:"", 
      Time_Out:"",
    
    },
  // { slno: "1", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "2", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "3", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "4", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "5", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "6", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "7", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "8", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "9", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "10", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  ];
  const columns = [
    {
      name: 'NAME',
      selector: 'Name',
      sortable: true,
    
    },
    {
      name: 'TRANSPORT COOPERATIVE CORPORATION',
      selector: 'Transport_Cooperative_Corporation',
      sortable: true,
    },
    {
      name: 'PLATE NUMBER',
      selector: 'Plate Number ',
      sortable: true,
    },
    {
      name: 'ROUTE',
      selector: 'Route',
      sortable: true,
    },
    {
      name: 'TIME IN',
      selector: 'Time_In',
      sortable: true,
    },
    {
      name: 'TIME OUT',
      selector: 'Time_Out',
      sortable: true,
      
    },

  ];
  const options = [
    { value: 'SND', label: 'SND' },
    { value: 'SNR', label: 'SNR' },
    { value: 'Alltransactions', label: 'All Transactions' },
  ];

function Test() {
  const [startDate, setStartDate] = useState(new Date());

    return (
        <div>
           <div className="row">
             <div className="col-lg-12">
             <h1 className="tiltl_head_tetx">Time Tracker</h1>
             </div>
           
            <div className="col-lg-3">
              <div className="d-flex justify-content-center">
              <div className="calaleder_section">
                <label>From</label>
                 <div className="datepiker_sec">
                 <DatePicker selected={startDate} onChange={(date) => setStartDate(date)} />
                 <i class="far fa-calendar-alt"></i>
                   </div> 
              </div>
              <div className="calaleder_section">
                <label>To</label>
                 <div className="datepiker_sec">
                 <DatePicker selected={startDate} onChange={(date) => setStartDate(date)} />
                 <i class="far fa-calendar-alt"></i>
                   </div> 
              </div>
              </div>
              
           
            </div>
            <div className="col-lg-4">
              <div className="justify-content-around d-flex">
              <div className="paddin_sss w-100">
                <label></label>
                <Select
                options={options}
              />
              </div>
              
              <div className="paddin_sss w-100">
                <label></label>
                <Select
                options={options}
              />
              </div>
              </div>
          
            
            </div>
            <div className="col-lg-5">
              <div className="search_download">
                <div className="input_1">
                    <input type="text" placeholder="Filter in Records..." />
                    <i class="fas fa-search"></i>
                </div>
                <div>
                  <button>Download <i class="fas fa-download"></i></button>
                </div>
              </div>
            </div>
            </div> 
            <div className="row">
              <div className="col-lg-12">
          <DataTable columns={columns} data={data} noHeader  className="table_new_s"  pagination={true} paginationPerPage="5" paginationRowsPerPageOptions={[5, 10, 15, 20]} />

              </div>
            </div>
          <div className="row">
            <div className="col-lg-12">
              <a href="" className="class_a" data-toggle="modal" data-target="#Editdetais"> <i class="fas fa-plus"></i> Add</a>
            </div>
          </div>
{/* 
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#newmsg">
  Open modal
</button> */}


<div class="modal" id="newmsg">
  <div class="modal-dialog modal-dialog-centered modal-md modal-dialog_cutom_width">
    <div class="modal-content">


      <div class="modal-header">
        <h4 class="modal-title model_heder_colo">New Message</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>


      <div class="modal-body">
        <div className="row">
          <div className="col-lg-12">
              <div className="send_msg_form">
                <label>Recepient:</label>
                <input type="text" />
              </div>
              <div className="send_msg_form">
                <label>Subject:</label>
                <input type="text" />
              </div>
          </div>
        </div>
        <div className="div_clas_po">
        <label>Text:</label>
       <CKEditor initData={<p>This is an example CKEditor 4 WYSIWYG editor instance.</p>} />
        </div>
       
      </div>

      <div class="modal-footer justify-content-between align-items-end">
        <div className="signature_sectooion">
          <label>signature</label>
          <div className="signature_filed">
            <div className="bottom_btn">
            <a href="">Change</a>
              <a href="">Remove</a>
            </div>
          </div>
        </div>
        <div className="button_section_model">
        <button type="button" class="btn dlelte" data-dismiss="modal">Delete</button>
        <button type="button" class="btn savemsg" >Save to Drafts</button>
        
        <button type="button" class="btn sendmsg" data-dismiss="modal">Send Message</button>
        </div>
       
      </div>

    </div>
  </div>
</div>
<div class="modal" id="Editdetais">
  <div class="modal-dialog modal-dialog-centered modal-md">
    <div class="modal-content">


      <div class="modal-header">
        <h4 class="modal-title model_heder_colo">Edit Details</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>


      <div class="modal-body edit_section_d_model">
      <div className="row mb-3">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Name</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="input_box_msg">
          <input type="text" />
         
          </div>
        </div>
       </div>
       <div className="row mb-3">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Transport Cooperative /Corporation</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="input_box_msg">
          <Select
                options={options}
              />
         
          </div>
        </div>
       </div>
       <div className="row mb-3">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Plate Number</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="input_box_msg">
          <input type="text" />
         
          </div>
        </div>
       </div>
       <div className="row mb-3">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Route</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="input_box_msg">
          <Select
                options={options}
              />
         
          </div>
        </div>
       </div>
       <div className="row">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Time In</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="input_box_msg d-flex justify-content-end">
          <DatePicker 
           selected={startDate}
           onChange={(date) => setStartDate(date)}
           showTimeSelect
           showTimeSelectOnly
           timeIntervals={15}
           timeCaption="Time"
           dateFormat="h:mm aa"
            />
         
          </div>
        </div>
       </div>
       <div className="row">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Time Out</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="input_box_msg d-flex justify-content-end">
          <DatePicker 
          selected={startDate}
          onChange={(date) => setStartDate(date)}
          showTimeSelect
          showTimeSelectOnly
          timeIntervals={15}
          timeCaption="Time"
          dateFormat="h:mm aa"
           />
         
          </div>
        </div>
       </div>
       <div className="row">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Status</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="content_clolk">
          
          <div className="flexs">
          <div class="custom-control custom-radio">
            <input type="radio" class="custom-control-input" id="customRadio" name="" value="" />
            <label class="custom-control-label" for="customRadio">Approved </label>
          </div>
          <div class="custom-control custom-radio">
            <input type="radio" class="custom-control-input" id="customRadio_1" name="" value="" />
            <label class="custom-control-label" for="customRadio_1">Disapproved </label>
          </div>
          </div>
          </div>
        </div>
       </div>

        
       
      </div>

      <div class="modal-footer  align-items-end">
     
        <div className="button_section_model">
      
        <button type="button" class="btn savemsg" data-dismiss="modal" >Delete</button>
        
        <button type="button" class="btn sendmsg" data-dismiss="modal">Save</button>
        </div>
       
      </div>

    </div>
  </div>
</div>
        </div>
    )
}

export default Test
