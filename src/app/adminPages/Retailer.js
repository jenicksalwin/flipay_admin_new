import React, { useState } from "react";
import DataTable from 'react-data-table-component';
import DatePicker from "react-datepicker";
import Select from 'react-select';
import "react-datepicker/dist/react-datepicker.css";
import { CKEditor } from 'ckeditor4-react';

// Trade History Table
const data = [
    {
      Name:"",
      Distributor:"",
      Account_Number:"",
      Email_Address:"", 
      Mobile_Number:"",
      address:"",
      Photo_ID:<div className="tik_flex_img"><img src={require("../../assets/images/tick_img.png")} alt="logo" className="img-fluid inner_img_e" /></div>,
      Selfie_Verification:<div className="tik_flex_img"><img src={require("../../assets/images/tick_img.png")} alt="logo" className="img-fluid inner_img_e" /></div>, 
      Business_Permit:<div className="tik_flex_img"><img src={require("../../assets/images/tick_img.png")} alt="logo" className="img-fluid inner_img_e" /></div>,
     Action:<div className="action_tab"><a href="" data-toggle="modal" data-target="#newmsg"><i class="far fa-comments"></i></a><a href=""  data-toggle="modal" data-target="#Editdetais"><i class="fas fa-edit"></i></a></div>,
    },
    {
      Name:"",
      Distributor:"",
      Account_Number:"",
      Email_Address:"", 
      Mobile_Number:"",
      address:"",
      Photo_ID:<div className="tik_flex_img"><img src={require("../../assets/images/tick_img.png")} alt="logo" className="img-fluid inner_img_e" /></div>,
      Selfie_Verification:<div className="tik_flex_img"><img src={require("../../assets/images/tick_img.png")} alt="logo" className="img-fluid inner_img_e" /></div>, 
      Business_Permit:<div className="tik_flex_img"><img src={require("../../assets/images/tick_img.png")} alt="logo" className="img-fluid inner_img_e" /></div>,
     Action:<div className="action_tab"><a href="" data-toggle="modal" data-target="#newmsg"><i class="far fa-comments"></i></a><a href=""  data-toggle="modal" data-target="#Editdetais"><i class="fas fa-edit"></i></a></div>,
    },
  // { slno: "1", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "2", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "3", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "4", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "5", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "6", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "7", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "8", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "9", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "10", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  ];
  const columns = [
    {
      name: 'NAME',
      selector: 'Name',
      sortable: true,
    },
    {
      name: 'DISTRIBUTOR',
      selector: 'Distributor',
      sortable: true,
    
    },
    {
      name: 'ACCOUNT NUMBER',
      selector: 'Account_Number',
      sortable: true,
    
    },
    {
      name: 'EMAIL ADDRESS',
      selector: 'Email_Address',
      sortable: true,
    },
    {
      name: 'MOBILE NUMBER',
      selector: 'Mobile_Number ',
      sortable: true,
    },
    {
      name: 'ADDRESS',
      selector: 'address',
      sortable: true,
    },
    {
      name: 'PHOTO ID',
      selector: 'Photo_ID',
      sortable: true,
    },
    {
      name: 'SELFIE VERIFICATION',
      selector: 'Selfie_Verification',
      sortable: true,
      
    },
    {
      name: 'BUSINESS PERMIT',
      selector: 'Business_Permit',
      sortable: true,
    },
    
    {
      name: 'ACTION',
      selector: 'Action',
      sortable: true,
    },
  ];
  const options = [
    { value: 'SND', label: 'SND' },
    { value: 'SNR', label: 'SNR' },
    { value: 'Alltransactions', label: 'All transactions' },
  ];

function Test() {
  const [startDate, setStartDate] = useState(new Date());

    return (
        <div>
           <div className="row mb-3">
             <div className="col-lg-12">
             <h1 className="tiltl_head_tetx">Retailer</h1>
             </div>
            <div className="col-lg-1">

            </div>
            <div className="col-lg-3">
            
              
           
            </div>
            <div className="col-lg-3">
             
            
            </div>
            <div className="col-lg-5">
              <div className="search_download">
                <div className="input_1">
                    <input type="text" placeholder="Filter in Records..." />
                    <i class="fas fa-search"></i>
                </div>
                <div>
                  <button>Download <i class="fas fa-download"></i></button>
                </div>
              </div>
            </div>
            </div> 
            <div className="row">
              <div className="col-lg-12">
          <DataTable columns={columns} data={data} noHeader  className="table_new_s"  pagination={true} paginationPerPage="5" paginationRowsPerPageOptions={[5, 10, 15, 20]} />

              </div>
            </div>
          <div className="row">
            <div className="col-lg-12">
              <a href="" className="class_a" data-toggle="modal" data-target="#add_detais"> <i class="fas fa-plus"></i> Add</a>
            </div>
          </div>
{/* 
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#newmsg">
  Open modal
</button> */}


<div class="modal" id="newmsg">
  <div class="modal-dialog modal-dialog-centered modal-md modal-dialog_cutom_width">
    <div class="modal-content">


      <div class="modal-header">
        <h4 class="modal-title model_heder_colo">New Message</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>


      <div class="modal-body">
        <div className="row">
          <div className="col-lg-12">
              <div className="send_msg_form">
                <label>Recepient:</label>
                <input type="text" />
              </div>
              <div className="send_msg_form">
                <label>Subject:</label>
                <input type="text" />
              </div>
          </div>
        </div>
        <div className="div_clas_po">
        <label>Text:</label>
       <CKEditor initData={<p>This is an example CKEditor 4 WYSIWYG editor instance.</p>} />
        </div>
       
      </div>

      <div class="modal-footer justify-content-between align-items-end">
        <div className="signature_sectooion">
          <label>signature</label>
          <div className="signature_filed">
            <div className="bottom_btn">
            <a href="">Change</a>
              <a href="">Remove</a>
            </div>
          </div>
        </div>
        <div className="button_section_model">
        <button type="button" class="btn dlelte" data-dismiss="modal">Delete</button>
        <button type="button" class="btn savemsg" >Save to Drafts</button>
        
        <button type="button" class="btn sendmsg" data-dismiss="modal">Send Message</button>
        </div>
       
      </div>

    </div>
  </div>
</div>
<div class="modal" id="Editdetais">
  <div class="modal-dialog modal-dialog-centered modal-md">
    <div class="modal-content">


      <div class="modal-header">
        <h4 class="modal-title model_heder_colo">Edit Details</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>


      <div class="modal-body edit_section_d_model">
       
      <div className="row mb-3">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Name</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="input_box_msg">
          <input type="text" />
         
          </div>
        </div>
       </div>
       <div className="row mb-3">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Distributor</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="input_box_msg">
          <Select
                options={options}
              />
          </div>
        </div>
       </div>
    

       <div className="row mb-3">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Account Number</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="input_box_msg">
          <input type="text" />
         
          </div>
        </div>
       </div>
       <div className="row mb-3">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Email Address</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="input_box_msg">
          <input type="text" />
         
          </div>
        </div>
       </div>
       <div className="row mb-3">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Mobile Number</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="input_box_msg">
          <input type="text" />
         
          </div>
        </div>
       </div>
    
        


       <div className="row">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Photo ID</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="content_clolk">
          <div className="img_section_78">
          <i class="far fa-image"></i>
          </div>
          <div className="image_options">
            <a href=""> View</a>
            <a href=""> Upload</a>

            <a href="" className="remover"> Remove</a>

          </div>
          </div>
        </div>
       </div>
       <div className="row">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Selfie Verification</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="content_clolk">
          <div className="img_section_78">
          <i class="fas fa-file-pdf"></i>
          </div>
          <div className="image_options">
            <a href=""> View</a>
            <a href=""> Upload</a>

            <a href="" className="remover"> Remove</a>

          </div>
          </div>
        </div>
       </div>
       <div className="row">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Business Permit</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="content_clolk">
          <div className="img_section_78">
          <i class="fas fa-file-pdf"></i>
          </div>
          <div className="image_options">
            <a href=""> View</a>
            <a href=""> Upload</a>

            <a href="" className="remover"> Remove</a>

          </div>
          </div>
        </div>
       </div>
      
      
      

        
       
      </div>

      <div class="modal-footer  align-items-end">
     
        <div className="button_section_model">
      
        <button type="button" class="btn savemsg" data-dismiss="modal" >Delete</button>
        
        <button type="button" class="btn sendmsg" data-dismiss="modal">Save</button>
        </div>
       
      </div>

    </div>
  </div>
</div>
<div class="modal" id="add_detais">
  <div class="modal-dialog modal-dialog-centered modal-md">
    <div class="modal-content">


      <div class="modal-header">
        <h4 class="modal-title model_heder_colo">Add Distributor</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>


    
      <div class="modal-body edit_section_d_model">
       
      <div className="row mb-3">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Name</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="input_box_msg">
          <input type="text" />
         
          </div>
        </div>
       </div>
       <div className="row mb-3">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Distributor</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="input_box_msg">
          <Select
                options={options}
              />
          </div>
        </div>
       </div>
    

       <div className="row mb-3">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Account Number</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="input_box_msg">
          <input type="text" />
         
          </div>
        </div>
       </div>
       <div className="row mb-3">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Email Address</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="input_box_msg">
          <input type="text" />
         
          </div>
        </div>
       </div>
       <div className="row mb-3">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Mobile Number</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="input_box_msg">
          <input type="text" />
         
          </div>
        </div>
       </div>
    
        


       <div className="row">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Photo ID</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="content_clolk">
          <div className="img_section_78">
          <i class="far fa-image"></i>
          </div>
          <div className="image_options">
            <a href=""> View</a>
            <a href=""> Upload</a>

            <a href="" className="remover"> Remove</a>

          </div>
          </div>
        </div>
       </div>
       <div className="row">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Selfie Verification</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="content_clolk">
          <div className="img_section_78">
          <i class="fas fa-file-pdf"></i>
          </div>
          <div className="image_options">
            <a href=""> View</a>
            <a href=""> Upload</a>

            <a href="" className="remover"> Remove</a>

          </div>
          </div>
        </div>
       </div>
       <div className="row">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Business Permit</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="content_clolk">
          <div className="img_section_78">
          <i class="fas fa-file-pdf"></i>
          </div>
          <div className="image_options">
            <a href=""> View</a>
            <a href=""> Upload</a>

            <a href="" className="remover"> Remove</a>

          </div>
          </div>
        </div>
       </div>
      
      
      

        
       
      </div>

      <div class="modal-footer  align-items-end">
     
        <div className="button_section_model">
      
        <button type="button" class="btn savemsg" data-dismiss="modal" >Delete</button>
        
        <button type="button" class="btn sendmsg" data-dismiss="modal">Add</button>
        </div>
       
      </div>

    </div>
  </div>
</div>
        </div>
    )
}

export default Test
