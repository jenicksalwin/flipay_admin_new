import React, { useState } from "react";
import DataTable from 'react-data-table-component';
import DatePicker from "react-datepicker";
import Select from 'react-select';
import "react-datepicker/dist/react-datepicker.css";
import { CKEditor } from 'ckeditor4-react';

// Trade History Table
const data = [
    {
      Name:"",
      Transport_Cooperative_Corporation:"", 
      Device:"",
      Type_of_Transaction:"card", 
      Transaction_Number:"", 
      Date_of_Transaction:"",
      Amount:"",
      Balance:"", 
      Status:<div className="status_section"><a href="" className="color_completed"> Completed</a></div>,
      Action:<div className="tik_flex_img"><a href="" data-toggle="modal" data-target="#edit_section"><i class="far fa-edit"></i></a></div>, 

    
    },
    {
      Name:"",
      Transport_Cooperative_Corporation:"", 
      Device:"",
      Type_of_Transaction:"card", 
      Transaction_Number:"", 
      Date_of_Transaction:"",
      Amount:"",
      Balance:"", 
      Status:<div className="status_section"><a href="" className="color_error"> Error</a></div>,
      Action:<div className="tik_flex_img"><a href="" data-toggle="modal" data-target="#edit_section"><i class="far fa-edit"></i></a></div>, 

    
    },
    {
      Name:"",
      Transport_Cooperative_Corporation:"", 
      Device:"",
      Type_of_Transaction:"card", 
      Transaction_Number:"", 
      Date_of_Transaction:"",
      Amount:"",
      Balance:"", 
      Status:<div className="status_section"><a href="" className="color_In_Progress">In Progress</a></div>,
      Action:<div className="tik_flex_img"><a href="" data-toggle="modal" data-target="#edit_section"><i class="far fa-edit"></i></a></div>, 

    
    },
    
  ];
  const columns = [
    {
      name: 'NAME',
      selector: 'Name',
      sortable: true,
    
    },
    {
      name: 'TRANSPORT COOPERATIVE CORPORATION',
      selector: 'Transport_Cooperative_Corporation',
      sortable: true,
    },
    {
      name: 'DEVICE',
      selector: 'Device',
      sortable: true,
    },
    {
      name: 'TYPE OF TRANSACTION',
      selector: 'Type_of_Transaction',
      sortable: true,
    },
    {
      name: 'TRANSACTION NUMBER',
      selector: 'Transaction_Number',
      sortable: true,
    },
    {
      name: 'AMOUNT',
      selector: 'Amount',
      sortable: true,
      
    },
    {
      name: 'BALANCE',
      selector: 'Balance',
      sortable: true,
      
    },
    {
      name: 'STATUS',
      selector: 'Status',
      sortable: true,
      
    },
    {
      name: 'ACTION',
      selector: 'Action',
      sortable: true,
      
    },


  ];
  const options = [
    { value: 'SND', label: 'SND' },
    { value: 'SNR', label: 'SNR' },
    { value: 'Alltransactions', label: 'All Transactions' },
  ];

function Test() {
  const [startDate, setStartDate] = useState(new Date());

    return (
        <div>
           <div className="row">
             <div className="col-lg-12">
             <h1 className="tiltl_head_tetx">Sales</h1>
             </div>
           
            <div className="col-lg-3">
              <div className="d-flex justify-content-center">
              <div className="calaleder_section">
                <label>From</label>
                 <div className="datepiker_sec">
                 <DatePicker selected={startDate} onChange={(date) => setStartDate(date)} />
                 <i class="far fa-calendar-alt"></i>
                   </div> 
              </div>
              <div className="calaleder_section">
                <label>To</label>
                 <div className="datepiker_sec">
                 <DatePicker selected={startDate} onChange={(date) => setStartDate(date)} />
                 <i class="far fa-calendar-alt"></i>
                   </div> 
              </div>
              </div>
              
           
            </div>
            <div className="col-lg-5">
              <div className="justify-content-around d-flex">
              <div className="paddin_sss w-100">
                <label></label>
                <Select
                options={options}
              />
              </div>
              <div className="paddin_sss w-100">
                <label></label>
                <Select
                options={options}
              />
              </div>
              
              <div className="paddin_sss w-100">
                <label></label>
                <Select
                options={options}
              />
              </div>
              </div>
          
            
            </div>
            <div className="col-lg-4">
              <div className="search_download search_download__new">
                <div className="input_1">
                    <input type="text" placeholder="Filter in Records..." />
                    <i class="fas fa-search"></i>
                </div>
                <div>
                  <button>Download <i class="fas fa-download"></i></button>
                </div>
              </div>
            </div>
            </div> 
            <div className="row">
              <div className="col-lg-12">
          <DataTable columns={columns} data={data} noHeader  className="table_new_s"  pagination={true} paginationPerPage="5" paginationRowsPerPageOptions={[5, 10, 15, 20]} />

              </div>
            </div>
          <div className="row">
            <div className="col-lg-12">
              <a href="" className="class_a" data-toggle="modal" data-target="#Editdetais"> <i class="fas fa-plus"></i> Add</a>
            </div>
          </div>
{/* 
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#newmsg">
  Open modal
</button> */}


<div class="modal" id="Editdetais">
  <div class="modal-dialog modal-dialog-centered modal-md">
    <div class="modal-content">


      <div class="modal-header">
        <h4 class="modal-title model_heder_colo">Edit Details</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>


      <div class="modal-body edit_section_d_model">

       <div className="row">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Status</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="content_clolk">
          <div className="content_clolk model_section">
          <div class="form-check form-check-inline d-flex align-items-center">
            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1" />
            <label class="form-check-label" for="inlineRadio1">Completed </label>
          </div>
          <div class="form-check form-check-inline d-flex align-items-center">
            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2" />
            <label class="form-check-label" for="inlineRadio2">In Progress</label>
          </div>
          <div class="form-check form-check-inline d-flex align-items-center">
            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2" />
            <label class="form-check-label" for="inlineRadio2">Error</label>
          </div>
          </div>
          </div>
        </div>
       </div>

        
       
      </div>

      <div class="modal-footer  align-items-end">
     
        <div className="button_section_model">
        <button type="button" class="btn sendmsg" data-dismiss="modal">Save</button>
        </div>
       
      </div>

    </div>
  </div>
</div>
        </div>
    )
}

export default Test
