import React, { useState,useEffect } from "react";
import DataTable from 'react-data-table-component';
import DatePicker from "react-datepicker";
import Select from 'react-select';
import "react-datepicker/dist/react-datepicker.css";
import { CKEditor } from 'ckeditor4-react';
import { getAllRider } from '../actions/rider';

// Trade History Table
const data = [
  {
    RiderName:"",
    TPS530: "",
    // First_Name:"", 
    // Middle_Name:"",
    Contact_Number:"", 
    Date_of_Birth:"",
    Email_Address:"", 
    Address:"", 
    Classification:<p className="m-0">Regular</p>,
    Card_UID:"", 
    Card_SN:"", 
    Verification:"Daily", 
    Status:<div className="status_section"><a href="" className="color_active"> Active</a></div>,
   Action:<div className="action_tab"><a href="" data-toggle="modal" data-target="#newmsg"><i class="far fa-comments"></i></a><a href=""  data-toggle="modal" data-target="#Editdetais"><i class="fas fa-edit"></i></a></div>,
  },
    {
      Last_Name:"",
      First_Name:"", 
      Middle_Name:"",
      Contact_Number:"", 
      Date_of_Birth:"",
      Email_Address:"", 
      Address:"", 
      Classification:<p className="m-0">Regular</p>,
      Card_UID:"", 
      Card_SN:"", 
      Verification:"Daily", 
      Status:<div className="status_section"><a href="" className="color_active"> Active</a></div>,
     Action:<div className="action_tab"><a href="" data-toggle="modal" data-target="#newmsg"><i class="far fa-comments"></i></a><a href=""  data-toggle="modal" data-target="#Editdetais"><i class="fas fa-edit"></i></a></div>,
    },
    {
      Last_Name:"",
      First_Name:"", 
      Middle_Name:"",
      Contact_Number:"", 
      Date_of_Birth:"",
      Email_Address:"", 
      Address:"", 
      Classification:<p className="m-0">Regular</p>,
      Card_UID:"", 
      Card_SN:"", 
      Verification:"Daily", 
      Status:<div className="status_section"><a href="" className="color_active"> Active</a></div>,
     Action:<div className="action_tab"><a href="" data-toggle="modal" data-target="#newmsg"><i class="far fa-comments"></i></a><a href=""  data-toggle="modal" data-target="#Editdetais"><i class="fas fa-edit"></i></a></div>,
    },
    {
      Last_Name:"",
      First_Name:"", 
      Middle_Name:"",
      Contact_Number:"", 
      Date_of_Birth:"",
      Email_Address:"", 
      Address:"", 
      Classification:<p className="m-0">Regular</p>,
      Card_UID:"", 
      Card_SN:"", 
      Verification:"Daily", 
      Status:<div className="status_section"><a href="" className="color_active"> Active</a></div>,
      Action:<div className="action_tab"><a href="" data-toggle="modal" data-target="#newmsg"><i class="far fa-comments"></i></a><a href=""  data-toggle="modal" data-target="#Editdetais"><i class="fas fa-edit"></i></a></div>,
    },
  // { slno: "1", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "2", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "3", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "4", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "5", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "6", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "7", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "8", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "9", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  // { slno: "10", datetime: "Dec 29, 2020 / 05:13AM", type: "Rajeshbtc", purid: '257896', amt: "8.00",},
  ];
  const columns = [
    {
      name: 'RiderName',
      selector: 'name',
      sortable: true,
    
    },
    {
      name: 'TPS 530',
      selector: 'First_Name',
      sortable: true,
    },
    {
      name: 'TPS 320',
      selector: 'Middle_Name ',
      sortable: true,
    },
    {
      name: 'Start Address',
      selector: 'Contact_Number',
      sortable: true,
    },
    {
      name: 'End Address',
      selector: 'Date_of_Birth ',
      sortable: true,
      
    },
    {
      name: 'Maximum Fee',
      selector: 'email',
      sortable: true,
    },
   
    {
      name: 'Address',
      selector: 'Address',
      sortable: true,
    },
    {
      name: 'Classification',
      selector: 'Classification',
      sortable: true,
    },
    {
      name: 'Card UID',
      selector: 'cardId',
      sortable: true,
    },
    {
      name: 'Card SN',
      selector: 'Card_SN',
      sortable: true,
    },
    {
      name: 'Verification',
      selector: 'Verification',
      sortable: true,
    },
    {
      name: 'Status',
      selector: 'emailStatus',
      sortable: true,
    },
    {
      name: 'Action',
      selector: 'Action',
      sortable: true,
      cell: record => {
        console.log('record', record)
  
        return (
          // <Fragment>
          <>
            <button
              data-tip="Edit"
              data-toggle="modal"
              data-target="#Editdetais"
              className="action_tab"
  
              // onClick={() => editRecord(record._id)}
              style={{ marginRight: "5px" }}
            >
              <i className="fa fa-edit"></i>
              
            </button>
     
            <button
              data-tip="Edit"
              data-toggle="modal"
              data-target="#newmsg"
              className="action_tab"
  
              // onClick={() => editRecord(record._id)}
              style={{ marginRight: "5px" }}
            >
              <i className="fa fa-comments"></i>
              
            </button>
            </>
  
          // </Fragment>
        );
      }
    },
  ];
  const options = [
    { value: 'SND', label: 'SND' },
    { value: 'SNR', label: 'SNR' },
    { value: 'Alltransactions', label: 'All transactions' },
  ];

function RiderList() {
  const [startDate, setStartDate] = useState(new Date());
  const initialFormValue = {
    'name': '',
    'status': 'active'
}
  const [riderList, setriderList] = useState();
  const [formValue, setFormValue] = useState(initialFormValue);

  const getriderList = async () => {
    const { status, loading, message, result } = await getAllRider();
    console.log('getAllRider', result)
    if (status == 'success') {
      console.log('status', status)
      setriderList(result)
    }
    else {

    }


  }

  useEffect(() => {
    getriderList();
  },[]
  )
  
    return (
        <div>
           <div className="row mb-3">
             <div className="col-lg-12">
             <h1 className="tiltl_head_tetx">RidersList</h1>
             </div>
            <div className="col-lg-1">

            </div>
            <div className="col-lg-2">
              <div className="paddin_sss">
                <label></label>
                <Select
                options={options}
              />
              </div>
            
            </div>
            <div className="col-lg-2">
              <div className="paddin_sss">
                <label></label>
                <Select
                options={options}
              />
              </div>
            
            </div>
            <div className="col-lg-2">
              <div className="paddin_sss">
                <label></label>
                <Select
                options={options}
              />
              </div>
            
            </div>
            <div className="col-lg-5">
              <div className="search_download">
                <div className="input_1">
                    <input type="text" placeholder="Filter in Records..." />
                    <i class="fas fa-search"></i>
                </div>
                <div>
                  <button>Download <i class="fas fa-download"></i></button>
                </div>
              </div>
            </div>
            </div> 
            <div className="row">
              <div className="col-lg-12">
          <DataTable columns={columns} data={riderList} noHeader  className="table_new_s"  pagination={true} paginationPerPage="5" paginationRowsPerPageOptions={[5, 10, 15, 20]} />

              </div>
            </div>
          <div className="row">
            <div className="col-lg-12">
              <a href="" className="class_a" data-toggle="modal" data-target="#Adddetais"> <i class="fas fa-plus"></i> Add</a>
            </div>
          </div>
{/* 
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#newmsg">
  Open modal
</button> */}


<div class="modal" id="newmsg">
  <div class="modal-dialog modal-dialog-centered modal-md modal-dialog_cutom_width">
    <div class="modal-content">


      <div class="modal-header">
        <h4 class="modal-title model_heder_colo">New Message</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>


      <div class="modal-body">
        <div className="row">
          <div className="col-lg-12">
              <div className="send_msg_form">
                <label>Recepient:</label>
                <input type="text" />
              </div>
              <div className="send_msg_form">
                <label>Subject:</label>
                <input type="text" />
              </div>
          </div>
        </div>
        <div className="div_clas_po">
        <label>Text:</label>
       <CKEditor initData={<p>This is an example CKEditor 4 WYSIWYG editor instance.</p>} />
        </div>
       
      </div>

      <div class="modal-footer justify-content-between align-items-end">
        <div className="signature_sectooion">
          <label>signature</label>
          <div className="signature_filed">
            <div className="bottom_btn">
            <a href="">Change</a>
              <a href="">Remove</a>
            </div>
          </div>
        </div>
        <div className="button_section_model">
        <button type="button" class="btn dlelte" data-dismiss="modal">Delete</button>
        <button type="button" class="btn savemsg" >Save to Drafts</button>
        
        <button type="button" class="btn sendmsg" data-dismiss="modal">Send Message</button>
        </div>
       
      </div>

    </div>
  </div>
</div>
<div class="modal" id="Editdetais">
  <div class="modal-dialog modal-dialog-centered modal-md">
    <div class="modal-content">


      <div class="modal-header">
        <h4 class="modal-title model_heder_colo">Edit Details</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>


      <div class="modal-body edit_section_d_model">

      <div className="row mb-3">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Last Name</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="input_box_msg">
          <input type="text" />
         
          </div>
        </div>
       </div>
       <div className="row mb-3">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>First Name</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="input_box_msg">
          <input type="text" />
         
          </div>
        </div>
       </div>
       <div className="row mb-3">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Middle Name</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="input_box_msg">
          <input type="text" />
         
          </div>
        </div>
       </div>
       <div className="row mb-3">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Contact Number</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="input_box_msg">
          <input type="text" />
         
          </div>
        </div>
       </div>
       <div className="row">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Date of Birth</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="input_box_msg panndin_back">
          <DatePicker selected={startDate} onChange={(date) => setStartDate(date)} />

          </div>
        </div>
       </div>
       <div className="row mb-3">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Email Address</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="input_box_msg">
          <input type="text" />
         
          </div>
        </div>
       </div>
       <div className="row mb-3">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Address</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="input_box_msg">
          <input type="text" />
         
          </div>
        </div>
       </div>

       
       <div className="row mb-3">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Classification</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="input_box_msg">
          <Select
                options={options}
              />

          </div>
        </div>
       </div>
    
       <div className="row mb-3">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Card UID</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="input_box_msg">
          <input type="text" />
         
          </div>
        </div>
       </div>
       <div className="row mb-3">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Card SN</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="input_box_msg">
          <input type="text" />
         
          </div>
        </div>
       </div>

       <div className="row">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Identity Verification</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="content_clolk">
          <div className="img_section_78">
          <i class="far fa-image"></i>
          </div>
          <div className="image_options">
            <a href=""> View</a>
            <a href=""> Upload</a>

            <a href="" className="remover"> Remove</a>

          </div>
          </div>
        </div>
       </div>
       <div className="row">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Account Verification</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="content_clolk">
          <div className="img_section_78">
          <i class="far fa-image"></i>
          </div>
          <div className="image_options">
            <a href=""> View</a>
            <a href=""> Upload</a>

            <a href="" className="remover"> Remove</a>

          </div>
          </div>
        </div>
       </div>
       <div className="row">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Selfie Verification</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="content_clolk">
          <div className="img_section_78">
          <i class="far fa-image"></i>
          </div>
          <div className="image_options">
            <a href=""> View</a>
            <a href=""> Upload</a>

            <a href="" className="remover"> Remove</a>

          </div>
          </div>
        </div>
       </div>
       <div className="row">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Business Verification</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="content_clolk">
          <div className="img_section_78">
          <i class="fas fa-file-pdf"></i>
          </div>
          <div className="image_options">
            <a href=""> View</a>
            <a href=""> Upload</a>

            <a href="" className="remover"> Remove</a>

          </div>
          </div>
        </div>
       </div>


      

      

       

       
      
      
      
      
      
     
      
       <div className="row">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Status</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="content_clolk">
          
          <div className="content_clolk model_section">
          <div class="form-check form-check-inline d-flex align-items-center">
            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1" />
            <label class="form-check-label" for="inlineRadio1">Approved</label>
          </div>
          <div class="form-check form-check-inline d-flex align-items-center">
            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2" />
            <label class="form-check-label" for="inlineRadio2">Disapproved</label>
          </div>
          </div>
          </div>
        </div>
       </div>

        
       
      </div>

      <div class="modal-footer  align-items-end">
     
        <div className="button_section_model">
      
        <button type="button" class="btn savemsg" data-dismiss="modal" >Delete</button>
        
        <button type="button" class="btn sendmsg" data-dismiss="modal">Save</button>
        </div>
       
      </div>

    </div>
  </div>
</div>

<div class="modal" id="Adddetais">
  <div class="modal-dialog modal-dialog-centered modal-md">
    <div class="modal-content">


      <div class="modal-header">
        <h4 class="modal-title model_heder_colo">Add Riders List</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>


      <div class="modal-body edit_section_d_model">

      <div className="row mb-3">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Last Name</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="input_box_msg">
          <input type="text" />
         
          </div>
        </div>
       </div>
       <div className="row mb-3">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>First Name</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="input_box_msg">
          <input type="text" />
         
          </div>
        </div>
       </div>
       <div className="row mb-3">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Middle Name</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="input_box_msg">
          <input type="text" />
         
          </div>
        </div>
       </div>
       <div className="row mb-3">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Contact Number</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="input_box_msg">
          <input type="text" />
         
          </div>
        </div>
       </div>
       <div className="row">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Date of Birth</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="input_box_msg panndin_back">
          <DatePicker selected={startDate} onChange={(date) => setStartDate(date)} />

          </div>
        </div>
       </div>
       <div className="row mb-3">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Email Address</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="input_box_msg">
          <input type="text" />
         
          </div>
        </div>
       </div>
       <div className="row mb-3">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Address</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="input_box_msg">
          <input type="text" />
         
          </div>
        </div>
       </div>

       
       <div className="row mb-3">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Classification</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="input_box_msg">
          <Select
                options={options}
              />

          </div>
        </div>
       </div>
    
       <div className="row mb-3">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Card UID</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="input_box_msg">
          <input type="text" />
         
          </div>
        </div>
       </div>
       <div className="row mb-3">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Card SN</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="input_box_msg">
          <input type="text" />
         
          </div>
        </div>
       </div>

       <div className="row">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Identity Verification</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="content_clolk">
          <div className="img_section_78">
          <i class="far fa-image"></i>
          </div>
          <div className="image_options">
            <a href=""> View</a>
            <a href=""> Upload</a>

            <a href="" className="remover"> Remove</a>

          </div>
          </div>
        </div>
       </div>
       <div className="row">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Account Verification</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="content_clolk">
          <div className="img_section_78">
          <i class="far fa-image"></i>
          </div>
          <div className="image_options">
            <a href=""> View</a>
            <a href=""> Upload</a>

            <a href="" className="remover"> Remove</a>

          </div>
          </div>
        </div>
       </div>
       <div className="row">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Selfie Verification</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="content_clolk">
          <div className="img_section_78">
          <i class="far fa-image"></i>
          </div>
          <div className="image_options">
            <a href=""> View</a>
            <a href=""> Upload</a>

            <a href="" className="remover"> Remove</a>

          </div>
          </div>
        </div>
       </div>
       <div className="row">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Business Verification</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="content_clolk">
          <div className="img_section_78">
          <i class="fas fa-file-pdf"></i>
          </div>
          <div className="image_options">
            <a href=""> View</a>
            <a href=""> Upload</a>

            <a href="" className="remover"> Remove</a>

          </div>
          </div>
        </div>
       </div>


      

      

       

       
      
      
      
      
      
     
      
       <div className="row">
        <div className="col-lg-4 grif_place">
          <div className="label_section">
          <label>Status</label>
          </div>
            
        </div>
        <div className="col-lg-8">
          <div className="content_clolk">
          
          <div className="content_clolk model_section">
          <div class="form-check form-check-inline d-flex align-items-center">
            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1" />
            <label class="form-check-label" for="inlineRadio1">Approved</label>
          </div>
          <div class="form-check form-check-inline d-flex align-items-center">
            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2" />
            <label class="form-check-label" for="inlineRadio2">Disapproved</label>
          </div>
          </div>
          </div>
        </div>
       </div>

        
       
      </div>

      <div class="modal-footer  align-items-end">
     
        <div className="button_section_model">
      
        <button type="button" class="btn savemsg" data-dismiss="modal" >Delete</button>
        
        <button type="button" class="btn sendmsg" data-dismiss="modal">Add</button>
        </div>
       
      </div>

    </div>
  </div>
</div>
        </div>
    )
}

export default RiderList
