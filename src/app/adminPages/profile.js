import React, { useState, useEffect } from "react";
import DataTable from 'react-data-table-component';
import DatePicker from "react-datepicker";
import Select from 'react-select';
import "react-datepicker/dist/react-datepicker.css";
import { CKEditor } from 'ckeditor4-react';
import { getAllService, editService, addService, getserviceCSV} from '../actions/service';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { getTransportAdminInfo1, profileUpdate } from '../actions/transportCooperative'
import { useSelector } from 'react-redux'

import config from '../actions/config';



function VehicleService() {
  const userData = useSelector((state)=>state && state.auth && state.auth.user)

  const initialFormValue = {
    '_id': '',
    'role': '',
    'email':'',
    'name':'',
    'newpassword':'',
    'confirmpassword':'',
    'profileImage':'',
    'articleImage':'',
    'cdacertificate':'',
    'seccertificate':'',
    'businessPermit':'',
    'fareMatrix':'',
    'boardResolution':''
    

  }

  const [startDate, setStartDate] = useState(new Date());
  const [adminInfo, setAdminInfo] = useState('')
  const [frondend, setfrondend] = useState("");
const [profileImage1, setProfile] = useState("");
// const [ cdacertificate, setCdaCertificate] = useState('');
const [ seccertificate1, setSeccertificate] = useState('');
const [ articleImage1, setarticleImage] = useState('');
const [ boardResolution1, setboardResolution] = useState('');
const [ businessPermit1, setbusinessPermit] = useState('');

const [ fareMatrix1, setfareMatrix] = useState('');

const [ cdacertificate1, setCdaCertificate] = useState('');


  const [formValue, setFormValue] = useState(initialFormValue)
  const [optionValue, setOptionvalue] = useState()
  const [toggleNav, setToggleNav] = useState()

  const getAdminInfo = async (reqData) => {
    const { result, status, loading, error } = await getTransportAdminInfo1(reqData);
    if (status == 'success') {
      console.log('result',result)
      setAdminInfo(result[0])
      setProfile(config.baseUrl+"images/admin/"+userData.profileImage)
      setCdaCertificate(config.baseUrl+"images/admin/"+userData.cdacertificate)
      setSeccertificate(config.baseUrl+"images/admin/"+userData.seccertificate)
      setarticleImage(config.baseUrl+"images/admin/"+userData.articleImage)
      setboardResolution(config.baseUrl+"images/admin/"+userData.boardResolution)
      setbusinessPermit(config.baseUrl+"images/admin/"+userData.businessPermit)
      setfareMatrix(config.baseUrl+"images/admin/"+userData.fareMatrix)

    }
    else {
      console.log('err,e', error)
    }
  }
  const { name,email,newpassword,confirmpassword, 
    profileImage,
    articleImage,
    cdacertificate,
    seccertificate,
    businessPermit,
    fareMatrix,
    boardResolution} = formValue;
  const handleChange = (e) => {
    // e.preventDefault();
    console.log('1e', e)

    const { id, value } = e.target;
    // const { name} = e.value
    let formData = { ...formValue, ...{ [id]: value } }
    console.log('formData', formData)
    setFormValue(formData)
    
  }
  const imgchange = (e) => {
    let formData = { ...formValue, ...{ ["image"]: e.target.files[0] } }
    console.log('formData12', formData)
   setFormValue(formData)
  //  console.log('formData123', formData)

   setfrondend(formData.image.name)
 }
 const handleFile = async (e) => {
  const { id, files } = e.target;
  let formData = { ...formValue, ...{ [id]: files[0] } }
  setFormValue(formData)
  if(id=="profileImage"){
      setProfile(files[0].name)
  }
  if(id=="cdacertificate"){
    setCdaCertificate(files[0].name)
}
if(id=="seccertificate"){
  setSeccertificate(files[0].name)
}
if(id=="businessPermit"){
  setbusinessPermit(files[0].name)
}
if(id=="boardResolution"){
  setboardResolution(files[0].name)
}
  if(id=="articleImage"){
      setarticleImage(files[0].name)
  }
  if(id == 'fareMatrix'){
    setfareMatrix(files[0].name)
  }
  // if(name=="selfiImage"){
  //     setSelfie(files[0].name)
  // }
  // if (!isEmpty(validateError)) {
  //     setValidateError({})
  // }
}

 const handleSubmit = async () => {

  let reqData = {
   name,email,newpassword,confirmpassword, 
   profileImage,
   articleImage,
    cdacertificate,
    seccertificate,
    businessPermit,
    fareMatrix,
    boardResolution
  }
  const formData = new FormData();
  // formData.append('_id',userData._id)
  formData.append('name',adminInfo.name)
  
  formData.append('email', adminInfo.email);
  formData.append('newpassword', formValue.newpassword);
  formData.append('profileImage',profileImage1);
  formData.append('confirmpassword', formValue.confirmpassword);
  formData.append('articleImage', articleImage1);
  formData.append('cdacertificate', cdacertificate1);
  formData.append('seccertificate', seccertificate1);
  formData.append('businessPermit', businessPermit1);
  formData.append('fareMatrix', fareMatrix1);
  formData.append('boardResolution', boardResolution1);

  const { status, loading, message, error } = await profileUpdate(formData);
  if (status == "success") {
    // console.log('formData1233', formData)

    // window.location.reload();
    toast(message, {
      position: toast.POSITION.TOP_CENTER,
    });
    // getAdminInfo()
    // alert(message)

    console.log('formData', formData)
    // alert(message)
  }
  else {
    console.log('err', error)
  }
 }
  const editR = async (record) => {

    // e.preventDefault();
    // const { id, value } = e.target;
    // let formData = { ...formValue, ...{ [id]: value } }
    // setFormValue()    
    console.log('record-----', record)
    setFormValue(record)
    // setKycStatusNo(record.kycStatusNo)
  }

  // const handleSearch = async (e) => {
  //   console.log('e', e.target.value)
  //   let reqData = {
  //     page: 1,
  //     limit: 10,
  //     search: e.target.value,
  //     // from: startDate,
  //     // to: endDate
  //   }
  //   getAdminInfo(reqData)
  // }

  useEffect(() => {
    if
    (userData && userData._id) {
      let user = {'_id': userData._id}
      // console.log('user',user)
      getAdminInfo(user)
      
    }
    
    

  },[])

  return (
    <div>
      <div className="row mb-3">
        <div className="col-lg-12">
          <h1 className="tiltl_head_tetx">Profile</h1>
        </div>
        <div className="col-lg-12 mt-3">
         <div className="profile_title_sectpk">
          <h1>Hello {userData.name}</h1>
          <p>Welcome Back</p>
         </div>
        </div>
        <div className="boder_"></div>

        <div className="col-lg-12">
         <div className="profile_img_section">
          <div className="profile-imnd">
          <img src={profileImage1} alt = "" width="40" 
                 height="40"/> <br/> <br/>
          </div>
          <div className="button_update_profile">
          <a className="input_yy">   <input type="file" name="profileImage"  onChange={handleFile} id="profileImage" className="clipload"/> Change Photo </a>

            {/* <button className="input_yy" type ="file" onChange={imgchange} id="profileImage">Change Photo</button> */}
            <button>Remove Photo</button>
          </div>
         </div>
        </div>
        <div className="col-lg-12 pt-5 form_profile">
        <form className="row">
         <div className="col-lg-6">
         <div class="form-group row">
          <label for="" class="col-sm-4 col-form-label">Name</label>
          <div class="col-sm-8">
            <input type="text" onChange={handleChange}class="form-control" id="name"  value={adminInfo.name}placeholder="" />
          </div>
        </div>
        <div class="form-group row">
          <label for="" class="col-sm-4 col-form-label">Email Address</label>
          <div class="col-sm-8">
            <input type="text" onChange={handleChange}class="form-control" id="email"  value={adminInfo.email} />
          </div>
        </div>
        <div class="form-group row">
          <label for="" class="col-sm-4 col-form-label">Change Password</label>
          <div class="col-sm-8">
            <input type="password" onChange={handleChange}class="form-control" id="newpassword"  value={formValue.newpassword}   placeholder="" />
          </div>
        </div>
        <div class="form-group row">
          <label for="" class="col-sm-4 col-form-label">Confirm Password</label>
          <div class="col-sm-8">
            <input type="password" class="form-control" onChange={handleChange} id="confirmpassword" value={formValue.confirmpassword} placeholder="" />
          </div>
        </div>
         </div>
         <div className="col-lg-6 edit_section_d_model edit_section_d_model_new">
         <div class="form-group row">
          <label for="" class="col-sm-5 col-form-label">CDA certification</label>
          <div class="col-sm-7">
          <div class="content_clolk">
            <div class="img_section_78">
            {/* <img src={config.baseUrl+"images/admin/"+userData.cdacertificate} alt = "" width="40" 
                 height="40"/> <br/> <br/> */}
            <i class="fas fa-file-pdf"></i>
            </div>
            <div class="image_options">
            <a href={cdacertificate1}  target="_blank">View</a>

            <a className="input_yy"> 
              <input type="file"    onChange={handleFile} id="cdacertificate" className="clipload"/> Upload </a>

              {/* <a href=""  onChange={imgchange} id="cdacertificate"class="remover"> Upload</a> */}
              <a href="" class="remover"> Remove</a>
              </div>
              </div>
          </div>
        </div>
        <div class="form-group row">
          <label for="" class="col-sm-5 col-form-label">SEC certification</label>
          <div class="col-sm-7">
          <div class="content_clolk">
            <div class="img_section_78">
            {/* <img src={config.baseUrl+"images/admin/"+userData.seccertificate} alt = "" width="40" 
                 height="40"/> <br/> <br/> */}
            <i class="fas fa-file-pdf"></i>
            </div>
            <div class="image_options">
            <a href={seccertificate1}  target="_blank">View</a>
            <a className="input_yy">   <input type="file"    onChange={handleFile} id="seccertificate" className="clipload"/> Upload </a>

              {/* <a href="" onChange={imgchange} id="seccertificate" class="remover"> Upload</a> */}
              <a href="" class="remover"> Remove</a>
              </div>
              </div>
          </div>
        </div>
        <div class="form-group row">
          <label for="" class="col-sm-5 col-form-label">Articles and by the laws</label>
          <div class="col-sm-7">
          <div class="content_clolk">
            <div class="img_section_78">
            {/* <img src={config.baseUrl+"images/admin/"+userData.articleImage} alt = "" width="40" 
                 height="40"/> <br/> <br/>    */}
            <i class="fas fa-file-pdf"></i>
            </div>
            <div class="image_options">
            <a href={articleImage1}  target="_blank">View</a>
            {/* <a href=""  onChange={imgchange} id="articleImage" > Upload</a> */}
            <a className="input_yy">   <input type="file"    onChange={handleFile} id="articleImage" className="clipload"/> Upload </a>

              <a href="" class="remover"> Remove</a>
              </div>
              </div>
          </div>
        </div>
        <div class="form-group row">
          <label for="" class="col-sm-5 col-form-label">Business Permit</label>
          <div class="col-sm-7">
          <div class="content_clolk">
            <div class="img_section_78">
            {/* <img src={config.baseUrl+"images/admin/"+userData.businessPermit} alt = "" width="40" 
                 height="40"/> <br/> <br/>  */}
            <i class="fas fa-file-pdf"></i>
            </div>
            <div class="image_options">
            <a href={businessPermit1}  target="_blank">View</a>
            {/* <a href className="remover" >
            <input
                                type="file"
                                className="remover"
                                id="businessPermit"                   
                                onChange={imgchange}                
                                            />    upload      
                                            </a>       */}
                                            
                              <a className="input_yy">   <input type="file"    onChange={handleFile} id="businessPermit" className="clipload"/> Upload </a>
              <a href="" class="remover"> Remove</a>
              </div>
              </div>
          </div>
        </div>
        <div class="form-group row">
          <label for="" class="col-sm-5 col-form-label">Board Resolution</label>
          <div class="col-sm-7">
          <div class="content_clolk">
            <div class="img_section_78">
            {/* <img src={config.baseUrl+"images/admin/"+userData.boardResolution} alt = "" width="40" 
                 height="40"/> <br/> <br/>   */}
            <i class="fas fa-file-pdf"></i>
            </div>
            <div class="image_options">
              
            <a href={boardResolution1}  target="_blank">View</a>
            <a className="input_yy">   <input type="file"    onChange={handleFile} id="boardResolution" className="clipload"/> Upload </a>

                  {/* <a href=""   onChange={imgchange} id="boardResolution" class="remover"> Upload</a> */}
              <a href="" class="remover"> Remove</a>
              </div>
              </div>

          </div>
        </div>
        <div class="form-group row">
          <label for="" class="col-sm-5 col-form-label">Fare Matrix</label>
          <div class="col-sm-7">
          <div class="content_clolk">
            <div class="img_section_78">
            {/* <img src={config.baseUrl+"images/admin/"+userData.fareMatrix} alt = "" width="40" 
        height="40"/> <br/> <br/>   */}
            <i class="fas fa-file-pdf"></i>
            </div>
            <div class="image_options">
            <a href={fareMatrix1}  target="_blank">View</a>
            <a className="input_yy">   <input type="file"    onChange={handleFile} id="fareMatrix" className="clipload"/> Upload </a>

              {/* <a href=""  onChange={imgchange} id="fareMatrix" class="remover"> Upload</a> */}
              <a href="" class="remover"> Remove</a>
              </div>
              </div>
          </div>
        </div>
         </div>
        </form>
        </div>
        <div className="col-lg-12">
          <div className="bottom_button_ss">
          <div class="button_section_model">
            <button type="button" class="btn savemsg">Cancel</button>
            <button type="button" onClick={handleSubmit} class="btn sendmsg">Save</button>
            </div>
          </div>
        </div>
        </div>
    </div>
  )
}

export default VehicleService
