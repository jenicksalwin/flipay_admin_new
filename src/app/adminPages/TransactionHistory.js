import React, { useState, useEffect } from "react";
import DataTable from 'react-data-table-component';
import DatePicker from "react-datepicker";
import Select from 'react-select';
import "react-datepicker/dist/react-datepicker.css";
import { CKEditor } from 'ckeditor4-react';
import { getTransactionHistory,getStatusInfo, getTypeInfo, getTransactionHistoryCsv } from '../actions/rider';

// Trade History Table
const data = [
  {
    Name: "",
    Transport_Cooperative_Corporation: "",
    Device: "",
    Type_of_Transaction: "card",
    Transaction_Number: "",
    Date_of_Transaction: "",
    Amount: "",
    Balance: "",

  },
  {
    Name: "",
    Transport_Cooperative_Corporation: "",
    Device: "",
    Type_of_Transaction: "card",
    Transaction_Number: "",
    Date_of_Transaction: "",
    Amount: "",
    Balance: "",

  },
  {
    Name: "",
    Transport_Cooperative_Corporation: "",
    Device: "",
    Type_of_Transaction: "card",
    Transaction_Number: "",
    Date_of_Transaction: "",
    Amount: "",
    Balance: "",
  },

];
const columns = [
  {
    name: 'NAME',
    selector: 'acc_holder_name',
    sortable: true,

  },
  {
    name: 'TYPE OF TRANSACTION',
    selector: 'typeInfo',
    sortable: true,
  },
  {
    name: 'TRANSACTION NUMBER',
    selector: 'transaction_number',
    sortable: true,
  },
  {
    name: 'DATE OF TRANSACTION',
    selector: 'transaction_date',
    sortable: true,

  },
  {
    name: 'AMOUNT',
    selector: 'amount',
    sortable: true,

  },
  {
    name: 'BALANCE',
    selector: 'Balance',
    sortable: true,

  },
  {
    name: 'STATUS',
    selector: 'cashinStatus',
    sortable: true
  },

  {
    name: 'ACTION',
    selector: 'Action',
    sortable: true,
    cell: record => {
      return (
        <>
          <button
            data-tip="Edit"
            data-toggle="modal"
            data-target="#Editdetais"
            className="action_tab buttn_trans"
          // onClick={() => editR(record)}
            style={{ marginRight: "5px" }}
          >
            <i className="fas fa-edit"></i>
          </button>
        </>
      );
    }
  },
];

function TransactionHistory() {
  const [startDate, setStartDate] = useState(new Date());
  const [endDate, setEndDate] = useState(new Date());

  const initialFormValue = {
    'name': '',
    'status': 'active',
    'count': ''
  }
  const options = [
    { value: 'Approved', label: 'Approved', id:'Approved' },
    { value: 'Rejected', label: 'Rejected', id:'Rejected' },
    { value: 'submitted', label: 'submitted', id:'submitted' },
  ];
  const Options1 = [
    { value: 'cash_in', label: 'Cashin', id:'cash_in'},
    { value: 'loadCard', label: 'Loadcard', id:'loadCard'},
  ];
  // const formData { count} = 
  const [TransactionHistory, setTransactionHistory] = useState();
  const [statusOption, setStatusOption] = useState();
  const [typeinfoOption, settypeInfoOption] = useState();
  const [formValue, setFormValue] = useState(initialFormValue);
  const [count, setCount] = useState()
  const getLoadcard = async (reqData) => {
    const { status, loading, message, result, count } = await getTransactionHistory(reqData);
    console.log('results', count)
    if (status == 'success') {
      console.log('status', status)
      setTransactionHistory(result)
      setCount(count)
    }
    else {

    }


  }
  const handleSearch = async (e) => {
    console.log('e', e.target.value)
    let reqData = {
      page: 1,
      limit: 10,
      search: e.target.value,
      count: count
      // from: startDate,
      // to: endDate
    }
    getLoadcard(reqData)
  }

  const handleOptionStatus = (e) => {
    console.log('selectedOption', e)
    let reqData = {
      search:e.value
  }
  getStatus(reqData);
  }

  const handleOptionTypeInfo = (e) => {
    console.log('selectedOption', e)
    let reqData = {
      search:e.value
  }
  typeInfo(reqData);
  }

  const  getStatus = async (reqData) =>  {
    // this.setState({ "loader": true })
    try {
        const { status, result, message } = await getStatusInfo(reqData);
        console.log('reqData',reqData)
        if(status){
            setTransactionHistory(result)
            console.log('result',result)

        }
       
    } catch (err) { }
}
const typeInfo = async (reqData) => {

  // this.setState({ "loader": true })
  try {
      const { status, result, message } = await getTypeInfo(reqData);
      console.log('result',result)
      if(status){
        setTransactionHistory(result)

        // settypeInfoOption(result)
        console.log('result1',result)
      }
     
  } catch (err) { 

    
  }
}
  const handleStartDate = (e) => {
    console.log('e', e)
    setStartDate(e)
  }
  const handleEndDate = async (e) => {
    console.log('startDate', startDate)
    console.log('endDate', endDate)
    setEndDate(e)
    let reqData = {
      page: "",
      limit: "",
      search: "",
      from: startDate,
      to: endDate
    }
    getLoadcard(reqData)
    console.log(reqData, 'reqData')
  }

  const handleDownloadCSV = async () => {
    // const { page, limit, search, from, to } = this.state;
    let reqData = {
      records: [],
      search: '',
      options: [],
      page: 1,
      limit: 10,
      count: count,
      loader: false,
      from: undefined,
      to: undefined,
      selectedOption: null,
      // search,
      // from = 1,
      // to
    }
    getTransactionHistoryCsv(reqData)
  }

  useEffect(() => {
    getLoadcard();
    getStatus();
    typeInfo(); 
  }, []
  )
  return (
    <div>
      <div className="row">
        <div className="col-lg-12">
          <h1 className="tiltl_head_tetx">Transaction History</h1>
        </div>

        <div className="col-lg-3">
          <div className="d-flex justify-content-center">
            <div className="calaleder_section">
              <label>From</label>
              <div className="datepiker_sec">
                <DatePicker selected={startDate} onChange={(date) => handleStartDate(date)} />
                <i class="far fa-calendar-alt"></i>
              </div>
            </div>
            <div className="calaleder_section">
              <label>To</label>
              <div className="datepiker_sec">
                <DatePicker selected={endDate} onChange={(date) => handleEndDate(date)} />
                <i class="far fa-calendar-alt"></i>
              </div>
            </div>
          </div>


        </div>
        <div className="col-lg-4">
          <div className="justify-content-around d-flex">
            <div className="paddin_sss w-100">
              <label></label>
              <Select
                id="status"
                value={options.value}
                onChange={handleOptionStatus}
                options={options}

              />
            </div>
            <div className="paddin_sss w-100">
              <label></label>
              <Select
              id="typeInfo"
              value={Options1.value}
              onChange={handleOptionTypeInfo}
                options={Options1}
              />
            </div>
          </div>


        </div>
        <div className="col-lg-5">
          <div className="search_download ">
            <div className="input_1">
              <input type="text" onChange={handleSearch} placeholder="Filter in Records..." />
              <i class="fas fa-search"></i>
            </div>
            <div>
              <button onClick={handleDownloadCSV}>Download <i class="fas fa-download"></i></button>
            </div>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-lg-12">
          <DataTable columns={columns} data={TransactionHistory} noHeader className="table_new_s"            noDataComponent="Loading..." 
 pagination={true} paginationPerPage="5" paginationRowsPerPageOptions={[5, 10, 15, 20]} />

        </div>
      </div>
      <div className="row">
        <div className="col-lg-12">
          <a href="" className="class_a" data-toggle="modal" data-target="#Editdetais"> <i class="fas fa-plus"></i> Add</a>
        </div>
      </div>



    </div>
  )
}

export default TransactionHistory
