
import axios from './axios';

export const getAllDevice = async (data) => {
    try {
        let respData = await axios({
            'method': 'get',
            'url': `/adminapi/device`,
            'headers': {
                'Authorization': localStorage.adminToken
            },
            'params': data
        });
        return {
            status: "success",
            loading: false,
            count: respData.data.count,
            result: respData.data.result
        }
    } catch (err) {
        return {
            status: "failed",
            loading: false,
            message: err.response.data.message,
            error: err.response.data.errors
        }
    }
};

export const getDeviceDropDown = async (data) => {
    try {
        let respData = await axios({
            'method': 'get',
            'url': `/adminapi/deviceDropDownNew`,
        });
        return {
            status: "success",
            loading: false,
            result: respData.data.result
        }
    } catch (err) {
        return {
            status: "failed",
            loading: false,
        }
    }
};

export const addDevice = async (data) => {
    try {
        let respData = await axios({
            'method': 'post',
            'url': `/adminapi/device`,
            'headers': {
                'Authorization': localStorage.adminToken
            },
            data
        });
        return {
            status: "success",
            loading: false,
            message: respData.data.message,
        }
    } catch (err) {
        return {
            status: "failed",
            loading: false,
            message: err.response.data.message,
            error: err.response.data.errors
        }
    }
};

export const editDevice = async (data) => {
    try {
        let respData = await axios({
            'method': 'put',
            'url': `/adminapi/device`,
            'headers': {
                'Authorization': localStorage.adminToken
            },
            data
        });
        return {
            status: "success",
            loading: false,
            message: respData.data.message,
        }
    } catch (err) {
        return {
            status: "failed",
            loading: false,
            message: err.response.data.message,
            error: err.response.data.errors
        }
    }
};