
import axios from './axios';

export const addtransportAdminInfo = async (data) => {
    try {
        let respData = await axios({
            'method': 'post',
            'url': `/adminapi/editTransportInfo`,
            'headers': {
                'Authorization': localStorage.adminToken
            },
            data
        });
        return {
            status: "success",
            loading: false,
            message: respData.data.message,
            result: respData.data.result
        }
    } catch (err) {
        return {
            status: "failed",
            loading: false,
            message: err.response.data.message,
            error: err.response.data.errors
        }
    }
};
export const getdeleteInfo = async (data) => {

    try {
        let respData = await axios({
            'method': 'post',
            'url': `/adminapi/deleteAdmin`,
            'headers': {
                'Authorization': localStorage.adminToken
            },
            data
        });
        return {
            status: "success",
            loading: false,
            message: respData.data.message,
            result: respData.data.result
        }
    } catch (err) {
        return {
            status: "failed",
            loading: false,
            message: err.response.data.message,
            error: err.response.data.errors
        }
    }
};

export const edittransportAdminInfo = async (data) => {
    try {
        let respData = await axios({
            'method': 'post',
            'url': `/adminapi/editTransportInfo`,
            'headers': {
                'Authorization': localStorage.adminToken
            },
            data
        });
        return {
            status: "success",
            loading: false,
            message: respData.data.message,
            result: respData.data.result
        }
    } catch (err) {
        return {
            status: "failed",
            loading: false,
            message: err.response.data.message,
            error: err.response.data.errors
        }
    }
};

export const getTransportAdminInfo = async (data) => {
    try {
        let respData = await axios({
            'method': 'post',
            'url': `/adminapi/transportadminInfo`,
            'params': data
        });
        return {
            status: "success",
            loading: false,
            result: respData.data.data
        }
    } catch (err) {
        return {
            status: "failed",
            loading: false,
            message: err.response.data.message,
            error: err.response.data.errors
        }
    }
};
export const getTransportAdminInfo1 = async (data) => {
    
    try {
        let respData = await axios({
            'method': 'post',
            'url': `/adminapi/ProfiletransportadminInfo`,
            'data':data
        });
        return {
            status: "success",
            loading: false,
            result: respData.data.result
        }
    } catch (err) {
        return {
            status: "failed",
            loading: false,
            message: err.response.data.message,
            error: err.response.data.errors
        }
    }
};
export const profileUpdate = async (data) => {
    try {
        let respData = await axios({
            'method': 'post',
            'url': `/adminapi/ProfileUpdate`,
            'headers': {
                'Authorization': localStorage.adminToken
            },
            data
        });
        return {
            status: "success",
            loading: false,
            message: respData.data.message,
            result: respData.data.result
        }
    } catch (err) {
        return {
            status: "failed",
            loading: false,
            message: err.response.data.message,
            error: err.response.data.errors
        }
    }
};