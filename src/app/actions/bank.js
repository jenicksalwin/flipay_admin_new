
import axios from './axios';

export const addVehicle = async (data) => {
    try {
        console.log("data",data);
        let respData = await axios({
            'method': 'post',
            'url': `/adminapi/addBank`,
            data
        });
        return {
            status: "success",
            loading: false,
            message: respData.data.message,
            result: respData.data.result
        }
    } catch (err) {
        return {
            status: "failed",
            loading: false,
            message: err.response.data.message,
            error: err.response.data.errors
        }
    }
};

export const editVehicle = async (data) => {
    try {
        console.log("data------->",data);
        let respData = await axios({
            'method': 'post',
            'url': `/adminapi/editBank`,
            data
        });
        return {
            status: "success",
            loading: false,
            message: respData.data.message,
            result: respData.data.result
        }
    } catch (err) {
        return {
            status: "failed",
            loading: false,
            message: err.response.data.message,
            error: err.response.data.errors
        }
    }
};

export const getAllVehicle = async (data) => {
    try {
        let respData = await axios({
            'method': 'get',
            'url': `/adminapi/BankList`,
            data
        });
        return {
            status: "success",
            loading: false,
            result: respData.data.result
        }
    } catch (err) {
        return {
            status: "failed",
            loading: false,
            message: err.response.data.message,
            error: err.response.data.errors
        }
    }
};
