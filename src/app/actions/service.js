
import axios from './axios';

export const addService = async (data) => {
    try {
        let respData = await axios({
            'method': 'post',
            'url': `/adminapi/addserviceNew`,
            data
        });
        return {
            status: "success",
            loading: false,
            message: respData.data.message,
            result: respData.data.result
        }
    } catch (err) {
        return {
            status: "failed",
            loading: false,
            message: err.response.data.message,
            error: err.response.data.errors
        }
    }
};

export const getserviceCSV = async (data) => {
    try {
        let respData = await axios({
            'method': 'get',
            'url': `/adminapi/getserviceCSV`,
            'params': data
        });

        const url = window.URL.createObjectURL(new Blob([respData.data]));
        const link = document.createElement('a');
        link.href = url;
        link.setAttribute('download', 'Vehicle.csv');
        document.body.appendChild(link);
        link.click();

        return true
    } catch (err) {
        return false
    }
};
export const editService = async (data) => {
    try {
        let respData = await axios({
            'method': 'put',
            'url': `/adminapi/editserviceNew`,
            data
        });
        return {
            status: "success",
            loading: false,
            message: respData.data.message,
            result: respData.data.result
        }
    } catch (err) {
        return {
            status: "failed",
            loading: false,
            message: err.response.data.message,
            error: err.response.data.errors
        }
    }
};

export const getAllService = async (data) => {
    try {
        let respData = await axios({
            'method': 'get',
            'url': `/adminapi/getservice`,
            'params' : data
        });
        console.log('respData',respData)
        return {
            status: "success",
            loading: false,
            result: respData.data.result,
            count: respData.data.count
        }
    } catch (err) {
        return {
            status: "failed",
            loading: false,
            message: err.response.data.message,
            error: err.response.data.errors
        }
    }
};
export const addServiceSNew = async (data) => {
    try {
        let respData = await axios({
            'method': 'post',
            'url': `/adminapi/addserviceNew`,
            data
        });
        return {
            status: "success",
            loading: false,
            message: respData.data.message,
            result: respData.data.result
        }
    } catch (err) {
        return {
            status: "failed",
            loading: false,
            message: err.response.data.message,
            error: err.response.data.errors
        }
    }
};

export const editServiceNew = async (data) => {
    try {
        let respData = await axios({
            'method': 'put',
            'url': `/adminapi/editserviceNew`,
            data
        });
        return {
            status: "success",
            loading: false,
            message: respData.data.message,
            result: respData.data.result
        }
    } catch (err) {
        return {
            status: "failed",
            loading: false,
            message: err.response.data.message,
            error: err.response.data.errors
        }
    }
};
