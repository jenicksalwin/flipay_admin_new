
import axios from './axios';

export const getAllRider = async (data) => {
    try {
        let respData = await axios({
            'method': 'get',
            'url': `/adminapi/riderList`,
            data
        });
        console.log(respData, "resDataUsersssssssssss")
        return {
            status: "success",
            loading: false,
            result: respData.data.result
        }

    } catch (err) {
        return {
            status: "failed",
            loading: false,
            message: err.response.data.message,
            error: err.response.data.errors
        }
    }
};


export const getAllUsers = async (data) => {
    try {
        let respData = await axios({
            'method': 'get',
            'url': `/adminapi/userList`,
            'params': data

        });

        return {
            status: "success",
            loading: false,
            result: respData.data.result
        }
    } catch (err) {
        return {
            status: "failed",
            loading: false,
            message: err.response.data.message,
            error: err.response.data.errors
        }
    }
};

export const getAllSubAdminInfo = async () => {
    try {
        let respData = await axios({
            'method': 'get',
            'url': `/adminapi/getAllSubAdminInfo`,

        });
        console.log("getAllSubAdminInfogetAllSubAdminInfo", respData);

        return {
            status: "success",
            loading: false,
            result: respData.data.result
        }
    } catch (err) {
        return {
            status: "failed",
            loading: false,
            message: err.response.data.message,
            error: err.response.data.errors
        }
    }
};

export const getAllDistInfo = async () => {
    try {
        let respData = await axios({
            'method': 'get',
            'url': `/adminapi/distList`
        });
        console.log("resssssdddd", respData)
        return {
            status: "success",
            loading: false,
            result: respData.data.result
        }

    } catch (err) {
        return {
            status: "failed",
            loading: false,
            message: err.response.data.message,
            error: err.response.data.errors
        }
    }
};
export const statusChanged = async (data) => {
    console.log("status changed")
    console.log("statuschangeeieieieiie", localStorage.adminToken);
    try {
        let respData = await axios({
            'method': 'post',
            'url': `/adminapi/statusKyc`,
            'headers': {
                'Authorization': localStorage.adminToken
            },
            data

        });
        return {
            status: "success",
            loading: false,
            result: respData.data.success,
            message: respData.data.message

        }
    } catch (err) {
        return {
            status: "failed",
            loading: false,
            message: err.response.data.message,
            error: err.response.data.errors
        }
    }
};
export const statusRejected = async (data) => {
    console.log("status Rejected changed")
    try {
        let respData = await axios({
            'method': 'post',
            'url': `/adminapi/statusKycRejected`,
            'headers': {
                'Authorization': localStorage.adminToken
            },
            data
        });
        return {
            status: "success",
            loading: false,
            result: respData.data.result
        }
    } catch (err) {
        return {
            status: "failed",
            loading: false,
            message: err.response.data.message,
            error: err.response.data.errors
        }
    }
};
export const getRideAdminInfo = async (data) => {
    try {
        let respData = await axios({
            'method': 'post',
            'url': `/adminapi/getRideAdminInfo`,
            'headers': {
                'Authorization': localStorage.adminToken
            },
            data
        });
        return {
            status: "success",
            loading: false,
            count: respData.data.count,
            result: respData.data.result
        }
    } catch (err) {
        return {
            status: "failed",
            loading: false,
            message: err.response.data.message,
            error: err.response.data.errors
        }
    }
};

export const getRideHistory = async (data) => {
    try {
        let respData = await axios({
            'method': 'get',
            'url': `/adminapi/rideHistoryNew`,
            'headers': {
                'Authorization': localStorage.adminToken
            },
            'params': data
        });
        return {
            status: "success",
            loading: false,
            count: respData.data.count,
            result: respData.data.result
        }
    } catch (err) {
        return {
            status: "failed",
            loading: false,
            message: err.response.data.message,
            error: err.response.data.errors
        }
    }
};

export const getRideHistoryCsv = async (data) => {
    try {
        let respData = await axios({
            'method': 'get',
            'url': `/adminapi/rideHistoryCsv`,
            'params': data
        });

        const url = window.URL.createObjectURL(new Blob([respData.data]));
        const link = document.createElement('a');
        link.href = url;
        link.setAttribute('download', 'rideHistory.csv');
        document.body.appendChild(link);
        link.click();

        return true
    } catch (err) {
        return false
    }
};

export const getFareIncome = async (data) => {
    try {
        let respData = await axios({
            'method': 'get',
            'url': `/adminapi/getFareIncome`,
            'headers': {
                'Authorization': localStorage.adminToken
            },
            'params': data
        });
        return {
            status: "success",
            loading: false,
            count: respData.data.count,
            result: respData.data.result
        }
    } catch (err) {
        return {
            status: "failed",
            loading: false,
            message: err.response.data.message,
            error: err.response.data.errors
        }
    }
};

export const getFareIncomeCsv = async (data) => {
    console.log('data123',data)
    try {
        let respData = await axios({
            'method': 'get',
            'url': `/adminapi/getFareIncomeCsv`,
            'params': data
        });

        const url = window.URL.createObjectURL(new Blob([respData.data]));
        const link = document.createElement('a');
        link.href = url;
        link.setAttribute('download', 'FareIncome.csv');
        document.body.appendChild(link);
        link.click();

        return true
    } catch (err) {
        return false
    }
};



export const getTransactionHistoryCsv = async (data) => {
    try {
        let respData = await axios({
            'method': 'get',
            'url': `/adminapi/TransactionHistoryCsv`,
            'params': data
        });

        const url = window.URL.createObjectURL(new Blob([respData.data]));
        const link = document.createElement('a');
        link.href = url;
        link.setAttribute('download', 'TransactionHistory.csv');
        document.body.appendChild(link);
        link.click();

        return true
    } catch (err) {
        return false
    }
};
export const addCardId = async (data) => {
    try {
        let respData = await axios({
            'method': 'post',
            'url': `/adminapi/addCard-rider`,
            data
        });
        return {
            status: "success",
            loading: false,
            result: respData.data.result
        }
    } catch (err) {
        return {
            status: "failed",
            loading: false,
            message: err.response.data.message,
            error: err.response.data.errors
        }
    }
};

export const addRider = async (data) => {
    console.log('data',data)
    try {
        let respData = await axios({
            'method': 'post',
            'url': `/adminapi/rider`,
            data
        });
        return {
            status: "success",
            loading: false,
            message: respData.data.message,
            result: respData.data.result
        }
    } catch (err) {
        return {
            status: "failed",
            loading: false,
            message: err.response.data.message,
            error: err.response.data.errors
        }
    }
};

export const editRider = async (data) => {
    try {
        let respData = await axios({
            'method': 'put',
            'url': `/adminapi/rider`,
            data
        });
        return {
            status: "success",
            loading: false,
            message: respData.data.message,
            result: respData.data.result
        }
    } catch (err) {
        return {
            status: "failed",
            loading: false,
            message: err.response.data.message,
            error: err.response.data.errors
        }
    }
};

export const getRiderWallet = async (data) => {
    try {
        let respData = await axios({
            'method': 'get',
            'url': `/adminapi/riderWallet`,
            'params': data
        });
        return {
            status: "success",
            loading: false,
            count: respData.data.count,
            result: respData.data.result
        }
    } catch (err) {
        return {
            status: "failed",
            loading: false,
            message: err.response.data.message,
            error: err.response.data.errors
        }
    }
}

export const addRiderBalance = async (data) => {
    try {
        let respData = await axios({
            'method': 'post',
            'url': `/adminapi/riderWallet`,
            data
        });
        return {
            status: "success",
            loading: false,
            message: respData.data.message,
        }
    } catch (err) {
        return {
            status: "failed",
            loading: false,
            message: err.response.data.message,
            error: err.response.data.errors
        }
    }
}
export const getLoadHistory = async (data) => {
    try {
        let respData = await axios({
            'method': 'get',
            'url': `/adminapi/adminloadcardHistory`,
            'params': data
        });
        return {
            status: "success",
            loading: false,
            count: respData.data.count,
            result: respData.data.data
        }
    } catch (err) {
        return {
            status: "failed",
            loading: false,
            message: err.response.data.message,
            error: err.response.data.errors
        }
    }
};
export const getTransactionHistory = async (data) => {
    try {
        let respData = await axios({
            'method': 'get',
            'url': `/adminapi/TransactionHistory`,
            'params': data
        });
        return {
            status: "success",
            loading: false,
            count: respData.data.count,
            result: respData.data.data
        }
    } catch (err) {
        return {
            status: "failed",
            loading: false,
            message: err.response.data.message,
            error: err.response.data.errors
        }
    }
};

export const getAllDistributionInfo = async () => {
    try {
        let respData = await axios({
            'method': 'get',
            'url': `/adminapi/distInfo`,

        });

        return {
            status: "success",
            loading: false,
            result: respData.data.result
        }
    } catch (err) {
        return {
            status: "failed",
            loading: false,
            message: err.response.data.message,
            error: err.response.data.errors
        }
    }
};
export const getLoadHistoryCsv = async (data) => {
    try {
        let respData = await axios({
            'method': 'get',
            'url': `/admindistapi/loadCardHistoryCsv`,
            'params': data
        });

        const url = window.URL.createObjectURL(new Blob([respData.data]));
        const link = document.createElement('a');
        link.href = url;
        link.setAttribute('download', 'LoadCardHistoryCsv.csv');
        document.body.appendChild(link);
        link.click();

        return true
    } catch (err) {
        return false
    }
};
export const getAdminLoadHistoryCsv = async (data) => {
    try {
        let respData = await axios({
            'method': 'get',
            'url': `/adminapi/AdminloadCardHistoryCsv`,
            'params': data
        });

        const url = window.URL.createObjectURL(new Blob([respData.data]));
        const link = document.createElement('a');
        link.href = url;
        link.setAttribute('download', 'LoadCardHistoryCsv.csv');
        document.body.appendChild(link);
        link.click();

        return true
    } catch (err) {
        return false
    }
};

export const getAdminDataName = async () => {
    try {
        let respData = await axios({
            'method': 'get',
            'url': `/adminapi/getAdminName`,
        });

        if (respData.data.status) {
            return {
                status: respData.data.status,
                result: respData.data.arrData
            }
        } else {
            return {
                status: respData.data.status,
                message: respData.data.message
            }
        }
    } catch (err) {
        return false
    }
};

export const getStatusInfo = async (data) => {
    try {
        let respData = await axios({
            'method': 'post',
            'url': `/adminapi/getStatusInfoOption`,
            data
        });

    //     if (respData.data.status) {
    //         return {
    //             status: respData.data.status,
    //             result: respData.data.arrData
    //         }
    //     } else {
    //         return {
    //             status: respData.data.status,
    //             message: respData.data.message
    //         }
    //     }
    // } catch (err) {
    //     return false
    // }
    return {
        status: "success",
        loading: false,
        // count: respData.data.count,
        result: respData.data.data
    }
} catch (err) {
    return {
        status: "failed",
        loading: false,
        message: err.response.data.message,
        error: err.response.data.errors
    }
}
};

export const getTypeInfo = async (data) => {
    try {
        let respData = await axios({
            'method': 'post',
            'url': `/adminapi/getTypeInfoOption`,
            data
        });

        return {
            status: "success",
            loading: false,
            // count: respData.data.count,
            result: respData.data.typeinfoOption
        }
    } catch (err) {
        console.log('err',err)
        return {
            status: "failed",
            loading: false,
            message: err.response.data.message,
            error: err.response.data.errors
        }
    }
};