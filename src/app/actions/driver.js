
import axios from './axios';

export const getAllDriver = async (data) => {
    try {
        let respData = await axios({
            'method': 'get',
            'url': `/adminapi/driver`,
            data
        });
        return {
            status: "success",
            loading: false,
            result: respData.data.result
        }
    } catch (err) {
        return {
            status: "failed",
            loading: false,
            message: err.response.data.message,
            error: err.response.data.errors
        }
    }
};

export const addDriver = async (data) => {
    try {
        let respData = await axios({
            'method': 'post',
            'url': `/adminapi/driver`,
            'headers': {
                'Authorization': localStorage.adminToken
            },

            data
        });
        return {
            status: "success",
            loading: false,
            result: respData.data.result
        }
    } catch (err) {
        return {
            status: "failed",
            loading: false,
            message: err.response.data.message,
            error: err.response.data.errors
        }
    }
};

export const editDriver = async (data) => {
    try {
        let respData = await axios({
            'method': 'put',
            'url': `/adminapi/driver`,
            data
        });
        return {
            status: "success",
            loading: false,
            result: respData.data.result
        }
    } catch (err) {
        return {
            status: "failed",
            loading: false,
            message: err.response.data.message,
            error: err.response.data.errors
        }
    }
};