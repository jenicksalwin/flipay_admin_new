
import axios from './axios';

export const getAllRider = async (data) => {
    try {
        let respData = await axios({
            'method': 'get',
            'url': `/adminapi/riderList`,
            data
        });
        return {
            status: "success",
            loading: false,
            result: respData.data.result
        }
    } catch (err) {
        return {
            status: "failed",
            loading: false,
            message: err.response.data.message,
            error: err.response.data.errors
        }
    }
};
export const getAllCashUsers = async (data) => {
    try {
        let respData = await axios({
            'method': 'get',
            'url': `/adminapi/userCashResult`,
            'params': data
            
        });
       
        return {
            status: "success",
            loading: false,
            result: respData.data.result
        }
    } catch (err) {
        return {
            status: "failed",
            loading: false,
            message: err.response.data.message,
            error: err.response.data.errors
        }
    }
};
export const statusChanged = async (data) => {
    console.log("status changed")
    try {
        let respData = await axios({
            'method': 'post',
            'url': `/adminapi/statusUserCash`,
            headers: {
                'Authorization': localStorage.adminToken
            },
            data,
            
        });
        return {
            status: "success",
            loading: false,
            result: respData.data.success,
            message:respData.data.message
            
        }
    } catch (err) {
        return {
            status: "failed",
            loading: false,
            message: err.response.data.message,
            error: err.response.data.errors
        }
    }
};
export const statusRejected = async (data) => {
    console.log("status Rejected changed",data)
    try {
        let respData = await axios({
            'method': 'post',
            'url': `/adminapi/statusUsercashRejected`,
            headers: {
                'Authorization': localStorage.adminToken
            },
            data
        });
        return {
            status: "success",
            loading: false,
            result: respData.data.result
        }
    } catch (err) {
        return {
            status: "failed",
            loading: false,
            message: err.response.data.message,
            error: err.response.data.errors
        }
    }
};
export const getRideHistory = async (data) => {
    try {
        let respData = await axios({
            'method': 'get',
            'url': `/adminapi/rideHistory`,
            'params': data
        });
        return {
            status: "success",
            loading: false,
            count: respData.data.count,
            result: respData.data.result
        }
    } catch (err) {
        return {
            status: "failed",
            loading: false,
            message: err.response.data.message,
            error: err.response.data.errors
        }
    }
};

export const getRideHistoryCsv = async (data) => {
    try {
        let respData = await axios({
            'method': 'get',
            'url': `/adminapi/rideHistoryCsv`,
            'params': data
        });

        const url = window.URL.createObjectURL(new Blob([respData.data]));
        const link = document.createElement('a');
        link.href = url;
        link.setAttribute('download', 'rideHistory.csv');
        document.body.appendChild(link);
        link.click();

        return true
    } catch (err) {
        return false
    }
};

export const addCardId = async (data) => {
    try {
        let respData = await axios({
            'method': 'post',
            'url': `/adminapi/addCard-rider`,
            headers: {
                'Authorization': localStorage.adminToken
            },
            data
        });
        return {
            status: "success",
            loading: false,
            result: respData.data.result
        }
    } catch (err) {
        return {
            status: "failed",
            loading: false,
            message: err.response.data.message,
            error: err.response.data.errors
        }
    }
};

export const getRiderWallet = async (data) => {
    try {
        let respData = await axios({
            'method': 'get',
            'url': `/adminapi/riderWallet`,
            'params': data
        });
        return {
            status: "success",
            loading: false,
            count: respData.data.count,
            result: respData.data.result
        }
    } catch (err) {
        return {
            status: "failed",
            loading: false,
            message: err.response.data.message,
            error: err.response.data.errors
        }
    }
}

export const addRiderBalance = async (data) => {
    try {
        let respData = await axios({
            'method': 'post',
            'url': `/adminapi/riderWallet`,
            data
        });
        return {
            status: "success",
            loading: false,
            message: respData.data.message,
        }
    } catch (err) {
        return {
            status: "failed",
            loading: false,
            message: err.response.data.message,
            error: err.response.data.errors
        }
    }
}
