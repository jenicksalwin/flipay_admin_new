
import axios from './axios';

export const addVehicle = async (data) => {
    try {
        let respData = await axios({
            'method': 'post',
            'url': `/adminapi/addVehicleNew`,
            'headers': {
                'Authorization': localStorage.adminToken
            },
            data
        });
        return {
            status: "success",
            loading: false,
            message: respData.data.message,
            result: respData.data.result
        }
    } catch (err) {
        console.log('err',err)
        return {
            status: "failed",
            loading: false,
            message: err.response.data.message,
            error: err.response.data.errors
        }
    }
};

export const addVechicalRoute = async (data) => {
    try {
        let respData = await axios({
            'method': 'post',
            'url': `/adminapi/addVechicalRoute`,
            data
        });
        return {
            status: "success",
            loading: false,
            message: respData.data.message,
            result: respData.data.result
        }
    } catch (err) {
        return {
            status: "failed",
            loading: false,
            message: err.response.data.message,
            error: err.response.data.errors
        }
    }
};

export const editVehicle = async (data) => {
    try {
        let respData = await axios({
            'method': 'put',
            'url': `/adminapi/editvehicleNew`,
            data
        });
        return {
            status: "success",
            loading: false,
            message: respData.data.message,
            result: respData.data.result
        }
    } catch (err) {
        return {
            status: "failed",
            loading: false,
            message: err.response.data.message,
            error: err.response.data.errors
        }
    }
};

export const getAllVehicle = async (data) => {
    try {
        let respData = await axios({
            'method': 'post',
            'url': `/adminapi/getvehicle`,
            'params': data

        });
        return {
            status: "success",
            loading: false,
            result: respData.data.result
        }
    } catch (err) {
        return {
            status: "failed",
            loading: false,
            message: err.response.data.message,
            error: err.response.data.errors
        }
    }
};
export const getvacleroutelist = async (data) => {
    try {
        let respData = await axios({
            'method': 'get',
            'url': `/adminapi/getvacleroutelist`,
            data
        });
        console.log("vechileIdroutelist",respData.data)
        return {
            status: "success",
            loading: false,
            result: respData.data.result
        }
    } catch (err) {
        return {
            status: "failed",
            loading: false,
            message: err.response.data.message,
            error: err.response.data.errors
        }
    }
};
export const getVehicleCsv = async (data) => {
    try {
        let respData = await axios({
            'method': 'get',
            'url': `/adminapi/getvehicleCSV`,
            'params': data
        });

        const url = window.URL.createObjectURL(new Blob([respData.data]));
        const link = document.createElement('a');
        link.href = url;
        link.setAttribute('download', 'Vehicle.csv');
        document.body.appendChild(link);
        link.click();

        return true
    } catch (err) {
        return false
    }
};