
import axios from './axios';

export const addAdmin = async (data) => {
    try {
        let respData = await axios({
            'method': 'post',
            'url': `/adminapi/addAdmin`,
            'headers': {
                'Authorization': localStorage.adminToken
            },
            data
        });
        return {
            status: "success",
            loading: false,
            message: respData.data.message,
            result: respData.data.result
        }
    } catch (err) {
        return {
            status: "failed",
            loading: false,
            message: err.response.data.message,
            error: err.response.data.errors
        }
    }
};
export const getdeleteInfo = async (data) => {

    try {
        let respData = await axios({
            'method': 'post',
            'url': `/adminapi/deleteAdmin`,
            'headers': {
                'Authorization': localStorage.adminToken
            },
            data
        });
        return {
            status: "success",
            loading: false,
            message: respData.data.message,
            result: respData.data.result
        }
    } catch (err) {
        return {
            status: "failed",
            loading: false,
            message: err.response.data.message,
            error: err.response.data.errors
        }
    }
};

export const editAdmin = async (data) => {
    try {
        let respData = await axios({
            'method': 'post',
            'url': `/adminapi/editAdmin`,
            'headers': {
                'Authorization': localStorage.adminToken
            },
            data
        });
        return {
            status: "success",
            loading: false,
            message: respData.data.message,
            result: respData.data.result
        }
    } catch (err) {
        return {
            status: "failed",
            loading: false,
            message: err.response.data.message,
            error: err.response.data.errors
        }
    }
};

export const getAllAdminInfo = async (data) => {
    try {
        let respData = await axios({
            'method': 'get',
            'url': `/adminapi/adminInfo`,
            data
        });
        return {
            status: "success",
            loading: false,
            result: respData.data.result
        }
    } catch (err) {
        return {
            status: "failed",
            loading: false,
            message: err.response.data.message,
            error: err.response.data.errors
        }
    }
};
