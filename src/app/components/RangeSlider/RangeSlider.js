import React, { Component } from 'react'
import Slider from 'react-rangeslider'
import 'react-rangeslider/lib/index.css'
class HorizontalCustomLabels extends Component {
  constructor (props) {
    super(props)
    this.state = {
      horizontal: 10,
      // maxAmount,
      // distanceTraveled

    }
  }
//   componentWillReceiveProps(nextProps, value) {
//     const { sliderChange} = this.props;

//     // console.log('componentWillReceiveProps', nextProps);
//     // console.log('value',value)
//     // sliderChange(value)
//     // console.log('value',value)
//     // this.setState(value);
// }
  handleChangeHorizontal = value => {
    // const { value} = this.props;
    // console.log('checked',checked)
    // handleToggle(checked, name)

    const { sliderChange} = this.props;
    
    // console.log('sliderChange', sliderChange);
    console.log('value1',value)
    
    sliderChange(value)
    this.setState({
      horizontal: value,
      maxAmount: value,
      distanceTraveled: value,
    })
  };

 

  render () {
    const { horizontal, vertical, maxAmount, distanceTraveled } = this.state
    const horizontalLabels = {
      0: 'Low',
   
      100: 'High'
    }

    

    const formatkg = value => value 


    return (
      <div className='slider custom-labels'>
        <Slider
          min={0}
          max={100}
onChange
          value={horizontal}
          labels={horizontalLabels}
          format={formatkg}
          handleLabel={horizontal}
          onChange={this.handleChangeHorizontal}
        />
        <div className='value'>{formatkg(horizontal)}</div>
 
      </div>
    )
  }
}

export default HorizontalCustomLabels