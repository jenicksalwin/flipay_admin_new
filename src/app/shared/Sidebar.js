import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { Collapse } from 'react-bootstrap';
import { Dropdown } from 'react-bootstrap';
import { Trans } from 'react-i18next';
import axios from "axios";
import store from "../store";
import jwt_decode from "jwt-decode";
import jwt from 'jsonwebtoken';
import keys from "../actions/config";
import { setCurrentUser, logoutUser } from "../actions/authActions";

import setAuthToken from "../utils/setAuthToken";
import CryptoJS from "crypto-js";
const key = CryptoJS.enc.Base64.parse("#base64Key#");
const iv = CryptoJS.enc.Base64.parse("#base64IV#");
const url = keys.baseUrl;
// import { Provider } from "react-redux";
if (localStorage.adminToken) {
  const token = localStorage.adminToken;
  setAuthToken(token);
  const decoded = jwt_decode(token);
  store.dispatch(setCurrentUser(decoded));
  const currentTime = Date.now() / 1000;
  if (decoded.exp < currentTime) {
    store.dispatch(logoutUser());
    window.location.href = "./login";
  }
}
class Sidebar extends Component {

  getuserid() {
    try {
      var name = "Bearer ";
      var authToken = localStorage.getItem('adminToken')
      if (authToken != null) {
        var token = authToken.replace(name, "");
        var decipher = CryptoJS.AES.decrypt(keys.secretOrKey, key, { iv: iv })
        var decrypt_val = decipher.toString(CryptoJS.enc.Utf8);
        jwt.verify(token, decrypt_val, (err, verified) => {
          if (!err && verified && verified.id) {
            if (verified.id != "" && verified.id != undefined) {
              this.getData(verified.id);

            } else {
              this.props.history.push("/login");
            }
          } else {
            this.props.history.push("/login");
          }
        });
      } else {
        this.props.history.push("/login");
      }
    } catch (err) {
    }
  }

  getData(id) {
    var data = {
      id: id
    }
    axios
      .post(url + "api/admindetail-data", data)
      .then((res) => {
        this.setState({ record: res.data });
        console.log(res.data, "====================================")
      })
      .catch();
  }
  state = {};

  toggleMenuState(menuState) {
    console.log('menuState', menuState)
    if (this.state[menuState]) {
      this.setState({ [menuState]: false });
    } else if (Object.keys(this.state).length === 0) {
      this.setState({ [menuState]: true });
    } else {
      Object.keys(this.state).forEach(i => {
        this.setState({ [i]: false });
      });
      this.setState({ [menuState]: true });
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      this.onRouteChanged();
    }
  }

  onRouteChanged() {
    document.querySelector('#sidebar').classList.remove('active');
    Object.keys(this.state).forEach(i => {
      this.setState({ [i]: false });
    });
    // let dropdownPaths = [];
    // if (store.getState().auth.user.driverList == true) {
    //   dropdownPaths.push({ path: '/driver', state: 'driverMenuOpen' })
    // }
    
    const dropdownPaths = [
      {path:'/client', state: 'clientMenuOpen'},
      {path:'/driver', state: 'driverMenuOpen'},
      {path:'/rider', state: 'RiderMenuOpen'},
      {path:'/distributor_retailer', state: 'DistributorRetailer'},
      {path:'/accounting_system', state: 'AccountingSystem'},


      {path:'/apps', state: 'appsMenuOpen'},
      {path:'/basic-ui', state: 'basicUiMenuOpen'},
      {path:'/form-elements', state: 'formElementsMenuOpen'},
      {path:'/tables', state: 'tablesMenuOpen'},
      {path:'/icons', state: 'iconsMenuOpen'},
      {path:'/charts', state: 'chartsMenuOpen'},
      {path:'/user-pages', state: 'userPagesMenuOpen'},
      {path:'/error-pages', state: 'errorPagesMenuOpen'},
    ];

    dropdownPaths.forEach((obj => {
      console.log('obj', obj.path)
      if (this.isPathActive(obj.path)) {
        console.log('isPathActive', this.isPathActive)
        this.setState({ [obj.state]: true })
      }
    }));

  }
  render() {
    return (
      <nav className="sidebar sidebar-offcanvas" id="sidebar">
        <div className="text-center sidebar-brand-wrapper d-flex align-items-center">
          <a className="sidebar-brand brand-logo brad_loda" href=""><img src={require("../../assets/images/small_logo.png")} alt="logo" className="logo_img" />Dashboard MS</a>
          <a className="sidebar-brand brand-logo-mini pt-3" href=""><img src={require("../../assets/images/small_logo.png")} alt="logo" className="logo_img" /></a>
        </div>
        <ul className="nav">
          {/* <li className="nav-item nav-profile not-navigation-link">
            <div className="nav-link">
              <Dropdown>
                <Dropdown.Toggle className="nav-link user-switch-dropdown-toggler p-0 toggle-arrow-hide bg-transparent border-0 w-100">
                  <div className="d-flex justify-content-between align-items-start">
                    <div className="profile-image">
                    <img className="img-xs rounded-circle" src={ require("../../assets/images/faces/face8.jpg")} alt="profile" />
                      <div className="dot-indicator bg-success"></div>
                    </div>
                    <div className="text-wrapper">
                      <p className="profile-name">Allen Moreno</p>
                      <p className="designation">Premium user</p>
                    </div>
                    
                  </div>
                </Dropdown.Toggle>
                <Dropdown.Menu className="preview-list navbar-dropdown">
                  <Dropdown.Item className="dropdown-item p-0 preview-item d-flex align-items-center" href="!#" onClick={evt =>evt.preventDefault()}>
                    <div className="d-flex">
                      <div className="py-3 px-4 d-flex align-items-center justify-content-center">
                        <i className="mdi mdi-bookmark-plus-outline mr-0"></i>
                      </div>
                      <div className="py-3 px-4 d-flex align-items-center justify-content-center border-left border-right">
                        <i className="mdi mdi-account-outline mr-0"></i>
                      </div>
                      <div className="py-3 px-4 d-flex align-items-center justify-content-center">
                        <i className="mdi mdi-alarm-check mr-0"></i>
                      </div>
                    </div>
                  </Dropdown.Item>
                  <Dropdown.Item className="dropdown-item preview-item d-flex align-items-center text-small" onClick={evt =>evt.preventDefault()}>
                    <Trans>Manage Accounts</Trans>
                  </Dropdown.Item>
                  <Dropdown.Item className="dropdown-item preview-item d-flex align-items-center text-small" onClick={evt =>evt.preventDefault()}>
                    <Trans>Change Password</Trans>
                  </Dropdown.Item>
                  <Dropdown.Item className="dropdown-item preview-item d-flex align-items-center text-small" onClick={evt =>evt.preventDefault()}>
                    <Trans>Check Inbox</Trans>
                  </Dropdown.Item>
                  <Dropdown.Item className="dropdown-item preview-item d-flex align-items-center text-small" onClick={evt =>evt.preventDefault()}>
                    <Trans>Sign Out</Trans>
                  </Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            </div>
          </li> */}

          <li className={this.isPathActive('/adminPages/transportCooperative') ? 'nav-item active' : 'nav-item'}>
            <Link className="nav-link" to="/adminPages/transportCooperative">

              <i className="mdi mdi-television menu-icon"></i>
              <span className="menu-title"><Trans>Dashboard</Trans></span>
            </Link>
          </li>

          {(store.getState().auth.user.transport == true  || store.getState().auth.user.vehicleService == true || store.getState().auth.user.vehicle == true || store.getState().auth.user.device == true || 
          store.getState().auth.user.retailer == true || store.getState().auth.user.role == 'Admin') &&

          <li className={this.isPathActive('/client') ? 'nav-item active' : 'nav-item'}>
            <div className={this.state.clientMenuOpen ? 'nav-link menu-expanded' : 'nav-link'} onClick={() => this.toggleMenuState('clientMenuOpen')} data-toggle="collapse">
              <i className="mdi mdi-crosshairs-gps menu-icon"></i>
              <span className="menu-title"><Trans>Client</Trans></span>
              <i className="menu-arrow"></i>
            </div>
            <Collapse in={this.state.clientMenuOpen}>
              <ul className="nav flex-column sub-menu">
              {store.getState().auth.user.transport == true &&
                <li className="nav-item"> <Link className={this.isPathActive('/FILIPAY_new_adminpanel/transportCooperative') ? 'nav-link active' : 'nav-link'} to="/transportCooperative"><Trans>Transport Cooperative</Trans></Link></li>
  }
                {store.getState().auth.user.vehicleService == true &&

                 <li className="nav-item"> <Link className={this.isPathActive('/FILIPAY_new_adminpanel/vehicleservice') ? 'nav-link active' : 'nav-link'} to="/vehicleservice"><Trans>Vehicle Service</Trans></Link></li>
  }
                  {store.getState().auth.user.vehicle == true &&

                 <li className="nav-item"> <Link className={this.isPathActive('/vehicle') ? 'nav-link active' : 'nav-link'} to="/vehicle"><Trans>Vehicle</Trans></Link></li>
               
                    }  
                                  {store.getState().auth.user.device == true &&
    
                   
    <li className="nav-item"> <Link className={this.isPathActive('/device') ? 'nav-link active' : 'nav-link'} to="/device"><Trans>Device</Trans></Link></li>
               
                                  }
                                                    {/* {store.getState().auth.user.distributor == true &&
                
                                  <li className="nav-item"> <Link className={this.isPathActive('/distributor') ? 'nav-link active' : 'nav-link'} to="/distributor"><Trans>Distributor</Trans></Link></li>
               
                                                    } */}
                                  {store.getState().auth.user.retailer == true &&
                                    <li className="nav-item"> <Link className={this.isPathActive('/retailer') ? 'nav-link active' : 'nav-link'} to="/retailer"><Trans>Retailer</Trans></Link></li>
                                  }
              </ul>
            </Collapse>
          </li>
  }

          {
          ( store.getState().auth.user.driverList == true ||  store.getState().auth.user.timeTracker == true  || store.getState().auth.user.sales == true||  store.getState().auth.user.driverTrxhistory ||  store.getState().auth.user.messageDriver == true ||  store.getState().auth.user.role == 'Admin') && <li className={this.isPathActive('/driver') ? 'nav-item active' : 'nav-item'}>
            <div className={this.state.driverMenuOpen ? 'nav-link menu-expanded' : 'nav-link'} onClick={() => this.toggleMenuState('driverMenuOpen')} data-toggle="collapse">
              <i className="mdi mdi-crosshairs-gps menu-icon"></i>
              <span className="menu-title"><Trans>Driver</Trans></span>
              <i className="menu-arrow"></i>
            </div>
            <Collapse in={this.state.driverMenuOpen}>
              <ul className="nav flex-column sub-menu">
                {store.getState().auth.user.driverList == true && <li className="nav-item"> <Link className={this.isPathActive('/FILIPAY_new_adminpanel/driverslist') ? 'nav-link active' : 'nav-link'} to="/driverslist"><Trans>Drivers List</Trans></Link></li>}
                {store.getState().auth.user.timeTracker == true && <li className="nav-item"> <Link className={this.isPathActive('/') ? 'nav-link active' : 'nav-link'} to="/"><Trans>Time Tracker</Trans></Link></li>}

                {store.getState().auth.user.sales == true && <li className="nav-item"> <Link className={this.isPathActive('/sales') ? 'nav-link active' : 'nav-link'} to="/sales"><Trans>Sales</Trans></Link></li>}

                {store.getState().auth.user.driverTrxhistory == true && <li className="nav-item"> <Link className={this.isPathActive('/') ? 'nav-link active' : 'nav-link'} to="/"><Trans>Transaction History</Trans></Link></li>}
                {store.getState().auth.user.messageDriver == true && <li className="nav-item"> <Link className={this.isPathActive('/messages') ? 'nav-link active' : 'nav-link'} to="/messages"><Trans>Messages</Trans></Link></li>}


                {/* <li className="nav-item"> <Link className={this.isPathActive('/') ? 'nav-link active' : 'nav-link'} to="/"><Trans>Sales</Trans></Link></li> */}
                {/* <li className="nav-item"> <Link className={this.isPathActive('/') ? 'nav-link active' : 'nav-link'} to="/"><Trans>Transaction History</Trans></Link></li>
                <li className="nav-item"> <Link className={this.isPathActive('/') ? 'nav-link active' : 'nav-link'} to="/"><Trans>Messages</Trans></Link></li> */}

              </ul>
            </Collapse>
          </li>
          }

{(store.getState().auth.user.riderList == true ||  store.getState().auth.user.wallet == true || store.getState().auth.user.rideHistory == true || store.getState().auth.user.rideMessages == true || store.getState().auth.user.role == 'Admin')  &&


          <li className={this.isPathActive('/rider') ? 'nav-item active' : 'nav-item'}>
            <div className={this.state.RiderMenuOpen ? 'nav-link menu-expanded' : 'nav-link'} onClick={() => this.toggleMenuState('RiderMenuOpen')} data-toggle="collapse">
              <i className="mdi mdi-crosshairs-gps menu-icon"></i>
              <span className="menu-title"><Trans>Rider</Trans></span>
              <i className="menu-arrow"></i>
            </div>
            <Collapse in={this.state.RiderMenuOpen}>
              <ul className="nav flex-column sub-menu">
              {store.getState().auth.user.riderList == true &&
                <li className="nav-item"> <Link className={this.isPathActive('/FILIPAY_new_adminpanel/ridersList') ? 'nav-link active' : 'nav-link'} to="/ridersList"><Trans>Riders List</Trans></Link></li>
              }

{store.getState().auth.user.wallet == true &&

                <li className="nav-item"> <Link className={this.isPathActive('/FILIPAY_new_adminpanel/wallet') ? 'nav-link active' : 'nav-link'} to="/wallet"><Trans>Wallet</Trans></Link></li>
}

{store.getState().auth.user.rideHistory == true &&

                <li className="nav-item"> <Link className={this.isPathActive('/ridehistory') ? 'nav-link active' : 'nav-link'} to="/ridehistory"><Trans>Ride History</Trans></Link></li>
}
{store.getState().auth.user.rideMessages == true &&


                <li className="nav-item"> <Link className={this.isPathActive('/messages') ? 'nav-link active' : 'nav-link'} to="/messages"><Trans>Messages</Trans></Link></li>
  }
              </ul>
            </Collapse>
          </li>
  }

          {(store.getState().auth.user.cashIn == true ||  store.getState().auth.user.loadSales == true || store.getState().auth.user.distributor == true || store.getState().auth.user.messagedistributor == true  || store.getState().auth.user.transactionHistory == true || store.getState().auth.user.role == 'Admin') &&

          <li className={this.isPathActive('/distributor_retailer') ? 'nav-item active' : 'nav-item'}>
            <div className={this.state.DistributorRetailer ? 'nav-link menu-expanded' : 'nav-link'} onClick={() => this.toggleMenuState('DistributorRetailer')} data-toggle="collapse">
              <i className="mdi mdi-crosshairs-gps menu-icon"></i>
              <span className="menu-title"><Trans>Distributor/Retailer</Trans></span>
              <i className="menu-arrow"></i>
            </div>
            <Collapse in={this.state.DistributorRetailer}>
              <ul className="nav flex-column sub-menu">
              {store.getState().auth.user.distributor == true &&

                <li className="nav-item"> <Link className={this.isPathActive('/distributor') ? 'nav-link active' : 'nav-link'} to="/distributor"><Trans>KYC</Trans></Link></li>
              }
                {/* <li className="nav-item"> <Link className={ this.isPathActive('/kyc-status') ? 'nav-link active' : 'nav-link' } to="/kyc-status"><Trans>KYC</Trans></Link></li> */}
                {store.getState().auth.user.cashIn == true &&

                <li className="nav-item"> <Link className={this.isPathActive('/cashIn') ? 'nav-link active' : 'nav-link'} to="/cashIn"><Trans>Cash In</Trans></Link></li>
  }
                  {store.getState().auth.user.loadSales == true &&

                <li className="nav-item"> <Link className={this.isPathActive('/loadcard') ? 'nav-link active' : 'nav-link'} to="/loadcard"><Trans>Load Balance</Trans></Link></li>
                  }
                  {store.getState().auth.user.transactionHistory == true &&


                <li className="nav-item"> <Link className={this.isPathActive('/transactionhistory') ? 'nav-link active' : 'nav-link'} to="/transactionhistory"><Trans>Transaction History</Trans></Link></li>
                  }
                                    {store.getState().auth.user.messageDistributor == true &&

                <li className="nav-item"> <Link className={this.isPathActive('/') ? 'nav-link active' : 'nav-link'} to="/"><Trans>Messages</Trans></Link></li>
                                    }
              </ul>
            </Collapse>
          </li>
  }

{(store.getState().auth.user.accountingSystem == true ||  store.getState().auth.user.fareIncome == true || store.getState().auth.user.loadSales == true || store.getState().auth.user.cardSales == true  || store.getState().auth.user.role == 'Admin') &&

          <li className={this.isPathActive('/accounting_system') ? 'nav-item active' : 'nav-item'}>
            <div className={this.state.AccountingSystem ? 'nav-link menu-expanded' : 'nav-link'} onClick={() => this.toggleMenuState('AccountingSystem')} data-toggle="collapse">
              <i className="mdi mdi-crosshairs-gps menu-icon"></i>
              <span className="menu-title"><Trans>Accounting System</Trans></span>
              <i className="menu-arrow"></i>
            </div>
            <Collapse in={this.state.AccountingSystem}>
              <ul className="nav flex-column sub-menu">
          {  store.getState().auth.user.accountingSystem == true &&
                <li className="nav-item"> <Link className={this.isPathActive('/accountingsystem') ? 'nav-link active' : 'nav-link'} to="/accountingsystem"><Trans>Accounting Management</Trans></Link></li>
          }
          {store.getState().auth.user.fareIncome == true &&
                <li className="nav-item"> <Link className={this.isPathActive('/fareincome') ? 'nav-link active' : 'nav-link'} to="/fareincome"><Trans>Fare Income</Trans></Link></li>
               
           } 
                     {store.getState().auth.user.loadSales == true &&

           <li className="nav-item"> <Link className={this.isPathActive('/loadsales') ? 'nav-link active' : 'nav-link'} to="/"><Trans>Load Sales</Trans></Link></li>
                     }   
                                       {store.getState().auth.user.cardSales == true &&

           <li className="nav-item"> <Link className={this.isPathActive('/cardsales') ? 'nav-link active' : 'nav-link'} to="/"><Trans>Card Sales</Trans></Link></li>
                                       }      
           </ul>
            </Collapse>
          </li>
  }

{(store.getState().auth.user.SubadminMenu == true  &&  store.getState().auth.user.role == 'Admin') && 

<li className={this.isPathActive('/AdminController') ? 'nav-item active' : 'nav-item'}>
  <Link className="nav-link" to="/AdminController">
    <i className="mdi mdi-television menu-icon"></i>
    <span className="menu-title"><Trans>Admin/Sub-Admin Controller</Trans></span>
  </Link>
</li>
}
  {(store.getState().auth.user.emailTemplate == true || store.getState().auth.user.role == 'Admin' && 
   
          <li className={this.isPathActive('/Email') ? 'nav-item active' : 'nav-item'}>
            <Link className="nav-link" to="/emailtemplate">
              <i className="mdi mdi-television menu-icon"></i>
              <span className="menu-title"><Trans>Email Template</Trans></span>
            </Link>
          </li>
  )}
  {store.getState().auth.user.privacyPolicy == true || store.getState().auth.user.role == 'Admin' && 


          <li className={this.isPathActive('/privacypolicy') ? 'nav-item active' : 'nav-item'}>
            <Link className="nav-link" to="/privacypolicy">
              <i className="mdi mdi-television menu-icon"></i>
              <span className="menu-title"><Trans>Privacy Policy</Trans></span>
            </Link>
          </li>
  }
 
    {store.getState().auth.user.adminActivity == true || store.getState().auth.user.role == 'Admin' && 


          <li className={this.isPathActive('/Admin_Activity') ? 'nav-item active' : 'nav-item'}>
            <Link className="nav-link" to="/Admin_Activity">
              <i className="mdi mdi-television menu-icon"></i>
              <span className="menu-title"><Trans>Admin Activity</Trans></span>
            </Link>
          </li>
  }

{store.getState().auth.user.support == true || store.getState().auth.user.role == 'Admin' && 

          <li className={this.isPathActive('/Support') ? 'nav-item active' : 'nav-item'}>
            <Link className="nav-link" to="/Support">
              <i className="mdi mdi-television menu-icon"></i>
              <span className="menu-title"><Trans>Support</Trans></span>
            </Link>
          </li>
  }

          {/* 
          <li className={ this.isPathActive('/basic-ui') ? 'nav-item active' : 'nav-item' }>
            <div className={ this.state.basicUiMenuOpen ? 'nav-link menu-expanded' : 'nav-link' } onClick={ () => this.toggleMenuState('basicUiMenuOpen') } data-toggle="collapse">
              <i className="mdi mdi-crosshairs-gps menu-icon"></i>
              <span className="menu-title"><Trans>Basic UI Elements</Trans></span>
              <i className="menu-arrow"></i>
            </div>
            <Collapse in={ this.state.basicUiMenuOpen }>
              <ul className="nav flex-column sub-menu">
                <li className="nav-item"> <Link className={ this.isPathActive('/basic-ui/buttons') ? 'nav-link active' : 'nav-link' } to="/basic-ui/buttons"><Trans>Buttons</Trans></Link></li>
                <li className="nav-item"> <Link className={ this.isPathActive('/basic-ui/dropdowns') ? 'nav-link active' : 'nav-link' } to="/basic-ui/dropdowns"><Trans>Dropdowns</Trans></Link></li>
              </ul>
            </Collapse>
          </li> */}


          {/* <li className={ this.isPathActive('/form-elements') ? 'nav-item active' : 'nav-item' }>
            <div className={ this.state.formElementsMenuOpen ? 'nav-link menu-expanded' : 'nav-link' } onClick={ () => this.toggleMenuState('formElementsMenuOpen') } data-toggle="collapse">
              <i className="mdi mdi-format-list-bulleted menu-icon"></i>
              <span className="menu-title"><Trans>Form Elements</Trans></span>
              <i className="menu-arrow"></i>
            </div>
            <Collapse in={ this.state.formElementsMenuOpen }>
              <ul className="nav flex-column sub-menu">
                <li className="nav-item"> <Link className={ this.isPathActive('/form-elements/basic-elements') ? 'nav-link active' : 'nav-link' } to="/form-elements/basic-elements"><Trans>Basic Elements</Trans></Link></li>
              </ul>
            </Collapse>
          </li>
          <li className={ this.isPathActive('/tables') ? 'nav-item active' : 'nav-item' }>
            <div className={ this.state.tablesMenuOpen ? 'nav-link menu-expanded' : 'nav-link' } onClick={ () => this.toggleMenuState('tablesMenuOpen') } data-toggle="collapse">
              <i className="mdi mdi-table-large menu-icon"></i>
              <span className="menu-title"><Trans>Tables</Trans></span>
              <i className="menu-arrow"></i>
            </div>
            <Collapse in={ this.state.tablesMenuOpen }>
              <ul className="nav flex-column sub-menu">
                <li className="nav-item"> <Link className={ this.isPathActive('/tables/basic-table') ? 'nav-link active' : 'nav-link' } to="/tables/basic-table"><Trans>Basic Table</Trans></Link></li>
              </ul>
            </Collapse>
          </li>
          <li className={ this.isPathActive('/icons') ? 'nav-item active' : 'nav-item' }>
            <div className={ this.state.iconsMenuOpen ? 'nav-link menu-expanded' : 'nav-link' } onClick={ () => this.toggleMenuState('iconsMenuOpen') } data-toggle="collapse">
              <i className="mdi mdi-account-box-outline menu-icon"></i>
              <span className="menu-title"><Trans>Icons</Trans></span>
              <i className="menu-arrow"></i>
            </div>
            <Collapse in={ this.state.iconsMenuOpen }>
              <ul className="nav flex-column sub-menu">
                <li className="nav-item"> <Link className={ this.isPathActive('/icons/mdi') ? 'nav-link active' : 'nav-link' } to="/icons/mdi">Material</Link></li>
              </ul>
            </Collapse>
          </li>
          <li className={ this.isPathActive('/charts') ? 'nav-item active' : 'nav-item' }>
            <div className={ this.state.chartsMenuOpen ? 'nav-link menu-expanded' : 'nav-link' } onClick={ () => this.toggleMenuState('chartsMenuOpen') } data-toggle="collapse">
              <i className="mdi mdi-chart-line menu-icon"></i>
              <span className="menu-title"><Trans>Charts</Trans></span>
              <i className="menu-arrow"></i>
            </div>
            <Collapse in={ this.state.chartsMenuOpen }>
              <ul className="nav flex-column sub-menu">
                <li className="nav-item"> <Link className={ this.isPathActive('/charts/chart-js') ? 'nav-link active' : 'nav-link' } to="/charts/chart-js">Chart Js</Link></li>
              </ul>
            </Collapse>
          </li>
          <li className={ this.isPathActive('/user-pages') ? 'nav-item active' : 'nav-item' }>
            <div className={ this.state.userPagesMenuOpen ? 'nav-link menu-expanded' : 'nav-link' } onClick={ () => this.toggleMenuState('userPagesMenuOpen') } data-toggle="collapse">
              <i className="mdi mdi-lock-outline menu-icon"></i>
              <span className="menu-title"><Trans>User Pages</Trans></span>
              <i className="menu-arrow"></i>
            </div>
            <Collapse in={ this.state.userPagesMenuOpen }>
              <ul className="nav flex-column sub-menu">
                <li className="nav-item"> <Link className={ this.isPathActive('/user-pages/login-1') ? 'nav-link active' : 'nav-link' } to="/user-pages/login-1"><Trans>Login</Trans></Link></li>
                <li className="nav-item"> <Link className={ this.isPathActive('/user-pages/register-1') ? 'nav-link active' : 'nav-link' } to="/user-pages/register-1"><Trans>Register</Trans></Link></li>
              </ul>
            </Collapse>
          </li>
          <li className={ this.isPathActive('/error-pages') ? 'nav-item active' : 'nav-item' }>
            <div className={ this.state.errorPagesMenuOpen ? 'nav-link menu-expanded' : 'nav-link' } onClick={ () => this.toggleMenuState('errorPagesMenuOpen') } data-toggle="collapse">
              <i className="mdi mdi-information-outline menu-icon"></i>
              <span className="menu-title"><Trans>Error Pages</Trans></span>
              <i className="menu-arrow"></i>
            </div>
            <Collapse in={ this.state.errorPagesMenuOpen }>
              <ul className="nav flex-column sub-menu">
                <li className="nav-item"> <Link className={ this.isPathActive('/error-pages/error-404') ? 'nav-link active' : 'nav-link' } to="/error-pages/error-404">404</Link></li>
                <li className="nav-item"> <Link className={ this.isPathActive('/error-pages/error-500') ? 'nav-link active' : 'nav-link' } to="/error-pages/error-500">500</Link></li>
              </ul>
            </Collapse>
          </li>
          <li className="nav-item">
            <a className="nav-link" href="http://www.bootstrapdash.com/demo/star-admin-free/react/documentation/documentation.html" rel="noopener noreferrer" target="_blank">
              <i className="mdi mdi-file-outline menu-icon"></i>
              <span className="menu-title"><Trans>Documentation</Trans></span>
            </a>
          </li> */}
        </ul>
      </nav>
    );
  }

  isPathActive(path) {
    return this.props.location.pathname.startsWith(path);
  }

  componentDidMount() {
    this.onRouteChanged();
    // add className 'hover-open' to sidebar navitem while hover in sidebar-icon-only menu
    const body = document.querySelector('body');
    document.querySelectorAll('.sidebar .nav-item').forEach((el) => {

      el.addEventListener('mouseover', function () {
        if (body.classList.contains('sidebar-icon-only')) {
          el.classList.add('hover-open');
        }
      });
      el.addEventListener('mouseout', function () {
        if (body.classList.contains('sidebar-icon-only')) {
          el.classList.remove('hover-open');
        }
      });
    });
  }

}

export default withRouter(Sidebar);